Tails module for Puppet

This module contains profiles for the Tails infrastructure, as well as legacy
classes. For the full picture, see:
https://gitlab.tails.boum.org/tails/puppet-code


Translation Platform documentation
==================================

Documentation for the Translation platform, as implemented by `tails::weblate*`
classes, can be found in:

    https://tails.net/contribute/design/translation_platform/

More specifically, documentation on the containerized setup for Weblate can be
found in:

    https://tails.net/contribute/design/translation_platform/containerized_setup

License and copyright
=====================

Unless explicitly noted in individual files, this module is:

Copyright (C) 2012-2019 Tails developers <tails@boum.org>
GPLv3
