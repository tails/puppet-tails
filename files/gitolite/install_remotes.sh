#!/bin/bash

set -e

gitolite_root=/var/lib/gitolite3
repository_root="${gitolite_root}/repositories"

/usr/bin/find "$repository_root" \
   -mindepth 1 -maxdepth 1 \
   -type d -print0 | while IFS= read -r -d '' repository_dir ; do

    repository=$(basename "$repository_dir")
    # set up remotes for the mirrors we're pushing to
    case "$repository" in
        puppet-*|etcher-binary.git|jenkins-jobs.git|promotion-material.git|tails.git)
            if ! grep -qs -x "${repository}" \
                  /var/lib/gitolite3/projects.list ; then
               continue
            fi
            (
               cd "$repository_dir"
               case "$repository" in
                   # The puppet-* and jenkins-jobs repos should now also
                   # mirror to Tails GitLab instance.
                   puppet-*|jenkins-jobs.git)
                       if git remote | grep -qs -x gitlab; then
                           git remote set-url gitlab "git@gitlab-ssh.tails.boum.org:tails/$repository"
                       else
                              git remote add gitlab "git@gitlab-ssh.tails.boum.org:tails/$repository"
                       fi
                       ;;

                   # All the others are now mirrors of their counterpart repos
                   # in Tails GitLab instance.
                   *)
		       ;;
               esac
            )
            ;;
	weblate-gatekeeper.git)
            cd "$repository_dir"
            git remote | grep -qs -x gitlab || \
                git remote add gitlab "git@gitlab-as-role-weblate-gatekeeper:tails/tails.git"
            ;;
        *)
            ;;
    esac
done
