#!/usr/bin/python3

import git
import importlib.machinery
import os
import sys
import tempfile
import unittest
from unittest.mock import patch

# Import the hook file as a module
weblate_hook = importlib.machinery.SourceFileLoader('weblate_hook', 'tails-weblate-update.hook').load_module()
weblate_hook.ALLOWED_LANGUAGES.result = ["de", "fr", "aa", "bb"]


class TestWeblateHookMain(unittest.TestCase):
    def setUp(self):
        self.repopath = tempfile.TemporaryDirectory()
        self.repo = git.Repo.init(self.repopath.name)

    def tearDown(self):
        self.repopath.cleanup()

    @patch.object(sys, 'argv', ['hooks/update', 'refs/heads/master',
                                '03b89c8f4b55306818f19f610cababbadcdc54c0',
                                '0000000000000000000000000000000000000000'])
    def test_deleting_branch(self):
        with patch.object(os, 'environ', {'GL_USER': ''}):
            self.assertIsNone(weblate_hook.main(self.repopath.name))
        with patch.object(os, 'environ', {'GL_USER': weblate_hook.EXPECTED_GL_USER}):
            with self.assertRaises(SystemExit) as e:
                weblate_hook.main(self.repopath.name)
            self.assertEqual(e.exception.args,
                             ('[POLICY] Weblate is not allowed to delete branches.',))

    @patch.object(sys, 'argv', ['hooks/update', 'refs/heads/master',
                                '0000000000000000000000000000000000000000',
                                '03b89c8f4b55306818f19f610cababbadcdc54c0'])
    def test_creating_branch(self):
        with patch.object(os, 'environ', {'GL_USER': ''}):
            self.assertIsNone(weblate_hook.main(self.repopath.name))
        with patch.object(os, 'environ', {'GL_USER': weblate_hook.EXPECTED_GL_USER}):
            with self.assertRaises(SystemExit) as e:
                weblate_hook.main(self.repopath.name)
            self.assertEqual(e.exception.args,
                             ('[POLICY] Weblate is not allowed to create new branches.',))

    @patch.object(sys, 'argv', ['hooks/update', 'refs/heads/master',
                                'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                                'a3b89c8f4b553a6818f19f61acababbadcdc54ca'])
    @patch('weblate_hook.validate_commit_range', return_value=[], autospec=True)
    def test_call_validate_commit_range(self, hook_validate_commit_range):
        with patch.object(os, 'environ', {'GL_USER': ''}):
            self.assertIsNone(weblate_hook.main(self.repopath.name))
            hook_validate_commit_range.assert_not_called()
        with patch.object(os, 'environ', {'GL_USER': weblate_hook.EXPECTED_GL_USER}):
            self.assertIsNone(weblate_hook.main(self.repopath.name))
            hook_validate_commit_range.assert_called_once_with(self.repo,
                                         'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                                         'a3b89c8f4b553a6818f19f61acababbadcdc54ca')

    @patch.object(sys, 'argv', ['hooks/update', 'refs/heads/master',
                                'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                                'a3b89c8f4b553a6818f19f61acababbadcdc54ca'])
    @patch('weblate_hook.validate_commit_range', return_value=['issue1', 'issue2'], autospec=True)
    def test_failing_call_validate_commit_range(self, hook_validate_commit_range):
        with patch.object(os, 'environ', {'GL_USER': ''}):
            self.assertIsNone(weblate_hook.main(self.repopath.name))
            hook_validate_commit_range.assert_not_called()
        with patch.object(os, 'environ', {'GL_USER': weblate_hook.EXPECTED_GL_USER}):
            with self.assertRaises(SystemExit) as e:
                weblate_hook.main(self.repopath.name)
            self.assertEqual(e.exception.args, ('issue1\nissue2',))
            hook_validate_commit_range.assert_called_once_with(self.repo,
                                         'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                                         'a3b89c8f4b553a6818f19f61acababbadcdc54ca')


class TestWeblateHookValidateCommitRange(unittest.TestCase):
    def setUp(self):
        badActor = git.Actor("bad", "bad@example.com")
        goodActor = weblate_hook.EXPECTED_COMMITTER

        self.repopath = tempfile.TemporaryDirectory()
        self.repo = git.Repo.init(self.repopath.name)

        os.mkdir(os.path.join(self.repopath.name, "wiki"))
        os.mkdir(os.path.join(self.repopath.name, "wiki/src"))

        self.repo.index.commit("init")
        self.repo.create_tag('base')

        open(os.path.join(self.repopath.name, "test"), "w").write("foo")
        self.repo.index.add(["test"])
        self.repo.index.commit("add test file", author=badActor, committer=badActor)
        self.repo.create_tag('random_file')

        open(os.path.join(self.repopath.name, "test"), "w").write("foo2")
        self.repo.index.add(["test"])
        self.repo.index.commit("add test file", author=goodActor, committer=goodActor)
        self.repo.create_tag('random_file_expected_committer')

        open(os.path.join(self.repopath.name, 'wiki/src', "test.xx.po"), "w").write("foo2")
        self.repo.index.add([os.path.join('wiki/src', "test.xx.po")])
        self.repo.index.commit("add test.xx.po", author=goodActor, committer=goodActor)
        self.repo.create_tag('test.xx.po')

        open(os.path.join(self.repopath.name, 'wiki/src', "test.de.po"), "w").write("foo2")
        self.repo.index.add([os.path.join('wiki/src', "test.de.po")])
        self.repo.index.commit("add test.de.po", author=goodActor, committer=goodActor)
        self.repo.create_tag('test.de.po')

        open(os.path.join(self.repopath.name, 'wiki/src', "test.aa.po"), "w").write("foo2")
        os.chmod(os.path.join(self.repopath.name, 'wiki/src', "test.aa.po"), 0o755)
        self.repo.index.add([os.path.join('wiki/src', "test.aa.po")])
        self.repo.index.commit("add test.aa.po", author=goodActor, committer=goodActor)
        self.repo.create_tag('test.aa.po')

        open(os.path.join(self.repopath.name, 'wiki/src', "aa.po"), "w").write("foo2")
        self.repo.index.add([os.path.join('wiki/src', "aa.po")])
        self.repo.index.commit("add aa.po", author=goodActor, committer=goodActor)
        self.repo.create_tag('aa.po')

        open(os.path.join(self.repopath.name, 'wiki/src', ".test.aa.po"), "w").write("foo2")
        self.repo.index.add([os.path.join('wiki/src', ".test.aa.po")])
        self.repo.index.commit("add .test.aa.po", author=goodActor, committer=goodActor)
        self.repo.create_tag('_.test.aa.po')


    def tearDown(self):
        self.repopath.cleanup()

    def hexsha(self, ref):
        return self.repo.commit(ref).hexsha

    def invalidCommiter(self, ref):
        return "{ref} - [POLICY] Weblate is only allowed to push commits with '{name} <{email}>' as committer.".format(ref=self.hexsha(ref)[-10:], name=weblate_hook.EXPECTED_COMMITTER.name, email=weblate_hook.EXPECTED_COMMITTER.email)

    def invalidLang(self):
        return "[POLICY] Language not allowed to push."

    def onlyPo(self):
        return "[POLICY] Weblate is only allowed to modify po files."

    def onlyDirectories(self, fname):
        return "[POLICY] Weblate is not allowed to push changes for {fname}.".format(fname=fname)

    def invalidPermissions(self, fname, perm):
        return "[POLICY] Weblate is only allowed to push files with mode 100644, while {fname} has mode {perm}.".format(fname=fname, perm=perm)

    def test_checkRandomFile(self):
        c1 = 'random_file'
        c2 = 'random_file_expected_committer'
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha('base'), self.hexsha(c1)), [self.invalidCommiter(c1),
            self.onlyPo(),
            self.onlyDirectories('test')])

        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha(c1), self.hexsha(c2)), [self.onlyPo(),
            self.onlyDirectories('test')])

        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha('base'), self.hexsha(c2)), [
            self.invalidCommiter(c1),
            self.onlyPo(),
            self.onlyDirectories('test'),
            ])

    def test_wikisrc(self):
        c1 = "test.xx.po"
        c2 = "test.de.po"
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha('random_file_expected_committer'), self.hexsha(c1)), [self.invalidLang()])
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha(c1), self.hexsha(c2)), [])
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha('random_file_expected_committer'), self.hexsha(c2)), [self.invalidLang()])

    def test_permissons(self):
        c1 = "test.aa.po"
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha('test.de.po'), self.hexsha(c1)), [self.invalidPermissions("wiki/src/test.aa.po",100755)])

    def test_invalid_files(self):
        c1 = "test.aa.po"
        c2 = "aa.po"
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha(c1), self.hexsha(c2)), [self.invalidLang()])

    def test_hiddenfiles(self):
        c1 = "aa.po"
        c2 = "_.test.aa.po"
        self.assertCountEqual(weblate_hook.validate_commit_range(self.repo, self.hexsha(c1), self.hexsha(c2)), [self.onlyPo()])

if __name__ == '__main__':
    unittest.main()
