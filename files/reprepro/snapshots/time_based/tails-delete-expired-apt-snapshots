#!/usr/bin/perl

# Dependencies:
#
#    libdatetime-perl
#    libdatetime-format-mail-perl
#    libfile-find-rule-perl
#    libfile-slurp-perl
#    libipc-system-simple-perl
#    liblist-moreutils-perl

use strict;
use warnings;

use DateTime;
use DateTime::Format::Mail;
use English qw{-no_match_vars};
use File::Basename;
use File::Find::Rule;
use File::Path qw(remove_tree);
use File::Slurp;
use Getopt::Long;
use IPC::System::Simple qw(runx);
use List::MoreUtils qw(uniq);


sub usage {
    die "Usage: $0 [--dry-run] archive-directory";
}

sub verbose_print {
    print @_
      if $ENV{VERBOSE};
}

my $dryrun = 0;
GetOptions ("dry-run" => \$dryrun)
  or usage();

print STDERR "\n*** DRY RUN *** running in simulation mode\n\n"
  if $dryrun && !$ENV{SILENT};

my $archive_dir = shift @ARGV;
if (! defined $archive_dir) {
    print "E: missing parameter: archive directory\n";
    usage();
}

if (! -d $archive_dir) {
    print "E: specified archive directory ($archive_dir) isn't a directory\n";
    usage();
}

if ($archive_dir !~ m{\A [/]}xms) {
    print "E: specified archive directory ($archive_dir) isn't an absolute path\n";
    usage();
}


if (! -d "$archive_dir/dists" ) {
    print "E: specified archive directory has no dists/ subdirectory\n";
    usage();
}

my @snapshots_dirs = File::Find::Rule->directory()
  ->name( 'snapshots' )
  ->in( "$archive_dir/dists" );

verbose_print("found snapshots directories: @snapshots_dirs\n");

# Let's load once and for all references known by reprepro to see
# which suites/snapshots are present there to see what needs removing
# (use a shell pipeline to avoid loading the entire output of
# dumpreferences into memory; we've seen it take no less than 7.5 GiB):
my @reprepro_references =
  `reprepro -b '$archive_dir' dumpreferences \\
      | awk '/^s=/ {print \$1}' \\
      | sort -u`;
${^CHILD_ERROR_NATIVE} == 0
    or die "E: Failed to load references: ${^CHILD_ERROR_NATIVE}";
chomp @reprepro_references;
verbose_print("reprepro-known snapshot references:", (join "\n - ", '', @reprepro_references), "\n");

my $now = DateTime->now();
my $removed = 0;

my @reprepro_options = ('-b', $archive_dir);
push @reprepro_options, '--verbose' if $ENV{VERBOSE};
push @reprepro_options, '--silent'  if $ENV{SILENT};

foreach my $dir (@snapshots_dirs) {
    # Let's get the suite name based on the directory name; the
    # contents of the Release files might be slightly off, depending
    # on the current reprepro configuration (Jessie/updates is a bit
    # special, and might not be handled perfectly):
    my $suite = $dir;
    # XXX: anchor these two regexps?
    $suite =~ s{ $archive_dir/dists/ }{}x;
    $suite =~ s{/snapshots}{};

    verbose_print("Looking at snapshots for $suite under $dir\n");
    foreach my $snapshot (glob("$dir/*")) {
        my $timestamp = basename $snapshot;
        # Get Valid-Until:
        my $valid_until;
        foreach my $line (read_file("$snapshot/Release")) {
            if ($line =~ /^Valid-Until: (.+)$/) {
                $valid_until = $1;
            }
        }
        # Stop here if needed:
        if (not defined $valid_until) {
            print "W: no Valid-Until field found in $snapshot/Release, skipping\n";
            next;
        }

        # Work around UTC suffix, which isn't parsed by DateTime::Format::Mail:
        $valid_until =~ s/UTC$/+0000/;
        verbose_print(" - $snapshot is valid until $valid_until\n");
        my $date = DateTime::Format::Mail->parse_datetime($valid_until);
        if (DateTime->compare($now, $date) > 0) {
            verbose_print("    expiration time reached\n");

            # Let's look at the references in reprepro to see if there's
            # something to delete:
            my $db_id = "s=$suite=$timestamp";
            if (grep { $db_id eq $_ } @reprepro_references) {
                if ($dryrun) {
                    verbose_print("    should be forgetting: $db_id\n");
                }
                else {
                    verbose_print("    forgetting: $db_id\n");
                    runx('reprepro', @reprepro_options, '_removereferences', $db_id);
                    verbose_print("    forgotten: $db_id\n");
                }
                $removed++;
            }

            # Remove actual files:
            if ($dryrun) {
                verbose_print("    should be removing: $snapshot\n");
            }
            else {
                verbose_print("    removing: $snapshot\n");
                my $rm_files = remove_tree($snapshot);
                verbose_print("    removed: $rm_files files\n");
            }
        }
        else {
            verbose_print("    nothing to do\n");
        }
    }
}

if ($removed > 0) {
    verbose_print("at least one snapshot was removed (total: $removed)\n");
    if ($dryrun) {
        verbose_print("should be running: reprepro deleteunreferenced\n");
    }
    else {
        verbose_print("running: reprepro deleteunreferenced\n");
        runx('reprepro', @reprepro_options, 'deleteunreferenced');
        verbose_print("all done\n");
    }
}
