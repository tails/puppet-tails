#!/usr/bin/python3
#
# Periodically check the HTTP status of different IPs and serve an endpoint
# that returns the status for the requested IP.
#
# Intended to be used with PowerDNS LUA records using the `ifurlextup`
# function.
#
# Pass the IP pool as a comma-separated list of IPs in the POOL environment
# variable.
#
# See: https://doc.powerdns.com/authoritative/lua-records/functions.html#functions-available

import datetime
import os
import re
import requests
import socket

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, make_response


pool = os.getenv('POOL').split(',')
interval = 30


up = set()
app = Flask(__name__)
m = re.compile('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$')


@app.route('/<ip>')
def index(ip):
    if m.match(ip) and ip in up:
        return 'up'
    return make_response('', 404)


def request(ip):
    # Temporarily trick requests to resolve to a specific IP
    orig = socket.getaddrinfo
    patch = lambda *_: [(socket.AddressFamily.AF_INET, socket.SocketKind.SOCK_STREAM, 6, '', (ip, 443))]
    socket.getaddrinfo = patch
    r = requests.get('https://tails.net/', timeout=10)
    socket.getaddrinfo = orig
    return r


def check_ip(ip):
    try:
        if request(ip).status_code == 200:
            up.add(ip)
            return
    except Exception as e:
        app.logger.warning(e)
    up.discard(ip)


def check_ips():
    for ip in pool:
        check_ip(ip)


scheduler = BackgroundScheduler()
scheduler.add_job(check_ips, 'date')  # run now
scheduler.add_job(check_ips, 'interval', seconds=interval)
scheduler.start()
