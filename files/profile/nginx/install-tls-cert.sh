#!/bin/bash
#
# This script installs a TLS certificate and key in /etc/nginx/ssl so Nginx is
# able to load it. It is intended to workaround the circular dependency between
# Nginx and Certbot:
#
#   - Certbot needs Nginx in order to generate Let's Encrypt certificates.
#   - Nginx needs certificates in place in order to startup.
#
# When called, it first tries to copy certificates from the Let's Encrypt live
# directory, and falls back to copying a snakeoil certificate if the Let's
# Encrypt ones are not available.
#
# It can be used in 2 forms:
#
#     Usage from command line:
#
#         install-tls-cert DOMAIN
#
#     Usage as certbot deploy-hook:
#
#         # certbot sets the RENEWED_DOMAINS environment variable when calling
#         # deploy hooks.
#         env RENEWED_DOMAINS='example.com example.org' install-tls-cert

set -ex

fix_perms() {
	chmod 0640 /etc/nginx/ssl/${1}.{pem,key}
	chown www-data:www-data /etc/nginx/ssl/${1}.{pem,key}
}

install_letsencrypt() {
	cp /etc/letsencrypt/live/${1}/fullchain.pem /etc/nginx/ssl/${1}.pem && \
	cp /etc/letsencrypt/live/${1}/privkey.pem /etc/nginx/ssl/${1}.key && \
	fix_perms "${1}"
}

install_snakeoil() {
	cp /etc/ssl/certs/ssl-cert-snakeoil.pem /etc/nginx/ssl/${1}.pem && \
	cp /etc/ssl/private/ssl-cert-snakeoil.key /etc/nginx/ssl/${1}.key && \
	fix_perms "${1}"
}

DOMAINS=$@

if [ $# -eq 0 ]; then
	if [ -z "${RENEWED_DOMAINS}" ]; then
		echo "Usage: $0 DOMAIN [DOMAIN ...]"
		exit 1
	fi
	DOMAINS="${RENEWED_DOMAINS}"
fi

for d in ${DOMAINS}; do
	install_letsencrypt "${d}" || install_snakeoil "${d}"
done
