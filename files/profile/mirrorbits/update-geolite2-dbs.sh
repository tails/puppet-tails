#!/bin/sh
#
# update-geolite2-dbs.sh
#
# Download GeoLite2 databases from MaxMind website and install them somewhere
# Mirrorbits can find them (/usr/share/GeoIP).
#
# There are currently 3 different databases: ASN, City and Country. This script
# downloads and decompresses them, and then copies each of the resulting .mmdb
# files to the appropriate directory.
#
# This script depends on a file containing the MaxMind API key
# (/etc/mirrorbits-geolite2-api-key).
#
# The user running this script needs permissions to write to /usr/share/GeoIP.
#
# Scheduling the run of this script and the restarting of the Mirrorbits
# service after an update is achieved via Puppet.

set -eu

LICENSE_KEY_FILE=/etc/mirrorbits-geolite2-api-key
URL='https://download.maxmind.com/app/geoip_download'
TARGET_DIR=/usr/share/GeoIP

if [ ! -r ${LICENSE_KEY_FILE} ]; then
        echo "$0: Error: Unable to read API key from file ${LICENSE_KEY_FILE}"
        exit 1
fi

LICENSE_KEY="$( cat ${LICENSE_KEY_FILE} )"
TEMPDIR=$( mktemp -d )
WGET="wget --quiet --directory-prefix=${TEMPDIR} --content-disposition"

trap "rm -rf ${TEMPDIR}" EXIT

for edition_id in ASN City Country; do
        qs="edition_id=GeoLite2-${edition_id}&license_key=${LICENSE_KEY}&suffix=tar.gz"
        ${WGET} "${URL}?${qs}"
        ${WGET} "${URL}?${qs}.sha256"
        ( cd ${TEMPDIR} && sha256sum --quiet -c GeoLite2-${edition_id}_*.tar.gz.sha256 )
        tar -C ${TEMPDIR} -xf ${TEMPDIR}/GeoLite2-${edition_id}_*.tar.gz
done

mv ${TEMPDIR}/*/*.mmdb ${TARGET_DIR}/
