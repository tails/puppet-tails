#!/usr/bin/python3
#
# update-mirrorbits-mirrors.py
#
# Read valid JSON data from either STDIN or a file and update the list of
# mirrors in Mirrorbits.
#
# If `/etc/mirrorbits-rpc-password` exists, use its contents to authorize
# operations in the Mirrorbits daemon.
#
# After this is run, the list of mirrors configured in Mirrorbits should match
# exactly the items in `mirrors.json` that provide an `rsync_url`.
#
# Run with `--help` for usage.


import argparse
import dbus
import json
import logging
import os
import re
import subprocess
import sys


NAME_REGEX = re.compile('https?://([^/]+)')
RPC_PASSWORD_FILE = '/etc/mirrorbits-rpc-password'
RPC_PASSWORD = None
LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'
LOGGER = logging.getLogger(__name__)


# If there's a password configured in /etc, we want to use it.
try:
    with open(RPC_PASSWORD_FILE, 'r') as f:
        RPC_PASSWORD = f.read()
except FileNotFoundError:
    pass


def _mirrorbits(args):
    """
    Execute mirrorbits with arguments.

    If RPC_PASSWORD is configured, call the Mirrorbits binary with `-a` and
    pass the RPC password via STDIN.
    """
    if RPC_PASSWORD:
        args = [ '-a' ] + args
    p = subprocess.run(
            [ 'mirrorbits' ] + args,
            capture_output=True,
            input=f'{RPC_PASSWORD}\n'.encode() if RPC_PASSWORD else b'')
    try:
        p.check_returncode()
        return p.stdout.decode()
    except subprocess.CalledProcessError as e:
        LOGGER.error(f'Mirrorbits exited with status code {p.returncode}: {p.stderr.decode()}')
        raise e


def fetch_extra_data(name):
    """
    Fetch extra data which is not dumped by `mirrorbits list`.
    """
    output = _mirrorbits(['show', name])
    comment = False
    for l in output.split('\n'):
        if not comment:
            if 'AdminEmail' in l:
                email = l.strip().split(' ')[1]
            if 'Comment' in l:
                comment = True
        else:
            comment = l.strip()
            break
    return email, comment


def get_expected_mirrors(mirrors):
    """
    Return the set of expected mirrors, according to mirrors.json.
    """
    expected = set()
    for m in filter(lambda i: 'rsync_url' in i, mirrors):
        if m['weight'] == 0:
            # Behave as the old JS dispatcher and do not redirect to mirrors with weight = 0.
            continue
        name = NAME_REGEX.match(m['url_prefix']).groups()[0]
        LOGGER.debug(f'Found expected mirror: {name}')
        notes = m['notes'] if 'notes' in m else ''
        expected.add((name, m['email'], m['weight'], m['url_prefix'], m['rsync_url'], notes))
    return expected


def get_current_mirrors():
    """
    Return a set containing one tuple per currently configured mirror in
    Mirrorbits.
    """
    current = set()
    output = _mirrorbits(['list', '-score', '-http', '-rsync', '-state=false'])
    for l in output.split('\n')[1:-1]:
        name, score, http, rsync = re.match('([^\s]+)[\s]+([^\s]+)\s+([^\s]+)\s+([^\s]+)', l).groups()
        LOGGER.debug(f'Found currently configured mirror: {name}')
        # some important data is not dumped by `mirrorbits list`
        email, comment = fetch_extra_data(name)
        current.add((name, email, int(score), http, rsync, comment))
    return current


def remove_mirror(mirror):
    """
    Remove a mirror from Mirrorbits.
    """
    name = mirror[0]
    LOGGER.info(f'Removing mirror: {name}')
    _mirrorbits(['remove', '-f', name])


def add_mirror(mirror):
    """
    Add a mirror to Mirrorbits.
    """
    name, email, score, http, rsync, comment = mirror
    LOGGER.info(f'Adding mirror: {name}')
    _mirrorbits(['add',
        '-admin-email', email,
        '-score', str(score),
        '-http', http,
        '-rsync', rsync,
        '-comment', comment,
        name])
    _mirrorbits(['enable', name])


def restart_service():
    """
    Restart the Mirrorbits service via Systemd.
    """
    sysbus = dbus.SystemBus()
    systemd1 = sysbus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
    manager = dbus.Interface(systemd1, 'org.freedesktop.systemd1.Manager')
    manager.RestartUnit('mirrorbits.service', 'fail')


def update_mirrors(mirrors, restart_on_changes=False):
    """
    Update Mirrorbits to contain exactly the list of mirrors passed as an
    argument. Optionally, restart the Mirrorbits service if changes are
    made.
    """
    expected = get_expected_mirrors(mirrors)
    current = get_current_mirrors()

    for m in current - expected:
        remove_mirror(m)
    for m in expected - current:
        add_mirror(m)

    if expected != current and restart_on_changes:
        restart_service()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description='Read `mirrors.json` from STDIN or file path and update Mirrorbits mirrors.')
    parser.add_argument('-i', '--infile',type=argparse.FileType('r'), help='Path to `mirrors.json`.')
    parser.add_argument('-d', '--debug', action='store_true', help='Print debug messages.')
    parser.add_argument('-r', '--restart-on-changes', action='store_true',
                        help='Restart the Mirrorbits service when there are changes to mirrors.')
    args = parser.parse_args()
    level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=level, format=LOG_FORMAT)

    if args.infile:
        mirrors = json.loads(args.infile.read())['mirrors']
    else:
        mirrors = json.load(sys.stdin)['mirrors']

    update_mirrors(mirrors, restart_on_changes=args.restart_on_changes)
