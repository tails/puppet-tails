#!/usr/bin/python3

# This script is used by:
#
#  - the *build_IUKs Jenkins jobs
#  - Release Managers, as part of the Tails release process

import argparse
import logging
import os
import shutil
import subprocess
import sys

from multiprocessing import Pool
from pathlib import Path

ISO_HISTORY_URL = 'https://iso-history.tails.boum.org'
TAILS_GIT_REMOTE = 'https://gitlab.tails.boum.org/tails/tails.git'
LOG_FORMAT = "%(asctime)-15s %(levelname)s %(message)s"

log = logging.getLogger()


def main():
    parser = argparse.ArgumentParser(description="Build IUKs")
    parser.add_argument("--tails-git-commit", type=str,
                        action="store",
                        default="HEAD")
    parser.add_argument("--tails-git-remote", type=str,
                        action="store",
                        default=TAILS_GIT_REMOTE)
    parser.add_argument("--use-existing-clone",
                        action="store_true",
                        default=False)
    parser.add_argument("--source-date-epoch", type=int,
                        action="store",
                        help="SOURCE_DATE_EPOCH")
    parser.add_argument("--iso-history-url", type=str,
                        action="store",
                        default=ISO_HISTORY_URL)
    parser.add_argument("--local-isos-dir", type=str,
                        action="store",
                        default=os.getenv("ISOS", default="."))
    parser.add_argument("--tmp-dir", type=str,
                        action="store",
                        default=os.getenv("TMPDIR", default="/tmp"))
    parser.add_argument("--output-dir", type=str,
                        action="store",
                        default=os.getenv("IUKS_DIR", default="."))
    parser.add_argument("--source-versions", type=str,
                        action="store")
    parser.add_argument("--new-version", type=str,
                        action="store")
    parser.add_argument("--create-iuk-extra-args", type=str,
                        action="store",
                        default="")
    parser.add_argument("--jobs", type=int,
                        action="store",
                        default=1)
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="verbose output")
    parser.add_argument("--debug", action="store_true", help="debug output")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stderr,
                            format=LOG_FORMAT)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr,
                            format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.WARN, stream=sys.stderr,
                            format=LOG_FORMAT)

    set_up_git_clone(remote=args.tails_git_remote,
                     commit=args.tails_git_commit,
                     use_existing_clone=args.use_existing_clone)
    Path(args.output_dir).resolve().mkdir(parents=True, exist_ok=True)
    get_iso(
        iso_history_url=args.iso_history_url,
        local_isos_dir=args.local_isos_dir,
        version=args.new_version
    )
    for source_version in args.source_versions.split(" "):
        get_iso(
            iso_history_url=args.iso_history_url,
            local_isos_dir=args.local_isos_dir,
            version=source_version)
    with Pool(processes=args.jobs) as pool:
        log.info("Running up to %d jobs at once" % args.jobs)
        lock_file = os.path.join(args.tmp_dir, 'gensquashfs-%d.lock' % os.getpid())
        if args.jobs > 1:
            args.create_iuk_extra_args += ' --gensquashfs-lock-file=%s' % lock_file
        for source_version in args.source_versions.split(" "):
            pool.apply_async(create_iuk, (), {
                'source_date_epoch': args.source_date_epoch,
                'source_version': source_version,
                'new_version': args.new_version,
                'output_dir': args.output_dir,
                'tmp_dir': args.tmp_dir,
                'local_isos_dir': args.local_isos_dir,
                'extra_args': args.create_iuk_extra_args,
                'parallel': args.jobs > 1,
            })
        pool.close()
        pool.join()
        subprocess.run(["sudo", "rm", '-f', lock_file], check=True)


def set_up_git_clone(remote, commit, use_existing_clone):
    log.info("Setting up %s at %s" % (remote, commit))
    repo_name = "tails"
    if not use_existing_clone:
        if Path(repo_name).exists():
            subprocess.run(["rm", "-rf", repo_name], check=True)
        subprocess.run(["git", "clone", remote, repo_name], check=True)
    subprocess.run(["git", "reset", "--hard", commit], check=True,
                   cwd=repo_name)


def iso_filename(version):
    return("tails-amd64-" + version + ".iso")


def relative_iso_path(version):
    return("tails-amd64-" + version + "/" + iso_filename(version))


def find_iso(version, local_isos_dir):
    candidate_dirs = [local_isos_dir, "."]
    for candidate_dir in candidate_dirs:
        for candidate_path in [
                candidate_dir + "/" + relative_iso_path(version),
                candidate_dir + "/" + iso_filename(version)]:
            if Path(candidate_path).exists():
                return(str(Path(candidate_path).resolve()))
    return False


def get_iso(iso_history_url, local_isos_dir, version):
    found_iso = find_iso(version, local_isos_dir)
    if found_iso:
        log.info("Found %s" % (found_iso))
        return
    else:
        url = iso_history_url + "/" + relative_iso_path(version)
        log.info("Downloading %s" % (url))
        subprocess.run(["wget", "--no-verbose", url], check=True)


def create_iuk(source_date_epoch, source_version, new_version, local_isos_dir,
               output_dir, tmp_dir, extra_args, parallel):
    output_file = str(
        Path(output_dir).resolve().joinpath(
            "Tails_amd64_" + source_version + "_to_" + new_version + ".iuk"
        ))

    new_iso_orig = find_iso(new_version, local_isos_dir)
    if parallel:
        new_iso_copy = str(
            Path(tmp_dir).resolve().joinpath(
                "Tails_amd64_" + new_version + "_from_" + source_version + ".iso"
            ))
        log.info("Duplicating %s image for %s processing" %
                 (new_version, source_version))
        shutil.copy(new_iso_orig, new_iso_copy)
    else:
        log.info("Using original %s image for %s processing" %
                 (new_version, source_version))
        new_iso_copy = new_iso_orig

    log.info("Creating %s" % (output_file))
    cmd = "sudo" + \
        " SOURCE_DATE_EPOCH='%s'" % source_date_epoch + \
        " LC_ALL=C" + \
        " TMPDIR='%s'" % tmp_dir + \
        " PERL5LIB=tails/config/chroot_local-includes/usr/src/perl5lib/lib" + \
        " ./tails/config/chroot_local-includes/usr/src/iuk/bin/tails-create-iuk" + \
        " --squashfs-diff-name '" + new_version + ".squashfs'" + \
        " --old-iso '" + find_iso(source_version, local_isos_dir) + "'" + \
        " --new-iso '" + new_iso_copy + "'" \
        " --outfile '" + output_file + "'"
    if extra_args:
        cmd = cmd + " " + extra_args
    log.debug("Running command %s" % cmd)
    subprocess.run(cmd, check=True, shell=True)
    subprocess.run([
        "sudo", "chown",
        "%s:%s" % (os.getuid(), os.getgid()),
        output_file
    ], check=True)

    if parallel:
        log.info("Removing copy of %s image" % new_version)
        os.unlink(new_iso_copy)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Exception as e:
        print(e, file=sys.stderr)
        sys.exit(1)
