# @summary
#   profile for ssh keymaster
#
# @param keynames
#   array with the names of all keys that should be created
#
class tails::profile::sshkeymaster (
  Array $keynames = [],
) {
  include sshkeys::keymaster

  $keynames.each | String $keyname | {
    sshkeys::create_key { $keyname:
      keytype => 'rsa',
      length  => 4096,
    }
  }

  Sshkeys::Create_key <<| tag == 'ssh_keymaster' |>>
}
