# @summary
#   profile to configure mountpoints
#
# @param mounts
#   hash with all mountpoints
#
class tails::profile::mounts (
  Hash $mounts = {},
) {
  $mounts.each | String $mountpoint, Hash $mountparams | {
    mount { $mountpoint:
      *      => $mountparams,
      before => Class['tails::profile::base'],
    }
  }
}
