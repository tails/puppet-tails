# @summary
#   Manage a fact file under the custom facts directory.
#
# @param content
#   The content of the fact file, needs to be convertible to YAML.
define tails::profile::facts_d::file (
  Hash $content,
) {
  file { "/etc/facter/facts.d/${title}.yaml":
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => stdlib::to_yaml($content),
    require => File['/etc/facter/facts.d'],
  }
}
