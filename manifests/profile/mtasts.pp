# @summary
#   Serve MTA-STS policy file.
#
# @param hostname
#   the hostname
#
# @param mx
#   the mailserver's hostname
#
class tails::profile::mtasts (
  Stdlib::Fqdn $hostname = 'mta-sts.tails.net',
  Stdlib::Fqdn $mx       = 'mta.tails.net',
) {
  file { "/srv/${hostname}":
    ensure => directory,
  }
  file { "/srv/${hostname}/.well-known":
    ensure  => directory,
    require => File["/srv/${hostname}"],
  }
  file { "/srv/${hostname}/.well-known/mta-sts.txt":
    ensure  => file,
    content => "version: STSv1
mode: enforce
mx: ${mx}
max_age: 86400",
    require => File["/srv/${hostname}/.well-known"],
  }

  tails::profile::nginx::server { $hostname:
    ssl      => true,
    www_root => "/srv/${hostname}",
    require  => File["/srv/${hostname}"],
  }
}
