# @summary
#   profile to manage borg backups of logical volumes
#
# @param backupserver
#   server to write backups to
#
# @param lvs
#   hash with all logical volumes to back up
#
class tails::profile::backuplvs (
  String $backupserver,
  Hash   $lvs,
) {
  $lvs.each | String $vg, Hash $job | {
    $job.each | String $lv, Hash $params | {
      borgbackup::backup_lv { "${vg}-${lv}":
        backupserver => $backupserver,
        vg           => $vg,
        lv           => $lv,
        *            => $params,
      }
    }
  }
}
