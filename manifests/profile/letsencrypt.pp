# @summary
#   Wrapper around the letsencrypt class
#
# @param config
#   extra letsencrypt configuration
#
# @param default_config
#   the default letsencrypt configuration
#
class tails::profile::letsencrypt (
  Hash $config = {},
  Hash $default_config  = {
    'agree-tos'           => 'True',
    'authenticator'       => 'webroot',
    'keep-until-expiring' => 'True',
    'post-hook'           => '/bin/systemctl reload nginx.service',
    'rsa-key-size'        => 4096,
    'server'              => 'https://acme-v02.api.letsencrypt.org/directory',
    'webroot-path'        => '/var/www/html',
  },
) {
  include tails::profile::sysadmins

  # Packages

  ensure_packages(['gnutls-bin']) # ships certtool

  package { 'certbot':
    ensure  => installed,
  }

  # Other resources

  class { 'letsencrypt':
    email           => $tails::profile::sysadmins::email,
    manage_install  => false,
    package_command => 'certbot',
    config          => $default_config + $config,
    require         => Package['certbot'],
  }
}
