# @summary
#   Configure basic networking
#
# @example
#   include tails::profile::network
#
# @param interfaces
#   A Hash used to declare network interface resources.
#
# @param export_host
#   Whether or not we export our hostname and IP address for the other
#   nodes to collect. This only works when only 1 interface is 
#   configured.
#
class tails::profile::network (
  Hash $interfaces     = {},
  Boolean $export_host = true,
) {
  service { 'networking':
    ensure => 'running',
  }

  class { 'network':
    ipaddress          => 'ruby-ipaddress',
    ipaddress_provider => 'apt',
    ensure_ipaddress   => 'present',
  }

  create_resources('network_config', $interfaces)

# if we only have 1 interface defined here, we can be sure this is the
# IP we want to share for other hosts to add to their /etc/hosts file.

  if $export_host {
    if $interfaces.length == 1 {
      $interfaces.each | String $iface, Hash $config | {
        @@host { $facts['networking']['fqdn']:
          ip           => $config['ipaddress'],
          host_aliases => $facts['networking']['hostname'],
          tag          => 'hello_neighbour',
        }
      }
    }
  }
}
