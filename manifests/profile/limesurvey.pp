# @summary
#   profile to manage a LimeSurvey service
#
# @param db_password
#   the database password
#
# @param db_user
#   the database user
#
# @param db_name
#   the database name
#
# @param apache_data_dir
#   the webroot
#
# @param mutable_data_dir
#   homedir for limesurvey_admin
#
# @param upstream_git_remote
#   the upstream git repository
#
# @param upstream_git_local
#   path to our local clone of upstream git repo
#
# @param monitor_repo_origin
#   where to get the monitoring scripts from
#
# @param monitor_repo_rev
#   which revision of the monitoring scripts we want
#
class tails::profile::limesurvey (
  String $db_password,
  String $db_user                          = 'limesurvey',
  String $db_name                          = 'limesurvey',
  Stdlib::Absolutepath $apache_data_dir    = '/var/www/limesurvey',
  Stdlib::Absolutepath $mutable_data_dir   = '/var/lib/limesurvey',
  String $upstream_git_remote              = 'https://github.com/LimeSurvey/LimeSurvey.git',
  Stdlib::Absolutepath $upstream_git_local = '/var/lib/limesurvey/git',
  String $monitor_repo_origin              = 'https://gitlab.tails.boum.org/tails/monitor-limesurvey-releases.git',
  String $monitor_repo_rev                 = 'master',
) {
  ### Sanity checks

  if $facts['os']['name'] != 'Debian' or versioncmp($facts['os']['distro']['release']['major'], '11') < 0 {
    fail('The tails::limesurvey class only supports Debian Bullseye or higher.')
  }

  ### Resources

  # Packages

  $packages = [
    'apache2',
    'libapache2-mod-removeip',
    'libdpkg-perl',
    'libgit-repository-perl',
    'libmethod-signatures-simple-perl',
    'libmoo-perl',
    'libmoox-options-perl',
    'libpath-tiny-perl',
    'libstrictures-perl',
    'libtry-tiny-perl',
    'libtype-tiny-perl',
    'ncdu',
    'php',
    'php-gd',
    'php-imagick',
    'php-mysql',
    'php-mbstring',
    'php-xml',
    'php-zip',
  ]
  ensure_packages ($packages)

  # Users and groups

  user { 'limesurvey':
    ensure => present,
    system => true,
    home   => $mutable_data_dir,
    gid    => 'limesurvey',
    shell  => '/bin/bash',
  }

  group { 'limesurvey':
    ensure => present,
    system => true,
  }

  # Mutable data

  file { $mutable_data_dir:
    ensure => directory,
    owner  => limesurvey,
    group  => limesurvey,
    mode   => '2755',
  }

  # Code

  exec { 'clone upstream Git repository':
    command => "cd / && git clone --mirror '${upstream_git_remote}' '${upstream_git_local}'",
    user    => limesurvey,
    creates => $upstream_git_local,
  }

  file { $apache_data_dir:
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0750',
    require => Package['apache2'],
  }

  exec { 'initialize production Git working copy':
    command => "git clone '${upstream_git_local}' '${apache_data_dir}'",
    user    => 'www-data',
    creates => "${apache_data_dir}/.git",
    require => [
      File[$apache_data_dir],
      Exec['clone upstream Git repository'],
    ],
  }

  # Service

  service { 'apache2':
    ensure  => running,
    require => Package['apache2'],
  }

  # Configuration

  file { "${mutable_data_dir}/config":
    ensure => directory,
    owner  => root,
    group  => 'limesurvey_admin',
    mode   => '2775',
  }

  file { "${mutable_data_dir}/config/apache-vhost.conf":
    ensure => file,
    owner  => root,
    group  => 'limesurvey_admin',
    mode   => '0664',
  }

  file { '/etc/apache2/sites-available/000-default.conf':
    ensure => symlink,
    target => "${mutable_data_dir}/config/apache-vhost.conf",
    notify => Service['apache2'],
  }

  exec { 'a2enmod removeip':
    creates => '/etc/apache2/mods-enabled/removeip.load',
    require => Package['libapache2-mod-removeip'],
  }

  # Database

  class { 'mysql::server':
    override_options => {
      'mysqld' => {
        'sql_mode' => 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,STRICT_ALL_TABLES',
        'ssl_ca'   => undef,
        'ssl_cert' => undef,
        'ssl_key'  => undef,
      },
    },
  }

  mysql::db { $db_name:
    charset  => 'utf8mb4',
    collate  => 'utf8mb4_unicode_ci',
    user     => $db_user,
    password => $db_password,
    host     => 'localhost',
    grant    => ['ALL'],
  }

  # Backups

  include backupninja
  backupninja::mysql { 'backup-limesurvey-db':
    ensure => present,
  }

  # Releases monitoring

  $clone_basedir = "${mutable_data_dir}/monitor-releases"

  file { $clone_basedir:
    ensure => directory,
    owner  => 'limesurvey',
    group  => 'limesurvey',
    mode   => '0755',
  }

  vcsrepo { "${clone_basedir}/git":
    provider => git,
    source   => $monitor_repo_origin,
    revision => $monitor_repo_rev,
    user     => 'limesurvey',
    require  => File[$clone_basedir],
  }

  include tails::profile::sysadmins

  cron { 'tails-limesurvey-releases-monitor':
    user        => 'limesurvey',
    hour        => '*/8',
    minute      => '27',
    command     => "${clone_basedir}/git/bin/tails-monitor-limesurvey-releases --git_dir=${mutable_data_dir}/git --last_checked_cache_file=${clone_basedir}/last_checked_tag", # lint:ignore:140chars -- command
    require     => [Package[$packages], Vcsrepo["${clone_basedir}/git"]],
    environment => ["MAILTO=${tails::profile::sysadmins::email}"],
  }
}
