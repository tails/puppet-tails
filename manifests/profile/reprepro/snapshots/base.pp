# @summary
#   Resources that are shared between tails::profile::reprepro::snapshots::* classes.
#
# @param user
#   the user to run 
#
# @param homedir
#   this user's homedir
#
# @param repositories_dir
#   this repositories directory
#
# @param ensure
#   NB: absent doesnt properly clean up
#
define tails::profile::reprepro::snapshots::base (
  Stdlib::Absolutepath $homedir,
  Stdlib::Absolutepath $repositories_dir,
  String $user,
  Enum['present', 'absent'] $ensure = present,
) {
  ### Sanity checks

  assert_private()

  if !defined(Class['reprepro']) {
    fail('Depends on the reprepro class')
  }

  ### Resources

  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }

  user { $user:
    ensure   => $ensure,
    home     => $homedir,
    gid      => $user,
    password => '*',
    comment  => 'Tails snapshots of APT repositories',
    system   => true,
    require  => Group[$user],
  }

  group { $user:
    ensure => $ensure,
    system => true,
  }

  file { $homedir:
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0751',
  }

  file { $repositories_dir:
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0755',
  }

  file { "${repositories_dir}/robots.txt":
    ensure  => $ensure,
    owner   => $user,
    group   => $user,
    mode    => '0644',
    content => "User-agent: *\nDisallow: /\n",
  }

  # reprepro::repository manages the permissions of its own $homedir/.gnupg
  # (that is, a subdirectory of our $homedir), but not those of ~/.gnupg
  file { "${homedir}/.gnupg":
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }
}
