# @summary
#   Manage tagged snapshots of the set of APT repositories Tails needs,
#   in a reprepro setup.
#
# @param ensure
#   NB: absent probably doesn't clean up very well
#
# @param user
#   the user to run this repo as
#
# @param homedir
#   this user's homedir
#
# @param email_recipient
#   whom to mail
#
# @param web_hostname
#   the hostname
#
# @param web_port
#   the http port
#
class tails::profile::reprepro::snapshots::tagged (
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Absolutepath $homedir     = '/srv/apt-snapshots/tagged',
  String $email_recipient           = 'root',
  String $user                      = 'reprepro-tagged-snapshots',
  Stdlib::Fqdn $web_hostname        = 'tagged.snapshots.deb.tails.net',
  Stdlib::Port $web_port            = 80,
) {
  $repositories_dir = "${homedir}/repositories"

  $package_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'present',
  }

  tails::profile::reprepro::snapshots::base { 'tagged':
    ensure           => $ensure,
    homedir          => $homedir,
    repositories_dir => $repositories_dir,
    user             => $user,
  }

  $prepare_tagged_snapshot_import_pkg_deps = [
    libfile-slurp-perl,
    libyaml-libyaml-perl,
    liblist-compare-perl,
    liblist-moreutils-perl,
    libdpkg-perl,
  ]
  ensure_packages(
    $prepare_tagged_snapshot_import_pkg_deps,
    { 'ensure' => $package_ensure }
  )

  file { '/usr/local/bin/tails-prepare-tagged-apt-snapshot-import':
    ensure  => $ensure,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/reprepro/snapshots/tagged/tails-prepare-tagged-apt-snapshot-import',
    require => Package[$prepare_tagged_snapshot_import_pkg_deps],
  }

  tails::profile::reprepro::snapshots::tagged::nginx { $web_hostname:
    www_root => "${homedir}/repositories",
  }

  tails::profile::reprepro::snapshots::tagged::nginx { 'tagged.snapshots.deb.tails.boum.org':
    www_root => "${homedir}/repositories",
  }

  mailalias { $user:
    recipient => $email_recipient,
  }

  # De-duplicate with hardlinks
  ensure_packages(hardlink, { 'ensure' => $package_ensure })
  cron { 'deduplicate-tagged-apt-snapshots':
    ensure  => $ensure,
    command => "output=\$(hardlink --keep-oldest --ignore-time '${repositories_dir}'); ret=\$?; [ \$ret = 0 ] || printf \"\\%s\" \"\$output\"; exit \$ret", # lint:ignore:140chars -- command
    hour    => 12,
    minute  => 17,
    user    => $user,
    require => Package['hardlink'],
  }
}
