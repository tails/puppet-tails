# @summary
#   Web server definitions for time-based snapshots website
#
# @param www_root
#   the webroot
#
define tails::profile::reprepro::snapshots::time_based::nginx (
  Stdlib::Absolutepath $www_root,
) {
  tails::profile::nginx::server { $name:
    www_root           => $www_root,
    server_cfg_prepend => { 'fancyindex' => 'on' },
    locations          => {
      "${name} deny sensitive"                    => {
        index_files   => [],
        location      => '~ ^/(conf|db|incoming|logs|tmp)/',
        location_deny => ['all'],
        www_root      => undef,
      },
      "${name} MIME type of trace files"          => {
        index_files         => [],
        location            => '~ ^/[a-z-]+/project/trace/[a-z-]+$',
        location_cfg_append => { 'default_type' => 'text/plain' },
        www_root            => undef,
        priority            => 501,
      },
      "${name} MIME type of APT repository files" => {
        index_files         => [],
        location            => '~ /(Release|InRelease|Packages|Sources)$',
        location_cfg_append => { 'default_type' => 'text/plain' },
        www_root            => undef,
        priority            => 502,
      },
    },
    raw_append         => [
      '# Map URL namespace to filesystem namespace',
      'rewrite ^/([a-z-]+)/([0-9]+)/dists/([^/]+(?:/updates)?)/(.*)$ /$1/dists/$3/snapshots/$2/$4 permanent;',
      'rewrite ^/([a-z-]+)/[0-9]+/pool/(.*)$ /$1/pool/$2 permanent;',
    ],
    require            => [
      Package['libnginx-mod-http-fancyindex'],
    ],
  }
}
