# @summary
#   Manage time-based snapshots of one APT repository in a reprepro setup.
#
#   This resource is meant to be used from tails::profile::reprepro::snapshots::time_based,
#   and is not designed to be used in isolation.
#
#   Resource name: the upstream repository we are mirroring.
#
# @param ensure
#   NB: the ensure parameter is not fully supported
#
# @param architectures
#   the architectures
#
# @param basedir
#   the repo's basedir
#
# @param signwith
#   the key to sign with
#
# @param user
#   the user to run this repo as
#
# @param homedir
#   this user's homedir
#
# @param automatic_refresh
#   whether to refresh automagically
#
# @param automatic_refresh_delta_hours
#   the refresh rate: hours
#
# @param automatic_refresh_delta_mins
#   the refresh rate: minutes
#
# @param distributions_template
#   template for the distributions file
#
# @param updates_template
#   template for the updates file
#
define tails::profile::reprepro::snapshots::time_based::repository (
  Hash $architectures,
  Stdlib::Absolutepath $basedir,
  Stdlib::Absolutepath $homedir,
  String $signwith,
  String $user,
  Enum['present', 'absent'] $ensure             = 'present',
  Enum['present', 'absent'] $automatic_refresh  = 'present',
  Integer[0, 23] $automatic_refresh_delta_hours = 0,
  Integer[0, 59] $automatic_refresh_delta_mins  = 0,
  Pattern[/\A\S+\z/] $distributions_template    = "tails/reprepro/snapshots/time_based/${name}/distributions.erb",
  Pattern[/\A\S+\z/] $updates_template          = "tails/reprepro/snapshots/time_based/${name}/updates.erb",
) {
  ### Sanity checks

  assert_private()

  if $name !~ /^[a-zA-Z-]+$/ {
    fail('Repository names must consist of letters and hyphens only.')
  }

  ### Resources

  $repository = $name

  reprepro::repository { "tails-time-based-snapshots-${repository}":
    architectures                => $architectures,
    user                         => $user,
    group                        => $user,
    uploaders                    => [],
    basedir                      => $basedir,
    origin                       => "Time-based snapshots of ${repository} APT repository",
    basedir_mode                 => '0751',
    incoming_mode                => '0700',
    signwith                     => $signwith,
    manage_distributions_conf    => true,
    distributions_template       => $distributions_template,
    manage_incoming_conf         => false,
    handle_incoming_with_inotify => false,
    manage_updates_conf          => true,
    updates_template             => $updates_template,
    index_template               => 'tails/reprepro/index.html.erb',
  }

  # Exec[/usr/local/bin/reprepro-export-key ...] is run as long as this file
  # does not exist. Unfortunately, that script (and much of the reprepro
  # Puppet module, really) assumes that $HOME == $basedir, which is not the case
  # for us, so it will never succeed at exporting the key. Let's prevent that
  # script from ever running, we don't need what it produces anyway.
  file { "${basedir}/key.asc":
    ensure => $ensure,
    owner  => root,
    group  => $user,
    mode   => '0440',
  }

  $service_ensure = $ensure ? {
    'present' => $automatic_refresh,
    'absent'  => 'absent',
  }

  $directory_ensure = $ensure ? {
    'present' => 'directory',
    'absent'  => 'absent',
  }

  tails::profile::reprepro::snapshots::time_based::service { "${repository}-1":
    ensure      => $service_ensure,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 1,
    repository  => $repository,
    basedir     => $basedir,
    homedir     => $homedir,
  }
  tails::profile::reprepro::snapshots::time_based::service { "${repository}-2":
    ensure      => $automatic_refresh,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 2,
    repository  => $repository,
    basedir     => $basedir,
    homedir     => $homedir,
  }
  tails::profile::reprepro::snapshots::time_based::service { "${repository}-3":
    ensure      => $service_ensure,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 3,
    repository  => $repository,
    basedir     => $basedir,
    homedir     => $homedir,
  }
  tails::profile::reprepro::snapshots::time_based::service { "${repository}-4":
    ensure      => $service_ensure,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 4,
    repository  => $repository,
    basedir     => $basedir,
    homedir     => $homedir,
  }

  tails::profile::reprepro::snapshots::secret_keys { "time_based_${name}":
    basedir => $basedir,
    homedir => $homedir,
    user    => $user,
  }

  file { "${basedir}/project":
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0755',
  }

  file { [
      "${basedir}/project/packages.db.size",
      "${basedir}/project/references.db.size",
    ]:
      ensure => $ensure,
      owner  => $user,
      group  => $user,
      mode   => '0644',
  }
}
