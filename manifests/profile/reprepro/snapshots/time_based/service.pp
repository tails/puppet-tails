# @summary
#   Manage a Systemd service that updates time-based snapshots of APT
#   repositories in a reprepro setup.
#   This defined resource is meant to be declared up to 4 times on a given
#   system.
#   The serial number of the generated snapshots uses the configured system
#   timezone, which should be set to UTC for better consistency with other date
#   information embedded e.g. in the mirrored APT repositories.
#
# @param repository
#   the repo to update
#
# @param basedir
#   the repo's basedir
#
# @param homedir
#   the homedir
#
# @param id
#   must be an integer among [1..4].
#
# @param delta_hours
#   how often to update: hours
#
# @param delta_mins
#   how often to update: minutes
#
# @param ensure
#   whether the cronjob should be there or not
#
# @param timeout
#   when to time out
#
# @param mailto
#   whom to mail in case of error
#
define tails::profile::reprepro::snapshots::time_based::service (
  Pattern[/\A[a-zA-Z-]+\z/] $repository,
  Stdlib::Absolutepath $basedir,
  Stdlib::Absolutepath $homedir,
  Integer[1, 4] $id,
  Integer[0, 23] $delta_hours       = 0,
  Integer[0, 59] $delta_mins        = 0,
  Enum['present', 'absent'] $ensure = 'present',
  Integer $timeout                  = 1200,
  String $mailto                    = 'foundations@tails.net',
) {
  $id_str = String($id)

  $enable = $ensure ? { 'absent' => false, default => true }
  $update_hour = ($delta_hours + 6 * ($id - 1)) % 24
  $update_minute = $delta_mins % 60
  $update_name = "tails-update-time-based-apt-snapshots-${repository}-${id_str}"

  systemd::timer { "${update_name}.timer":
    ensure          => $ensure,
    active          => $enable,
    enable          => $enable,
    # lint:ignore:140chars
    service_content => @("EOT"/$),
      [Unit]
      Description=Take time-based snapshot of ${repository}
      After=network.target

      [Service]
      User=reprepro-time-based-snapshots
      Group=reprepro-time-based-snapshots
      Type=oneshot
      TimeoutStopSec=${timeout}
      KillSignal=SIGCONT
      Environment=SILENT=1
      Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
      ExecStart=flock -n '/var/lock/tails-delete-expired-apt-snapshots-${repository}' tails-delete-expired-apt-snapshots '${basedir}'
      ExecStart=flock -n '/var/lock/tails-update-time-based-apt-snapshots-${repository}' /bin/sh -c 'tails-update-time-based-apt-snapshots ${repository} ${basedir} \$(date +%%Y%%m%%d)0${id_str}'

      [Install]
      WantedBy=multi-user.target
      |EOT
    timer_content   => @("EOT"),
      [Timer]
      OnCalendar=*-*-* ${update_hour}:${update_minute}:00
      [Install]
      WantedBy=timers.target
      |EOT
    require         => [
      File['/usr/local/bin/tails-update-time-based-apt-snapshots'],
      File['/usr/local/bin/tails-delete-expired-apt-snapshots'],
      Reprepro::Repository["tails-time-based-snapshots-${repository}"],
    ],
    # lint:endignore
  }

  cron { "tails-stats-time-based-apt-snapshots-${repository}-${id_str}":
    ensure      => $ensure,
    environment => ["MAILTO=${mailto}"],
    command     => "stat --format='\\%s' '${basedir}/db/packages.db' > '${basedir}/project/packages.db.size' && stat --format='\\%s' '${basedir}/db/references.db' > '${basedir}/project/references.db.size'", # lint:ignore:140chars -- command
    user        => 'reprepro-time-based-snapshots',
    hour        => ($delta_hours + 6 * ($id - 1)) % 24,
    minute      => ($delta_mins - 10) % 60,
    require     => [
      File["${basedir}/project/packages.db.size"],
      File["${basedir}/project/references.db.size"],
    ],
  }
}
