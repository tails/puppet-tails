# @summary
#   Import GnuPG pubkeys from a source directory.
#
#   We don't try to avoid cluttering the target user's home directory
#   here, which is fine for the usecase at hand. This should be fixed if
#   we ever want to refactor this defined resource into a more
#   generic one.
#
#   As is, this is not meant to be used elsewhere than in
#   tails::profile::reprepro::snapshots::time_based.
#
# @param source
#   the source of the public keys
#
# @param user
#   the target user
#
# @param homedir
#   the target user's homedir
#
# @param ensure
#   NB: setting this to absent doesn't remove the keys from the chain
#
define tails::profile::reprepro::snapshots::time_based::import_upstream_keys (
  String $source,
  String $user,
  Stdlib::Absolutepath $homedir,
  Enum['present', 'absent'] $ensure = 'present',
) {
  ### Sanity checks
  assert_private()

  ### Resources

  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }

  $upstream_keys_dir = "${homedir}/${name}"

  file { $upstream_keys_dir:
    ensure  => $directory_ensure,
    recurse => true,
    owner   => $user,
    group   => $user,
    mode    => '0644',
    source  => $source,
  }

  exec { "tails-reprepro-snapshots-time_based-import-upstream-keys-${name}":
    user        => $user,
    group       => $user,
    command     => "gpg --batch --quiet --import '${upstream_keys_dir}'/*.asc",
    subscribe   => File[$upstream_keys_dir],
    refreshonly => true,
    require     => File["${homedir}/.gnupg"],
  }
}
