# @summary
#   Web server definitions for tagged snapshots website
#
# @param www_root
#   the webroot
#
define tails::profile::reprepro::snapshots::tagged::nginx (
  Stdlib::Absolutepath $www_root,
) {
  tails::profile::nginx::server { $name:
    index_files        => [],
    www_root           => $www_root,
    server_cfg_prepend => { 'fancyindex' => 'on' },
    locations          => {
      "${name} deny sensitive"                    => {
        index_files   => [],
        location      => '~ ^/(conf|db|incoming|logs|tmp)/',
        location_deny => ['all'],
        www_root      => undef,
      },
      "${name} MIME type of APT repository files" => {
        index_files         => [],
        location            => '~ /(Release|InRelease|Packages|Sources)$',
        location_cfg_append => { 'default_type' => 'text/plain' },
        www_root            => undef,
        priority            => 501,
      },
    },
    require            => [
      Package['libnginx-mod-http-fancyindex'],
    ],
  }
}
