# @summary
#   Manage all reverse proxies for Tails APT repositories
#
class tails::profile::reprepro::reverse_proxies (
) {
  tor::daemon::onion_service { 'http-hidden-v3':
    ports   => ['80'],
  }

  tails::profile::reprepro::reverse_proxy { 'deb.tails.boum.org': }
  tails::profile::reprepro::reverse_proxy { 'time-based.snapshots.deb.tails.boum.org': }
  tails::profile::reprepro::reverse_proxy { 'tagged.snapshots.deb.tails.boum.org': }

  tails::profile::reprepro::reverse_proxy { 'deb.tails.net': }
  tails::profile::reprepro::reverse_proxy { 'time-based.snapshots.deb.tails.net': }
  tails::profile::reprepro::reverse_proxy { 'tagged.snapshots.deb.tails.net': }
  tails::profile::reprepro::reverse_proxy { 'umjqavufhoix3smyq6az2sx4istmuvsgmz4bq5u5x56rnayejoo6l2qd.onion':
    ssl => false,
  }
}
