# @summary
#   Manage Tails custom APT repository
#
# @param basedir
#   reprepro basedir
#
# @param email_recipient
#   whom to mail
#
# @param uploaders
#   users who should be able to upload
#
# @param origin
#   the origin identifier
#
# @param web_hostname
#   the public hostname
#
# @param onion_v3_hostname
#   the onion hostname
#
# @param web_port
#   the http port
#
# @param git_remote
#   the git repo
#
class tails::profile::reprepro::custom (
  Stdlib::Absolutepath $basedir = '/srv/reprepro',
  String $email_recipient       = 'root',
  Array[String] $uploaders      = [],
  String $origin                = 'Tails',
  Stdlib::Fqdn $web_hostname    = 'deb.tails.net',
  String $onion_v3_hostname     = undef,
  Stdlib::Port $web_port        = 80,
  String $git_remote            = 'https://gitlab.tails.boum.org/tails/tails.git'
) inherits tails::profile::reprepro::params {
  ### Sanity checks

  if !defined(Class['tails::profile::nginx']) {
    fail('Depends on the tails::profile::nginx class')
  }

  if !defined(Class['reprepro']) {
    fail('Depends on the reprepro class')
  }

  ### Class variables

  $git_repo = "${basedir}/tails.git"
  $shell_lib = '/usr/local/share/tails-reprepro/functions.sh'

  ### Resources

  reprepro::repository { 'tails':
    uploaders                    => $uploaders,
    basedir                      => $basedir,
    origin                       => $origin,
    basedir_mode                 => '0751',
    incoming_mode                => '1775',
    manage_distributions_conf    => false,
    manage_incoming_conf         => false,
    handle_incoming_with_inotify => true,
    index_template               => 'tails/reprepro/index.html.erb',
  }

  ensure_packages(['git', 'moreutils'])

  file {
    '/usr/local/share/tails-reprepro':
      ensure => directory,
      mode   => '0755',
      owner  => root,
      group  => reprepro;

    $shell_lib:
      mode    => '0644',
      owner   => root,
      group   => reprepro,
      source  => 'puppet:///modules/tails/reprepro/custom/functions.sh',
      require => File['/usr/local/share/tails-reprepro'];

    '/usr/local/bin/tails-diff-suites':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-diff-suites',
      require => File[$shell_lib],
      mode    => '0755',
      owner   => root,
      group   => root;

    '/usr/local/bin/tails-merge-suite':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-merge-suite',
      require => File[$shell_lib],
      mode    => '0755',
      owner   => root,
      group   => root;

    '/usr/local/bin/tails-suites-list':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-suites-list',
      require => File[$shell_lib],
      mode    => '0755',
      owner   => root,
      group   => root;

    '/usr/local/bin/tails-suites-to-distributions':
      source => 'puppet:///modules/tails/reprepro/custom/tails-suites-to-distributions',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/bin/tails-suites-to-incoming':
      source => 'puppet:///modules/tails/reprepro/custom/tails-suites-to-incoming',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/bin/tails-update-reprepro-config':
      source  => 'puppet:///modules/tails/reprepro/custom/tails-update-reprepro-config',
      require => [
        Exec['tails-reprepro-git-clone'],
        File[$shell_lib],
        File['/usr/local/bin/tails-suites-list'],
        File['/usr/local/bin/tails-suites-to-distributions'],
        File['/usr/local/bin/tails-suites-to-incoming'],
        Package['moreutils'],
      ],
      mode    => '0755',
      owner   => root,
      group   => root;

    "${basedir}/conf/deny_all_uploaders":
      mode    => '0660',
      owner   => root,
      group   => reprepro,
      content => '',
      require => File["${basedir}/conf"];
  }

  cron { 'tails-update-reprepro-config':
    user        => 'reprepro',
    minute      => '*',
    command     => "flock -n /var/lock/tails-update-reprepro-config /usr/local/bin/tails-update-reprepro-config '${git_repo}' '${origin}' '${basedir}'", # lint:ignore:140chars -- command
    require     => File['/usr/local/bin/tails-update-reprepro-config'],
    environment => ["MAILTO=${email_recipient}"],
  }

  # Can't use vcsrepo, that doesn't support --mirror before
  # https://github.com/puppetlabs/puppetlabs-vcsrepo/commit/b8f25cea95317a4b2a622e2799f1aa7ba159bdca
  # that is not part of an upstream release as of 20160523
  exec { 'tails-reprepro-git-clone':
    user    => reprepro,
    group   => reprepro,
    cwd     => $basedir,
    command => "git clone --bare --mirror '${git_remote}' '${git_repo}' && chmod -R g+rX '${git_repo}'",
    creates => "${git_repo}/config",
    require => Package['git'],
    timeout => -1,
  }

  exec { 'tails-reprepro-import-keys':
    user        => reprepro,
    group       => reprepro,
    command     => "gpg --homedir '${basedir}/.gnupg' --batch --quiet --import '${basedir}/.gnupg/keys.asc'",
    subscribe   => File["${basedir}/.gnupg/keys.asc"],
    refreshonly => true,
    notify      => Exec["/usr/local/bin/reprepro-export-key '${basedir}'"],
    require     => File["${basedir}/.gnupg/keys.asc"],
  }

  mailalias { 'reprepro': ensure => present, recipient => ['root']; }

  class { 'tails::profile::reprepro::custom::nginx':
    hostname          => $web_hostname,
    basedir           => $basedir,
    onion_v3_hostname => $onion_v3_hostname,
  }

  # Refresh OpenPGP keys

  package { ['dbus-x11', 'parcimonie']:
    ensure          => present,
    install_options => ['--no-install-recommends'],
  }

  file { '/etc/systemd/system/parcimonie-reprepro.service':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['dbus-x11', 'parcimonie'],
    content => "[Unit]
Description=Refresh reprepro's GnuPG keyring

[Service]
Type=simple
ExecStart=/usr/bin/dbus-launch /usr/bin/parcimonie --verbose
User=reprepro

[Install]
WantedBy=multi-user.target
",
  }

  service { 'parcimonie-reprepro.service':
    ensure    => running,
    enable    => true,
    provider  => systemd,
    require   => [
      File['/etc/systemd/system/parcimonie-reprepro.service'],
      File_line['set_onion_keyserver_in_dirmngr'],
    ],
    subscribe => File['/etc/systemd/system/parcimonie-reprepro.service'],
  }

  # Fix parcimonie in Stretch (see debian bug #898085)
  file { '/srv/reprepro/.gnupg/dirmngr.conf':
    ensure => file,
    owner  => reprepro,
    group  => reprepro,
    mode   => '0644',
  }

  file_line { 'set_onion_keyserver_in_dirmngr':
    path => '/srv/reprepro/.gnupg/dirmngr.conf',
    line => 'keyserver hkp://jirk5u4osbsr34t5.onion',
  }

  exec { 'shutdown_reprepro_dirmngr':
    user        => reprepro,
    command     => 'dirmngr --shutdown',
    subscribe   => File_line['set_onion_keyserver_in_dirmngr'],
    refreshonly => true,
  }

  # Make sure the exported public key is up-to-date
  cron { 'tails-reprepro-export-key':
    user    => reprepro,
    minute  => 17,
    command => "/usr/local/bin/reprepro-export-key '${basedir}'",
    require => Reprepro::Repository['tails'],
  }

  class { 'tails::profile::reprepro::custom::notify_incoming':
    ensure           => present,
    mail_to          => $email_recipient,
    mail_from        => 'reprepro@lizard.tails.net',
    reprepro_basedir => $basedir,
  }

  # Configure the signing key for this reprepro instance
  $gpg_conf = "${basedir}/.gnupg/gpg.conf"

  file { $gpg_conf:
    ensure => file,
    owner  => reprepro,
    group  => reprepro,
    mode   => '0600',
  }

  file_line { "set default key in ${gpg_conf}":
    path  => $gpg_conf,
    line  => "default-key ${tails::profile::reprepro::params::signing_key}",
    match => '^default-key\s',
  }
}
