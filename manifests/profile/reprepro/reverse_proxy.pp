# @summary
#   Manage a nginx vhost that reverse proxies one of our APT repositories to another web server.
#
# @param public_hostname
#   the repo's public hostname
#
# @param upstream_hostname
#   where to proxy to
#
# @param upstream_port
#   which port to proxy to
#
# @param ssl
#   whether to setup a TLS protected endpoint
define tails::profile::reprepro::reverse_proxy (
  Stdlib::Fqdn     $public_hostname   = $name,
  String           $upstream_hostname = 'apt.lizard',
  Integer          $upstream_port     = 80,
  Boolean          $ssl               = true,
) {
  tails::profile::nginx::reverse_proxy { "reprepro ${public_hostname}":
    public_hostname   => $public_hostname,
    upstream_hostname => $upstream_hostname,
    upstream_port     => $upstream_port,
    ssl               => $ssl,
    # Note: we don't redirect non-ACME requests to HTTPS as this would
    # break apt-cacher-ng caching on our ISO build systems.
    ssl_redirect      => false,
  }
}
