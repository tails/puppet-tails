# @summary
#   Manage the nginx vhost for the Tails overlay APT repository
#
# @param hostname
#   the repo's public hostname
#
# @param basedir
#   reprepro's basedir
#
# @param onion_v3_hostname
#   the repo's onion address
#
class tails::profile::reprepro::custom::nginx (
  Stdlib::Fqdn $hostname,
  Stdlib::Absolutepath $basedir,
  Stdlib::Fqdn $onion_v3_hostname,
) {
  assert_private()

  include tails::profile::nginx
  ensure_packages(['libnginx-mod-http-fancyindex'])

  tails::profile::nginx::server { $hostname:
    server_name => [$hostname, $onion_v3_hostname, 'deb.tails.boum.org'],
    autoindex   => 'off',
    www_root    => $basedir,
    locations   => {
      "${hostname} root"           => {
        location    => '/',
        index_files => ['index.html'],
        www_root    => undef,
      },
      "${hostname} deny dotfiles"  => {
        index_files   => [],
        location      => '~ ^/[.].*',
        location_deny => ['all'],
        www_root      => undef,
        priority      => 501,
      },
      "${hostname} deny sensitive" => {
        index_files   => [],
        location      => '~ ^/(conf|db|incoming|logs|tails.git|tmp)/',
        location_deny => ['all'],
        www_root      => undef,
        priority      => 502,
      },
      "${hostname} dists and pool" => {
        index_files          => [],
        www_root             => undef,
        location             => '~ ^/(dists|pool)/',
        location_cfg_prepend => {
          fancyindex => 'on',
        },
        raw_append           => [
          'location ~ /(Release|InRelease|Packages|Sources)$ {',
          '    default_type text/plain;',
          '}',
        ],
        priority             => 503,
      },
    },
    require     => [
      Package['libnginx-mod-http-fancyindex'],
    ],
  }

  ensure_packages(['acl'])

  $acls = '/usr/local/etc/reprepro-custom-webdirs.acl'

  file { $acls:
    content => "group::r-X\ndefault:group::r-X\nother::r-X\ndefault:other::r-X\n",
    owner   => root,
    group   => root,
    mode    => '0600',
  }

  exec { 'set-acl-on-tails-reprepro-custom-web-dirs':
    command     => "/usr/bin/setfacl -R -M '${acls}' '${basedir}/dists' '${basedir}/pool'",
    require     => [
      File[$acls, "${basedir}/dists", "${basedir}/pool"],
      Package['acl'],
    ],
    subscribe   => File[$acls],
    refreshonly => true;
  }
}
