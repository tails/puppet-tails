# @summary
#   Setup service to watch reprepro incoming directory and send email
#   notification about newly uploaded .changes file.
#
# @param mail_from
#   whom to mail as
#
# @param mail_to
#   whom to mail to
#
# @param reprepro_basedir
#   reprepro's basedir
#
# @param ensure
#   whether these services should exist or not
#
class tails::profile::reprepro::custom::notify_incoming (
  String $mail_from,
  String $mail_to,
  Stdlib::Absolutepath $reprepro_basedir,
  Enum['present', 'absent'] $ensure = present,
) {
  assert_private()

  # Sanity check
  validate_email_address($mail_from, $mail_to)

  $reprepro_incoming_directory = "${reprepro_basedir}/incoming/"

  $service_ensure = $ensure ? {
    'absent' => 'stopped',
    default  => 'running',
  }

  $service_enable = $ensure ? {
    'absent' => false,
    default  => true,
  }

  file { '/usr/local/sbin/reprepro-notify-incoming-changes':
    ensure => $ensure,
    source => 'puppet:///modules/tails/reprepro/custom/notify-incoming/reprepro-notify-incoming-changes',
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { '/etc/default/reprepro-notify-incoming-changes':
    ensure  => $ensure,
    content => template('tails/reprepro/custom/notify-incoming/default/reprepro-notify-incoming-changes'),
    owner   => root,
    group   => root,
    mode    => '0755',
  }

  file { '/etc/systemd/system/reprepro-notify-incoming-changes.service':
    ensure  => $ensure,
    source  => 'puppet:///modules/tails/reprepro/custom/notify-incoming/systemd/reprepro-notify-incoming-changes.service',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => [
      File['/etc/default/reprepro-notify-incoming-changes'],
      File['/usr/local/sbin/reprepro-notify-incoming-changes']
    ],
  }

  service { 'reprepro-notify-incoming-changes':
    ensure    => $service_ensure,
    enable    => $service_enable,
    provider  => systemd,
    require   => File['/etc/systemd/system/reprepro-notify-incoming-changes.service'],
    subscribe => File['/etc/systemd/system/reprepro-notify-incoming-changes.service'],
  }

  exec { 'systemctl enable reprepro-notify-incoming-changes.service':
    creates => '/etc/systemd/system/multi-user.target.wants/reprepro-notify-incoming-changes.service',
  }
}
