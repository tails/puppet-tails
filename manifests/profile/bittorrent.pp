# @summary
#   profile for bittorrent server
#
class tails::profile::bittorrent () {
  package { ['transmission-cli', 'transmission-daemon']:
    ensure => present,
  }

  service { 'transmission-daemon':
    ensure  => running,
    enable  => true,
    require => Package['transmission-daemon'],
  }

  include augeas

  augeas { 'transmission settings' :
    lens    => 'Json.lns',
    incl    => '/etc/transmission-daemon/settings.json',
    changes => [
      "set '/files/etc/transmission-daemon/settings.json/dict/entry[.='rpc-bind-address']/string' '127.0.0.1'",
      "set '/files/etc/transmission-daemon/settings.json/dict/entry[.='rpc-authentication-required']/const' 'false'",
    ],
    require => Package['transmission-daemon'],
    notify  => Service['transmission-daemon'],
  }

  rbac::user { 'foundations-team-members': }

  rbac::group { 'foundations-team-members-to-debian-transmission':
    group   => 'debian-transmission',
    role    => 'foundations-team-members',
    require => Package['transmission-daemon'],
  }

  tirewall::public_service { 'tails::profile::bittorrent (TCP)':
    proto => 'tcp',
    dport => 51413,
  }

  tirewall::public_service { 'tails::profile::bittorrent (UDP)':
    proto => 'udp',
    dport => 51413,
  }
}
