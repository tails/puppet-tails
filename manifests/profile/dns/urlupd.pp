# @summary
#   An HTTP server to use with PowerDNS ifurlextup
#
# @param pool
#   The pool of IPs that should be periodically checked
#
class tails::profile::dns::urlupd (
  Array[Stdlib::IP::Address::V4] $pool,
) {
  if $::facts['os']['distro']['codename'] != 'bookworm' {
    fail("The ${name} class only supports Debian Bookworm.")
  }

  ensure_packages(['gunicorn', 'python3-flask', 'python3-apscheduler', 'python3-requests'])

  file { '/usr/local/lib/python3.11/dist-packages/urlupd.py':
    source  => 'puppet:///modules/tails/profile/dns/urlupd.py',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => [
      Package['gunicorn'],
      Package['python3-flask'],
      Package['python3-apscheduler'],
      Package['python3-requests'],
    ],
    notify  => Service['urlupd'],
  }

  systemd::unit_file { 'urlupd.service':
    enable  => true,
    # lint:ignore:strict_indent
    content => @("EOT"),
      [Unit]
      Description=HTTP server to use with PowerDNS ifurlextup
      After=network.target
      
      [Service]
      Type=notify
      DynamicUser=yes
      Environment=POOL=${pool.join(',')}
      ExecStart=/usr/bin/gunicorn urlupd:app
      Restart=on-failure
      
      [Install]
      WantedBy=multi-user.target
      | EOT
    # lint:endignore
    notify  => Service['urlupd'],
    require => File['/usr/local/lib/python3.11/dist-packages/urlupd.py'],
  }

  service { 'urlupd':
    ensure  => running,
    require => Systemd::Unit_file['urlupd.service'],
  }
}
