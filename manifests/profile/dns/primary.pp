# @summary
#   profile to manage primary DNS server
#
# @param mysql_password
#   the mysql password for pdns
#
# @param mysql_root_password
#   the mysql root password
#
# @param mysql_xerox_password
#   the mysql password for the replication user
#
# @param mysql_server_id
#   the server id
#
# @param pdns_api_key
#   the powerdns api key
#
class tails::profile::dns::primary (
  String $mysql_password,
  String $mysql_root_password,
  String $mysql_xerox_password,
  String $mysql_server_id,
  String $pdns_api_key,
) {
  include tails::profile::dns::urlupd

  class { 'powerdns':
    db_password           => $mysql_password,
    db_root_password      => $mysql_root_password,
    custom_repo           => true,
    backend_install       => false,
    backend_create_tables => false,
    require               => Class['tails::profile::dns::urlupd'],
  }

  powerdns::config { 'gmysql-dnssec': }
  powerdns::config { 'enable-lua-records': value => 'yes' }

  # don't use debian default pdns config
  file { '/etc/powerdns/pdns.d/pdns.local.gmysql.conf':
    ensure => absent,
  }

  class { 'mysql::server':
    root_password    => $mysql_root_password,
    override_options => {
      'mysqld' => {
        'bind_address'            => '0.0.0.0',
        'server_id'               => $mysql_server_id,
        'log-slave-updates'       => 'ON',
        'sync_binlog'             => 1,
        'log-bin'                 => '/var/log/mysql-bin/mysql-bin.log',
        'read_only'               => 'OFF',
        'binlog-format'           => 'ROW',
        'log-error'               => '/var/log/mysql/error.log',
        'innodb_buffer_pool_size' => '75M',
        'ssl_ca'                  => undef,
        'ssl_cert'                => undef,
        'ssl_key'                 => undef,
      },
    },
  }

  mysql_user { 'xerox@%':
    ensure        => 'present',
    password_hash => mysql_password($mysql_xerox_password),
  }

  mysql_grant { 'xerox@%/*.*':
    ensure     => 'present',
    privileges => ['REPLICATION SLAVE'],
    table      => '*.*',
    user       => 'xerox@%',
  }

  powerdns::config { 'api':
    ensure  => present,
    setting => 'api',
    value   => 'yes',
    type    => 'authoritative',
  }

  powerdns::config { 'api-key':
    ensure  => present,
    setting => 'api-key',
    value   => $pdns_api_key,
    type    => 'authoritative',
  }

  tirewall::public_service { 'tails::profile::dns::primary (TCP)':
    proto => 'tcp',
    dport => 53,
  }

  tirewall::public_service { 'tails::profile::dns::primary (UDP)':
    proto => 'udp',
    dport => 53,
  }
}
