# @summary
#   profile to manage secondary DNS servers
#
# @param mysql_password
#   the mysql password for pdns
#
# @param mysql_root_password
#   the mysql root password
#
# @param mysql_primary_server
#   the primary server to sync from
#
# @param mysql_xerox_password
#   the mysql password for the replication user
#
# @param mysql_server_id
#   the server id
#
class tails::profile::dns::secondary (
  String $mysql_password,
  String $mysql_root_password,
  String $mysql_primary_server,
  String $mysql_xerox_password,
  String $mysql_server_id,
) {
  include tails::profile::dns::urlupd

  class { 'powerdns':
    db_password           => $mysql_password,
    db_root_password      => $mysql_root_password,
    custom_repo           => true,
    backend_install       => false,
    backend_create_tables => false,
    require               => Class['tails::profile::dns::urlupd'],
  }

  powerdns::config { 'gmysql-dnssec': }
  powerdns::config { 'enable-lua-records': value => 'yes' }

  # don't use debian default pdns config
  file { '/etc/powerdns/pdns.d/pdns.local.gmysql.conf':
    ensure => absent,
  }

  class { 'mysql::server':
    root_password    => $mysql_root_password,
    override_options => {
      'mysqld' => {
        'server_id'               => $mysql_server_id,
        'read_only'               => 'ON',
        'log-error'               => '/var/log/mysql/error.log',
        'innodb_buffer_pool_size' => '75M',
        'ssl_ca'                  => undef,
        'ssl_cert'                => undef,
        'ssl_key'                 => undef,
      },
    },
  }

  exec { 'change master':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => "mysql --defaults-extra-file=/root/.my.cnf -e \"STOP SLAVE; CHANGE MASTER TO MASTER_HOST = '${mysql_primary_server}', MASTER_USER = 'xerox', MASTER_PASSWORD = '${mysql_xerox_password}', MASTER_USE_GTID = slave_pos; START SLAVE;\"", # lint:ignore:140chars
    unless  => "mysql --defaults-extra-file=/root/.my.cnf -e 'SHOW SLAVE STATUS\\G' | grep 'Slave_SQL_Running: Yes'",
  }

  tirewall::public_service { 'tails::profile::dns::secondary (TCP)':
    proto => 'tcp',
    dport => 53,
  }

  tirewall::public_service { 'tails::profile::dns::secondary (UDP)':
    proto => 'udp',
    dport => 53,
  }
}
