# @summary
#   Manage a SMTP relay used by WhisperBack to send email to Tails help desk
#
# @param secret_key
#   the onion service's secret key
#
# @param public_key
#   the onion service's public key
#
# @param onion_address
#   the onion address
#
define tails::profile::whisperback::onion (
  Sensitive[String] $secret_key,
  String $public_key,
  String $onion_address,
) {
  include tor

  tor::daemon::onion_service { $name:
    ports   => ['25'],
    v3_data => {
      hs_ed25519_secret_key => $secret_key,
      hs_ed25519_public_key => $public_key,
      hostname              => $onion_address,
    },
  }

  @@icinga2::object::service { "smtp-${onion_address}":
    check_command      => 'torsmtp',
    max_check_attempts => 12,
    check_interval     => '5m',
    retry_interval     => '5m',
    host_name          => lookup(monitoring::master::domainname),
    vars               => {
      smtp_address => $onion_address,
      tor          => true,
      smtp_timeout => 30,
    },
    import             => ['generic-service'],
    target             => '/etc/icinga2/conf.d/services.conf',
    zone               => lookup(monitoring::master::domainname),
    tag                => 'smtp',
  }
}
