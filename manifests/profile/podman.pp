# @summary
#   Manage the basic configuration of Podman in a system.
class tails::profile::podman {
  ensure_packages(['fuse-overlayfs'])

  sysctl::value { 'kernel.unprivileged_userns_clone':
    value => 1,
  }

  class { 'podman':
    manage_subuid            => true,
    # Docker module will raise an error when loading facts if the podman-docker
    # package is installed.
    podman_docker_pkg_ensure => 'absent',
    require                  => [
      Package['fuse-overlayfs'],
      Sysctl::Value['kernel.unprivileged_userns_clone'],
    ],
  }
}
