# @summary
#   Manage sudo.
#
# @example
#   include tails::profile::sudo
#
class tails::profile::sudo (
) {
  include tails::profile::sysadmins

  class { 'sudo':
    config_file_replace => false,
  }

  sudo::conf { 'mailto':
    content => "Defaults    mailto=\"${tails::profile::sysadmins::email}\"",
  }

  sudo::conf { 'group-sudo':
    content => '%sudo   ALL=(ALL:ALL) NOPASSWD : ALL',
  }
}
