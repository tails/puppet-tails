# @summary
#   manage cron.
#
# @example
#   include tails::profile::cron
#
class tails::profile::cron () {
  package { 'cron':
    ensure => present,
  } -> service { 'cron':
    ensure    => running,
    enable    => true,
    hasstatus => true,
  }
}
