# @summary
#   Manage the custom facts directory.
class tails::profile::facts_d {
  file { ['/etc/facter', '/etc/facter/facts.d']:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
