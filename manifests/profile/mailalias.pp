# @summary
#   profile to manage mail aliases
#
# @param aliases
#   array of aliases
#
class tails::profile::mailalias (
  Array $aliases,
) {
  $aliases.each | Hash $alias | {
    $recipient = $alias['recipient']
    $addresses = $alias['addresses']
    $addresses.each | String $address | {
      mailalias { $address:
        recipient => $recipient,
      }
    }
  }
}
