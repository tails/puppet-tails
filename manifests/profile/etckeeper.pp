# @summary
#   profile for etckeeper
class tails::profile::etckeeper () {
  include etckeeper

  file { '/etc/puppet/etckeeper-commit-pre':
    source => 'puppet:///modules/tails/profile/etckeeper/etckeeper-commit-pre',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
  file { '/etc/puppet/etckeeper-commit-post':
    source => 'puppet:///modules/tails/profile/etckeeper/etckeeper-commit-post',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
