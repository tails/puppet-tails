# @summary
#   Manage dropbear
#
# @example
#   include tails::profile::dropbear
#
# @param config
#   the dropbear configuration
#
class tails::profile::dropbear (
  String $config = 'DROPBEAR_OPTIONS="-p 22 -s -j -k -I 600"',
) {
  $packages = [
    'cryptsetup-bin',
    'cryptsetup-initramfs',
    'dropbear-initramfs',
  ]

  ensure_packages($packages)

  file { '/etc/dropbear/initramfs/dropbear.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => $config,
    require => Package['dropbear-initramfs'],
  }

  file { '/etc/dropbear/initramfs/authorized_keys':
    ensure  => file,
    source  => 'file:///root/.ssh/authorized_keys',
    links   => follow,
    require => Package['dropbear-initramfs'],
  }

  exec { '/usr/sbin/update-initramfs -u':
    subscribe   => [
      File['/etc/dropbear/initramfs/dropbear.conf'],
      File['/etc/dropbear/initramfs/authorized_keys'],
    ],
    refreshonly => true,
  }
}
