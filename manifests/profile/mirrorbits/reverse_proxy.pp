# @summary
#   profile to manage an nginx vhost that reverse proxies to a Mirrorbits instance.
#
# @param public_hostname
#   the public hostname
#
# @param upstream_hostname
#   where to reverse proxy to
#
# @param upstream_port
#   which port to reverse proxy to
#
class tails::profile::mirrorbits::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'mirrors.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'rsync.lizard',
  Stdlib::Port $upstream_port     = 8080,
) {
  tails::profile::nginx::server { $public_hostname:
    ssl       => true,
    locations => {
      "${public_hostname} root"                 => {
        location            => '/',
        location_cfg_append => { 'return' => "301 https://${public_hostname}/tails/", },
        index_files         => [],
        ssl_only            => true,
      },
      "${public_hostname} reverse_proxy"        => {
        location       => '/tails/',
        rewrite_rules  => ['/tails/(.*)$ /$1 break'],
        proxy          => "http://${upstream_hostname}:${upstream_port}",
        proxy_redirect => "http://${upstream_hostname}/ https://${public_hostname}/",
        ssl_only       => true,
      },
      "${public_hostname} reverse_proxy static" => {
        location       => '~* \.(?:ico|css|js|gif|jpe?g|png)$',
        rewrite_rules  => ['/tails/(.*)$ /$1 break'],
        proxy          => "http://${upstream_hostname}:${upstream_port}",
        proxy_redirect => "http://${upstream_hostname}/ https://${public_hostname}/",
        ssl_only       => true,
        expires        => '7d',
        add_header     => { 'Cache-Control' => 'public' },
      },
    },
  }
}
