# @summary
#   Manage what's needed to serve an instance of the Tails website
#   This needs to be refactored into a separate module/profile
#
# @param ensure
#   NB: "ensure => absent" cleans up only partially.
#
# @param public_hostname
#   the site's hostname
#
# @param letsencrypt
#   whether to use letsencrypt
#
# @param ssl
#   whether to use ssl
#
# @param ssl_cert
#   custom path to ssl certificate (or false)
#
# @param ssl_key
#   custom path to ssl key (or false)
#
# @param ssl_redirect
#   whether to redirect http to https
#
# @param web_dir
#   the webroot
#
# @param stats
#   whether to set up a cronjob to mail site statistics
#
# @param manage_log_dir
#   whether to manage the log_dir here
#
# @param log_dir
#   the directory where logs are written to
#
# @param canonical
#   whether to set the canonical link header
#
define tails::profile::website::webserver::instance (
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Fqdn $public_hostname     = $name,
  Boolean $ssl                      = true,
  Boolean $ssl_redirect             = true,
  Boolean $letsencrypt              = true,
  Variant[String, Boolean, Array[String]] $ssl_cert = false,
  Variant[String, Boolean, Array[String]] $ssl_key  = false,
  Stdlib::Absolutepath $web_dir     = "/srv/${public_hostname}/html",
  Boolean $stats                    = true,
  Boolean $manage_log_dir           = true,
  Stdlib::Absolutepath $log_dir     = "/var/log/nginx/${public_hostname}",
  Optional[Stdlib::Fqdn] $canonical = undef,
) {
  ### Resources

  include tails::profile::website::webserver::common
  include tails::profile::nginx

  ensure_packages(['libnginx-mod-http-fancyindex'])

  $sts_header = $ssl ? {
    true    => { 'Strict-Transport-Security' => { 'max-age=31536000; includeSubDomains; preload' => 'always' } },
    default => {},
  }

  $link_header = $canonical ? {
    undef    => {},
    default => { 'Link' => '<https://tails.net$request_uri>; rel="canonical"' },
  }

  $default_headers = {
    'X-Frame-Options'        => 'sameorigin',
    'X-XSS-Protection'       => '1; mode=block',
    'X-Content-Type-Options' => 'nosniff',
  } + $link_header

  $location_headers = $default_headers + $sts_header

  tails::profile::nginx::server { $name:
    access_log         => "${log_dir}/access.log",
    error_log          => 'absent',
    autoindex          => 'off',
    ssl_redirect       => $ssl_redirect,
    www_root           => $web_dir,
    ssl                => $ssl,
    letsencrypt        => $letsencrypt,
    ssl_cert           => $ssl_cert,
    ssl_key            => $ssl_key,
    add_header         => $default_headers,
    server_cfg_prepend => {
      fancyindex      => 'on',
      gzip            => 'on',
      gzip_comp_level => '5',
      gzip_min_length => '256',
      gzip_vary       => 'on',
      include         => '/etc/nginx/include.d/tails_website_rewrite_rules.conf',
    },
    raw_prepend        => [
      'if ($arg_back) {',
      '  rewrite ^ "$uri#back?" redirect;',
      '}',
    ],
    gzip_types         => 'application/atom+xml
      application/javascript
      application/json
      application/ld+json
      application/manifest+json
      application/rss+xml
      application/vnd.geo+json
      application/vnd.ms-fontobject
      application/x-font-ttf
      application/x-web-app-manifest+json
      application/xhtml+xml
      application/xml
      font/opentype
      image/bmp
      image/svg+xml
      image/x-icon
      text/cache-manifest
      text/css
      text/plain
      text/vcard
      text/vnd.rim.location.xloc
      text/vtt
      text/x-component
      text/x-cross-domain-policy',
    locations          => {
      "${name} addslash"              => {
        index_files         => [],
        www_root            => undef,
        location            => '@addslash',
        location_cfg_append => { return => '301 $uri/$is_args$args' },
        ssl_only            => $ssl,
        add_header          => $location_headers,
      },
      "${name} static"                => {
        index_files => [],
        www_root    => undef,
        location    => '~* \.(?:css|gif|ico|jpe?g|js|png|svg|ttf)$',
        ssl_only    => $ssl,
        add_header  => $location_headers,
        expires     => '1d',
        priority    => 501,
      },
      "${name} latest.json"           => {
        index_files => [],
        www_root    => undef,
        location    => '= /install/v2/Tails/amd64/stable/latest.json',
        ssl_only    => $ssl,
        add_header  => {
          'Cache-Control'               => 'max-age=7200, must-revalidate',
          'Access-Control-Allow-Origin' => '*', # See: https://gitlab.tails.boum.org/tails/sysadmin/-/issues/17771
        } + $location_headers,
        priority    => 502,
      },
      "${name} mirrors.json"          => {
        index_files => [],
        www_root    => '/srv/mirrors_json/',
        location    => '= /mirrors.json',
        try_files   => ['$uri', '=404'],
        ssl_only    => $ssl,
        add_header  => {
          'Cache-Control' => 'max-age=7200, must-revalidate',
        } + $location_headers,
        priority    => 503,
      },
      "${name} funding-manifest-urls" => {
        index_files => [],
        location    => '= /.well-known/funding-manifest-urls',
        try_files   => ['/funding-manifest-urls', '=404'],
        ssl_only    => $ssl,
        add_header  => {
          'Cache-Control' => 'max-age=7200, must-revalidate',
        } + $location_headers,
        priority    => 503,
      },
      "${name} torrent files"         => {
        index_files         => [],
        www_root            => undef,
        location            => '= /torrents/files/',
        ssl_only            => $ssl,
        add_header          => $location_headers,
        location_cfg_append => { fancyindex => 'on' },
        priority            => 504,
      },
      # Matches all requests that don't end with "/",
      # except those caught by a location with the "^~" modifier
      # or by an exact match ("=" modifier).
      "${name} no-end-slash catchall" => {
        index_files => [],
        www_root    => undef,
        location    => '~ [^/]$',
        try_files   => ['$uri', '@addslash'],
        ssl_only    => $ssl,
        add_header  => $location_headers,
        priority    => 506,
      },
      # Matches all requests that end with "/",
      # except those caught by a location with the "^~" modifier
      # or by an exact match ("=" modifier).
      "${name} end-slash catchall"    => {
        index_files => [],
        www_root    => undef,
        location    => '~ /$',
        try_files   => ['$uri/index.$lang.html', '$uri/index.en.html', '$uri/index.html =404'],
        ssl_only    => $ssl,
        add_header  => $location_headers,
        priority    => 507,
      },
    },
    require            => [
      Package['libnginx-mod-http-fancyindex'],
      File['/etc/nginx/include.d/tails_website_rewrite_rules.conf'],
    ],
  }

  if $manage_log_dir {
    file { $log_dir:
      ensure => directory,
      owner  => 'www-data',
      group  => 'adm',
      mode   => '0750',
    }

    file { "/etc/logrotate.d/nginx-${public_hostname}":
      ensure  => $ensure,
      content => template('tails/website/nginx/logrotate.conf.erb'),
      owner   => root,
      group   => root,
      mode    => '0644',
      require => File[$log_dir],
    }
  }

  $cron_ensure = $stats ? { true => 'present', default => 'absent' }
  $require_log_dir = $manage_log_dir ? { true => [File[$log_dir]], default => [] } #lint:ignore:unquoted_string_in_selector

  cron { "tails-website-email-last-month-stats ${public_hostname}":
    ensure      => $cron_ensure,
    user        => 'www-data',
    monthday    => '2',
    hour        => '17',
    minute      => '57',
    command     => "/usr/local/bin/tails-website-last-month-stats '${log_dir}'",
    environment => ['MAILTO=tails-dev@boum.org'],
    require     => [File['/usr/local/bin/tails-website-last-month-stats']] + $require_log_dir,
  }
}
