# @summary
#   Manage rewrite rules shared by all Tails website instances
#   This should be refactored into a separate profile/module.
#
# @param ensure
#   ensure whether rules are there or not
#
# @param po_slave_languages
#   this could really do with some better naming
#
class tails::profile::website::webserver::rewrite_rules (
  Enum['present', 'absent'] $ensure = 'present',
  Hash $po_slave_languages          = $tails::profile::website::webserver::params::production_slave_languages,
) inherits tails::profile::website::webserver::params {
  file { '/etc/nginx/include.d/tails_website_rewrite_rules.conf':
    ensure  => $ensure,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0644',
    content => epp('tails/website/nginx/rewrite_rules.conf.epp', {
        po_slave_languages => $po_slave_languages,
    }),
  }
}
