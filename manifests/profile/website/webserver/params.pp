# @summary
#   Default parameters shared by all Tails website builders and webservers
#   This should be refactored to a separate module/profile
#
# @param production_slave_languages
#   this really needs a better name
#
class tails::profile::website::webserver::params (
  Hash $production_slave_languages = {},
) {
  $weblate_additional_languages = lookup('weblate::params::weblate_additional_languages')
  $weblate_slave_languages = deep_merge($production_slave_languages, $weblate_additional_languages)
}
