# @summary
#   Manage resources shared by all Tails website instances
#   This should be refactored into a separate module/profile
#
# @param ensure
#   whether or not all these resources should be there
#
# @param po_slave_languages
#   this really needs a better name
#
class tails::profile::website::webserver::common (
  Enum['present', 'absent'] $ensure = 'present',
  Hash $po_slave_languages          = $tails::profile::website::webserver::params::production_slave_languages,
) inherits tails::profile::website::webserver::params {
  assert_private()

  $mappings = $po_slave_languages.reduce({}) | $memo, $value | {
    $memo + { "~*^${value[0]}" => $value[0] }
  }

  nginx::resource::map { 'lang':
    default  => 'en',
    string   => '$http_accept_language',
    mappings => $mappings,
  }

  include tails::profile::website::webserver::rewrite_rules

  $packages = [
    'libnginx-mod-http-fancyindex',
  ]

  ensure_packages($packages)

  file { '/usr/local/bin/tails-website-last-month-stats':
    ensure => $ensure,
    source => 'puppet:///modules/tails/website/tails-website-last-month-stats',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
