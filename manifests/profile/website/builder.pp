# @summary
#   Manage what's needed to build the Tails website
#   This should be refactored to a separate module/profile
#
class tails::profile::website::builder (
) {
  ### Sanity checks

  if $facts['os']['name'] != 'Debian' or versioncmp($facts['os']['distro']['release']['major'], '11') < 0 {
    fail('This class only supports Debian Bullseye and higher.')
  }

  ### Resources

  file { '/usr/local/lib/site_perl':
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/website/ikiwiki-libdir',
    recurse => true,
    purge   => true,
  }

  package { 'ikiwiki':
    ensure  => present,
    require => Apt::Pin['ikiwiki'],
  }

  # Fix tails#18992
  tails::profile::apt::signed_source { 'ikiwiki':
    key_fp     => 'D68F87149EBA77541573C1C12453AA9CE4123A9A',
    key_source => 'puppet:///modules/tails/D68F87149EBA77541573C1C12453AA9CE4123A9A.asc',
    location   => 'http://deb.tails.boum.org/',
    release    => 'ikiwiki',
    repos      => 'main',
  }
  apt::pin { 'ikiwiki':
    ensure   => present,
    packages => 'ikiwiki',
    origin   => 'deb.tails.boum.org',
    priority => 991,
    require  => Tails::Profile::Apt::Signed_source['ikiwiki'],
  }

  #17005
  $require_po4a = $facts['os']['distro']['codename'] ? { 'bookworm' => Apt::Pin['po4a'], default => undef }

  package { 'po4a':
    ensure  => present,
    require => $require_po4a,
  }
  if $facts['os']['distro']['codename'] == 'bookworm' {
    include tails::profile::apt

    apt::source { 'bullseye':
      ensure   => present,
      location => $tails::profile::apt::debian_url,
      release  => 'bullseye',
      repos    => $tails::profile::apt::repos,
      pin      => {
        originator => 'Debian',
        codename   => 'bullseye',
        priority   => 1,
      },
    }
    apt::pin { 'po4a':
      ensure     => present,
      packages   => 'po4a',
      originator => 'Debian',
      codename   => 'bullseye',
      priority   => 1000,
    }
  }

  $packages = [
    'libyaml-perl',
    'libyaml-libyaml-perl',
    'libyaml-syck-perl',
    'perlmagick',
  ]

  ensure_packages($packages)
}
