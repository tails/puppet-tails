# @summary
#   Setup a mirror of the Tails website that is able to receive data via Rsync
#
# @param domain
#   The domain name where the mirror is served
#
# @param alt_domain
#   An alternative domain so we can easily visit the mirror
#
# @param source
#   The IP address of the host that pushes the website via Rsync
#
# @param public_ip
#   The public ip address
#
# @param www_root
#   The website path in the filesystem
#
# @param stats
#   Whether this mirror should calculate and report stats
#
class tails::profile::website::mirror (
  String                  $domain,
  String                  $alt_domain,
  Stdlib::IP::Address::V4 $source,
  Stdlib::IP::Address::V4 $public_ip,
  Stdlib::Absolutepath    $www_root = "/srv/${domain}",
  Boolean                 $stats    = false,
) {
  file { '/var/www':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  user { 'www-data':
    home           => '/var/www',
    shell          => '/bin/sh',
    system         => true,
    purge_ssh_keys => true,
    require        => File['/var/www'],
  }

  file { $www_root:
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0755',
  }

  tails::profile::website::webserver::instance { $alt_domain:
    web_dir        => $www_root,
    stats          => false,
    manage_log_dir => true,
    canonical      => $domain,
    require        => File[$www_root],
  }

  sshkeys::set_authorized_keys { 'tails::profile::website':
    user    => 'www-data',
    home    => '/var/www',
    # restrict the key usage to remote rsync from a specific source only
    options => [
      'restrict',
      "command=\"/usr/bin/rrsync -wo ${www_root}\"",
      "from=\"${source}\"",
    ],
    require => [
      User['www-data'],
      File[$www_root],
    ],
  }

  # Main website instance, with collected TLS certificate

  File <<| tag == "tails_www_tls_${domain}" |>> {
    notify => Service['nginx']
  }

  tails::profile::website::webserver::instance { $domain:
    web_dir        => $www_root,
    letsencrypt    => false,
    ssl_cert       => "/etc/ssl/${domain}/fullchain.pem",
    ssl_key        => "/etc/ssl/${domain}/privkey.pem",
    stats          => $stats,
    manage_log_dir => true,
    canonical      => $domain,
    require        => [
      File[$www_root],
      File["/etc/ssl/${domain}/fullchain.pem"],
      File["/etc/ssl/${domain}/privkey.pem"],
    ],
  }

  # Either handle the Let's Encrypt TLS certificate request and export or
  # add a redirect to the primary mirror.

  $redirect_domains = {
    'tails.boum.org' => 301,
    'www.tails.net' => 302,
  }

  $all_domains = ($redirect_domains.keys() + [$domain])
  $all_domains.each | String $d | {
    if $alt_domain == "www1.${domain}" {
      tails::profile::nginx::letsencrypt { $d:
        monitoring_ensure => 'absent',
      }
      tails::profile::letsencrypt::export { $d:
        tag => "tails_www_tls_${d}",
      }
    } else {
      nginx::resource::location { "${d} ACME challenge redirect":
        server              => $d,
        location            => '^~ /.well-known/acme-challenge',
        index_files         => [],
        ssl                 => false,
        location_cfg_append => { return => "302 http://www1.${domain}\$request_uri", },
      }
    }
    @@monitoring::services::tls { "${d}-${public_ip}":
      address => $public_ip,
      cn      => [$d],
    }
  }

  # Redirectors

  $redirect_domains.each | String $source, Integer $code | {
    tails::profile::website::redirector { $source:
      access_log       => "/var/log/nginx/${domain}/access.log",
      target_domain    => $domain,
      www_root         => "/srv/${domain}",
      http_status_code => $code,
    }
    $http_expect = {
      301 => 'HTTP/1.1 301 Moved Permanently',
      302 => 'HTTP/1.1 302 Moved Temporarily',
    }
    @@monitoring::services::http { "mirror-${source}-${domain}-${public_ip}":
      vhost   => $source,
      address => $public_ip,
      vars    => {
        'http_expect'       => $http_expect[$code],
        'http_headerstring' => "Location: https://${domain}/",
      },
    }
  }

  # Serve /mirrors.json
  file { '/srv/mirrors_json':
    owner => www-data,
    group => www-data,
    mode  => '0755',
  }

  tails::profile::mirrors_json { '/srv/mirrors_json/mirrors.json':
    require => File['/srv/mirrors_json'],
  }
}
