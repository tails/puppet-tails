# @summary
#   Redirect from one domain to another
#   This should be refactored into a separate module/profile
#
# @param target_domain
#   where we'll redirect to
#
# @param www_root
#   the webroot
#
# @param ensure
#   whether the redirect should be there or not
#
# @param access_log
#   path to the access log
#
# @param source_domain
#   where we'll redirect from
#
# @param http_status_code
#   whether to redirect with 301 or 302
#
define tails::profile::website::redirector (
  String $target_domain,
  Stdlib::Absolutepath $www_root,
  Enum['absent', 'present'] $ensure = 'present',
  Optional[Stdlib::Absolutepath] $access_log = undef,
  String $source_domain = $name,
  Integer[301, 302] $http_status_code = 302,
) {
  $headers = {
    'X-Frame-Options'           => 'sameorigin',
    'X-XSS-Protection'          => '1; mode=block',
    'X-Content-Type-Options'    => 'nosniff',
    'Strict-Transport-Security' => { 'max-age=31536000; includeSubDomains; preload' => 'always' },
    'Link'                      => '<https://tails.net$request_uri>; rel="canonical"',
  }

  File <<| tag == "tails_www_tls_${source_domain}" |>>

  tails::profile::nginx::server { $source_domain:
    access_log         => $access_log,
    autoindex          => 'off',
    gzip_types         => 'application/atom+xml',
    locations          => {
      "${source_domain} catchall redirect"       => {
        location            => '/',
        index_files         => [],
        www_root            => undef,
        location_cfg_append => { return => "${http_status_code} https://${target_domain}\$request_uri" },
      },
      # The Tails Upgrader included in Tails 5.13 and older versions is based
      # on code that doesn't support redirects, so upgrades for all older
      # versions of Tails will only continue to work if we continue to serve
      # UDFs and OpenPGP keys directly instead of redirecting to the target
      # domain.
      "${source_domain} serve UDFs"              => {
        add_header  => $headers,
        index_files => [],
        ssl_only    => true,
        www_root    => $www_root,
        location    => '~ ^/upgrade/.*/upgrades\.yml(|\.pgp)$',
      },
      "${source_domain} serve OpenPGP keys"      => {
        add_header  => $headers,
        index_files => [],
        ssl_only    => true,
        www_root    => $www_root,
        location    => '~ ^/tails-[a-z-]+\.key$',
      },
      # The Tails Upgrader included in Tails 5.8 and older versions is based on
      # code that doesn't support redirects and tries to download mirrors.json
      # from the tails.boum.org domain, so upgrades for these versions will
      # only continue to work if we continue to serve mirrors.json directly
      # instead of redirecting to the target domain.
      "${source_domain} serve mirrors.json"      => {
        add_header  => $headers,
        index_files => [],
        ssl_only    => true,
        www_root    => '/srv/mirrors_json',
        location    => '/mirrors.json',
      },
      # The `tails-security-check` script included in Tails 5.13 and older
      # versions fetches a security feed on every session using code that
      # doesn't support redirects, so notifying the user about security issues
      # that are not fixed yet will only continue to work if we continue
      # to serve the atom feed directly instead of redirecting to the target
      # domain.
      "${source_domain} serve the Security page" => {
        add_header  => $headers,
        index_files => [],
        ssl_only    => true,
        www_root    => $www_root,
        location    => '~ ^/security/index\.[^\.]+\.atom$',
      },
    },
    server_cfg_prepend => {
      gzip            => 'on',
      gzip_comp_level => '5',
      gzip_min_length => '256',
      gzip_vary       => 'on',
    },
    letsencrypt        => false,
    ssl                => true,
    ssl_redirect       => false,
    ssl_cert           => "/etc/ssl/${source_domain}/fullchain.pem",
    ssl_key            => "/etc/ssl/${source_domain}/privkey.pem",
  }
}
