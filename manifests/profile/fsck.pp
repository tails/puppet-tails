# @summary
#   Ensure we don't hang on fsck during boot.
#
# @example
#   include tails::profile::fsck
#
class tails::profile::fsck (
) {
# TODO: does this even still work with systemd?
  augeas { 'rcS-FSCKFIX':
    context => '/files/etc/default/rcS', changes => 'set FSCKFIX yes';
  }
}
