# @summary
#   Manage Rspamd
#
# @param dkim_signing
#   whether outgoing mails need to be dkim signed
#
# @param domains
#   array of maildomains who need dkim signing
#
class tails::profile::rspamd (
  Boolean       $dkim_signing = false,
  Array[String] $domains      = [],
) {
  include redis
  class { 'rspamd':
    manage_package_repo => false,
  }

  rspamd::config {
    'redis:servers': value => '127.0.0.1';
    'milter_headers:use': value => ['authentication-results','x-spam-status'];
    'milter_headers:extended_spam_headers': value => true;
    'mx_check:enabled': value => true;
    'actions:reject': value => '8';
    'actions:add_header': value => '3';
    'actions:greylist': value => '2';
    'actions:rewrite_subject': value => '4';
    'actions:subject': value => '***SPAM*** %s';
    'rbl:rbls.nixspam.enabled': value => 'false';
  }

  if $dkim_signing {
    rspamd::config {
      'dkim_signing:sign_local': value => true;
    }
    rspamd::config {
      'dkim_signing:sign_headers': value => '(o)from:(x)sender:(o)reply-to:(x)date:(x)message-id:(o)to:(o)cc:(x)content-type:(x)content-transfer-encoding:resent-to:resent-cc:resent-from:resent-sender:resent-message-id:(x)in-reply-to:(x)references:list-id:list-help:list-owner:list-unsubscribe:list-unsubscribe-post:list-subscribe:list-post:(x)openpgp:(x)autocrypt'; # lint:ignore:140chars
    }
    rspamd::config {
      'dkim_signing:use_esld': value => false;
    }

    file { '/var/lib/rspamd/dkim':
      ensure  => directory,
      owner   => '_rspamd',
      group   => '_rspamd',
      mode    => '0700',
      require => Class['rspamd'],
    }

    $domains.each | String $signdomain | {
      exec { "/usr/bin/rspamadm dkim_keygen -s 'dkim' -d ${signdomain} -k /var/lib/rspamd/dkim/${signdomain}.dkim.key":
        user    => '_rspamd',
        group   => '_rspamd',
        require => File['/var/lib/rspamd/dkim'],
        creates => "/var/lib/rspamd/dkim/${signdomain}.dkim.key",
      }
    }
  }
}
