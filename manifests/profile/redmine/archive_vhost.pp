# @summary
#   Manage nginx sites for Tails' archived Redmine
#
# @param public
#   whether the archive is public
#
# @param ensure
#   whether the site should exist
#
# @param web_hostname
#   the archive's public hostname
#
# @param http_user
#   in case of a private archive, the username
#
# @param http_password_hash
#   in case of a private archive, the password hash
#
# @param web_dir
#   the webroot
#
define tails::profile::redmine::archive_vhost (
  Boolean $public,
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Fqdn $web_hostname           = "${name}.tails.net",
  Optional[String] $http_user          = undef,
  Optional[String] $http_password_hash = undef,
  Stdlib::Absolutepath $web_dir        = "/srv/${web_hostname}",
) {
  ### Sanity checks

  if ! $public and $http_password_hash == undef {
    fail("\$http_password_hash must be set for private archives")
  }

  if ! $public and $http_user == undef {
    fail("\$http_user must be set for private archives")
  }

  ### Resources

  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }
  file { [$web_dir, "${web_dir}/code"]:
    ensure => $directory_ensure,
    owner  => root,
    group  => 'www-data',
    mode   => '0750',
  }

  file { "${web_dir}/index.html":
    ensure  => $ensure,
    content => epp('tails/profile/redmine/archive/index.html.epp', { public => $public }),
    owner   => root,
    group   => 'www-data',
    mode    => '0640',
  }

  if $ensure == 'present' and ! $public {
    $auth_file_ensure = 'file'
    $auth_basic = 'Tails :: private Redmine archive'
    $auth_basic_user_file = "/etc/nginx/auth.d/htpasswd.${web_hostname}"
    $auth_require = [File["/etc/nginx/auth.d/htpasswd.${web_hostname}"]]
    $index_files = []
    $autoindex = 'on'
    $location_cfg_append = { 'fancyindex' => 'on' }
  } else {
    $auth_file_ensure = 'absent'
    $auth_basic = undef
    $auth_basic_user_file = undef
    $auth_require = undef
    $index_files = ['index.html']
    $autoindex = 'off'
    $location_cfg_append = {}
  }

  file { "/etc/nginx/auth.d/htpasswd.${web_hostname}":
    ensure  => $auth_file_ensure,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0640',
    content => "${http_user}:${http_password_hash}",
    require => File['/etc/nginx/auth.d'],
  } ~> Service['nginx']

  ['tails.net', 'tails.boum.org'].each | $domain | {
    tails::profile::nginx::server { "${name}.${domain}":
      index_files => $index_files,
      www_root    => '/var/www/html',
      ssl         => true,
      autoindex   => $autoindex,
      locations   => {
        "${name}.${domain} root" => {
          location             => '/',
          www_root             => $web_dir,
          ssl_only             => true,
          index_files          => $index_files,
          auth_basic           => $auth_basic,
          auth_basic_user_file => $auth_basic_user_file,
          location_cfg_append  => $location_cfg_append,
        },
      },
      require     => [
        File[$web_dir],
      ] + $auth_require,
    }
  }
}
