# @summary
#   Manage redirection from https://redmine.tails.boum.org/ to GitLab
#
# @param ensure
#   whether the redirections should be in place
#
# @param redmine_hostname
#   the hostname of the old redmine instance
#
# @param gitlab_url
#   the url of the new gitlab instance
#
# @param ssl
#   whether to use ssl
#
# @param web_dir
#   the webroot
#
# @param attachments_source
#   rewrite rules for attachments
#
# @param private_attachments
#   rewrite rules for private attachments
#
class tails::profile::redmine::redirector (
  Enum['present', 'absent'] $ensure     = 'present',
  Stdlib::Fqdn $redmine_hostname        = 'redmine.tails.boum.org',
  String $gitlab_url                    = 'https://gitlab.tails.boum.org',
  Boolean $ssl                          = true,
  Stdlib::Absolutepath $web_dir         = "/srv/${redmine_hostname}",
  String $attachments_source            = 'puppet:///modules/tails/profile/redmine/attachments_rewrite_rules.conf',
  Optional[String] $private_attachments = undef,
) {
  tails::profile::nginx::server { $redmine_hostname:
    www_root  => $web_dir,
    ssl       => true,
    locations => {
      "${redmine_hostname} attachments" => {
        location    => '/code/attachments/',
        ssl_only    => true,
        index_files => [],
        www_root    => undef,
        include     => [
          '/etc/nginx/include.d/tails_redmine_redirector_attachments.conf',
          '/etc/nginx/include.d/tails_redmine_redirector_private_attachments.conf',
        ],
      },
      "${redmine_hostname} root"        => {
        location            => '/',
        ssl_only            => true,
        rewrite_rules       => ["^/code/issues/(.*) ${gitlab_url}/tails/tails/-/issues/\$1 permanent"],
        index_files         => [],
        www_root            => undef,
        location_cfg_append => {
          'return' => '302 https://gitlab.tails.boum.org/',
        },
      },
    },
    require   => [
      File[$web_dir],
    ],
  }

  file { '/etc/nginx/include.d/tails_redmine_redirector_attachments.conf':
    ensure => $ensure,
    source => $attachments_source,
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  if $private_attachments {
    file { '/etc/nginx/include.d/tails_redmine_redirector_private_attachments.conf':
      ensure  => $ensure,
      content => $private_attachments,
      owner   => root,
      group   => root,
      mode    => '0644',
    }
  }

  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }

  file { $web_dir:
    ensure => $directory_ensure,
    owner  => root,
    group  => 'www-data',
    mode   => '0750',
  }
}
