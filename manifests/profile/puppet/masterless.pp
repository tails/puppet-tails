# masterless puppet run
#
# assumes this repo is already cloned in /etc/puppet/code
#
class tails::profile::puppet::masterless {
  ensure_packages(['puppet'])

  include tails::profile::puppet::eyaml

  file { '/etc/puppet/hiera.yaml':
    ensure => symlink,
    target => '/etc/puppet/code/hiera.yaml',
  }

  # lint:ignore:strict_indent
  $_service = @(EOT)
  [Unit]
  Description=Timer for masterless puppet runs

  [Service]
  Type=oneshot
  User=root
  Group=root
  ExecStartPre=-find /var/cache/puppet/reports -type f -ctime +1 -exec rm -f {} \;
  ExecStartPre=-/usr/bin/git -C /etc/puppet/code pull --ff-only --verify-signatures
  ExecStartPre=-/usr/bin/git -C /etc/puppet/code submodule sync
  ExecStartPre=-/usr/bin/git -C /etc/puppet/code submodule update --init --recursive
  ExecStart=/usr/bin/puppet apply /etc/puppet/code/manifests/%H.pp
  | EOT

  $_timer = @(EOT)
  [Unit]
  Description=Timer for masterless puppet runs

  [Timer]
  OnCalendar=*-*-* *:05,35:00
  Persistent=true
  RandomizedDelaySec=1500

  [Install]
  WantedBy=timers.target
  | EOT
  # lint:endignore

  systemd::timer { 'run-puppet.timer':
    timer_content   => $_timer,
    service_content => $_service,
    active          => true,
    enable          => true,
  }
}
