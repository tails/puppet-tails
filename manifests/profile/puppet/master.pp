# @summary
#   profile for puppetmaster
#
# @param external_nodes
#   external nodes classifier executable
#
# @param public_mirror
#   where to sync the code repository to
#
class tails::profile::puppet::master (
  Variant[String[0], Stdlib::Absolutepath] $external_nodes = undef,
  String                                   $public_mirror  = 'git@gitlab-ssh.tails.boum.org:tails/puppet-code',
) {
  include rbac

  file { [
      '/opt',
      '/opt/puppetlabs',
      '/opt/puppetlabs/server',
      '/opt/puppetlabs/server/apps',
      '/opt/puppetlabs/server/apps/puppetserver',
      '/etc/puppet/conf.d',
    ]:
      ensure => directory,
  }

# dirty hack to prevent the puppetmodule from resetting vardir ownership
# and restarting puppet master all the time
  File <| title == '/var/lib/puppet' |> {
    owner => undef,
    group => undef,
  }
  File <| title == '/var/cache/puppet' |> {
    owner => undef,
    group => undef,
  }

  class { 'puppet':
    manage_packages       => true,
    agent                 => true,
    runmode               => 'systemd.timer',
    server                => true,
    server_reports        => 'store,puppetdb',
    server_foreman        => false,
    server_external_nodes => $external_nodes,
    server_git_repo       => true,
    server_git_repo_path  => '/var/lib/puppet/puppet-code.git',
    server_storeconfigs   => true,
    additional_settings   => { fact_value_length_soft_limit => 65535 },
  }

  class { 'puppet::server::puppetdb':
    server => $facts['networking']['fqdn'],
  }

# ensure all commits to our puppet repo are signed

  file { '/var/lib/puppet/puppet-code.git/hooks/update':
    source  => 'puppet:///modules/tails/profile/puppet/master/check-commit-signature',
    owner   => 'puppet',
    group   => 'puppet',
    mode    => '0755',
    require => Class['puppet'],
  }

# ensure we have a remote for our public mirror

  exec { "/usr/bin/git -C /var/lib/puppet/puppet-code.git remote add mirror ${public_mirror}":
    user   => puppet,
    group  => puppet,
    unless => '/usr/bin/git -C /var/lib/puppet/puppet-code.git remote |grep -qs -x mirror';
  }

# and set up a hook to sync to the public mirror

  file { '/var/lib/puppet/puppet-code.git/hooks/post-update':
    source  => 'puppet:///modules/tails/profile/puppet/master/gitlab-mirror',
    owner   => 'puppet',
    group   => 'puppet',
    mode    => '0755',
    require => Class['puppet'],
  }
# ensure we have backups for the puppetdb

  include backupninja
  backupninja::pgsql { 'backup-puppetdb':
    ensure => present,
  }

# make sure we can encrypt sensitive data in hiera

  include tails::profile::puppet::eyaml

# ensure sysadmins can access the git repo

  rbac::ssh { 'sysadmins-to-puppet':
    user => 'puppet',
    role => 'sysadmins',
  }

# ensure puppet knows the sysadmin PGP keys

  include yapgp
  $rbac::roles['sysadmins'].each | String $username | {
    $fp = $rbac::users[$username]['pgpkey']

    pgp_key { "puppet-${fp}":
      ensure => present,
      fp     => $fp,
      user   => puppet,
      trust  => 5,
    }
  }

# Clean up reports

  exec { '/usr/bin/find /var/lib/puppetserver/reports -type f -ctime +2 -exec /bin/rm {} \;': }

# Firewall

  tirewall::accept_trusted_subnets { 'Puppet Master':
    dport => 8140,
  }

# Log rotation

  ensure_packages(['logrotate'])

  file { '/etc/logrotate.d/puppet':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    source  => 'puppet:///modules/tails/profile/puppet/master/logrotate.conf',
    require => Package['logrotate'],
  }
}
