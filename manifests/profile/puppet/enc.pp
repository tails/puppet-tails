# @summary
#   The following Puppet ENC was added to help with more complex deployments,
#   but is (currently) not really needed/used during normal operation. If you
#   want to remove it, please make sure to check the contents of the script in
#   case its use has changed and this comment has not been updated.
#
class tails::profile::puppet::enc () {
  # lint:ignore:strict_indent
  $_script = @(EOT)
  #!/usr/bin/python3
  #
  # This is a simple Puppet ENC aimed to help with complex deployments. It allows
  # nodes to be mapped to different Puppet environments which, in our case, map
  # to different Git branches of our Puppet code repository.
  #
  # The script takes a node name as its only argument and prints a YAML document
  # containing an `environment` key whose content depends on the node.
  #
  # By default, nodes not explicitly listed in the ENVIRONMENTS dict will be
  # mapped to the `production` environment.
  #
  # To assign a node to a different environment, add it to the ENVIRONMENTS dict,
  # as such:
  #
  #     ENVIRONMENTS = defaultdict(lambda: 'production', {
  #         'dev-node.example.com': 'development',
  #         'special-node.example.com': 'special-branch',
  #     }
  #
  # No classes or parameters are currently set by this script, even though their
  # corresponding keys are included in the output YAML file, as containing at
  # least one of them is a requirement according to the ENC specs.
  #
  # Make sure to push the corresponding branch(es) to the Puppet code repository
  # before pushing new node-to-env mappings in this script, otherwise Puppet
  # apply will fail in those nodes as the Puppet Server won't be able to find the
  # corresponding branch(es) in the repository.
  #
  # References:
  #
  #    https://www.puppet.com/docs/puppet/latest/nodes_external.html
  #    https://github.com/theforeman/puppet-puppet#git-repo-support
  
  from argparse import ArgumentParser
  from collections import defaultdict
  
  
  ENVIRONMENTS = defaultdict(lambda: 'production', {})
  
  
  def parse_args():
      parser = ArgumentParser()
      parser.add_argument('node', help='The node to be classified.')
      return parser.parse_args()
  
  
  if __name__ == '__main__':
      args = parse_args()
      env = ENVIRONMENTS[args.node]
      print('classes: {}')
      print('parameters: {}')
      print(f'environment: {env}')
  | EOT
  # lint:endignore

  file { '/usr/local/bin/puppet-enc':
    ensure  => 'file',
    content => $_script,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }
}
