# @summary
#   oneshot puppet run
#
class tails::profile::puppet::oneshot {
  class { 'puppet':
    runmode         => 'unmanaged',
    manage_packages => false,
    agent           => true,
    ssldir          => '/var/lib/puppet/ssl',
    vardir          => '/var/cache/puppet',
    sharedir        => '/usr/share/puppet',
  }

  systemd::unit_file { 'puppet-oneshot.service':
    enable  => true,
    content => '[Unit]
Description=Service to run puppet agent once
After=network.target

[Service]
Type=oneshot
ExecStart=/usr/bin/puppet agent --config /etc/puppet/puppet.conf --onetime --no-daemonize --detailed-exitcode --no-usecacheonfailure
SuccessExitStatus=2
User=root
Group=root

[Install]
WantedBy=multi-user.target',
  }

  file { '/opt/puppetlabs':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
