# @summary
#   profile to ensure a node has hiera-eyaml installed and keys generated
#
# @param puppetuser
#   the posix user that should generate and own the eyaml keys
#
class tails::profile::puppet::eyaml (
  String $puppetuser = 'puppet',
) {
  ensure_packages(['hiera-eyaml'])

  file { '/etc/puppet/keys':
    ensure => directory,
    mode   => '0755',
    owner  => $puppetuser,
  }

  exec { '/usr/bin/eyaml createkeys --pkcs7-keysize=4096':
    user    => $puppetuser,
    cwd     => '/etc/puppet',
    creates => '/etc/puppet/keys/private_key.pkcs7.pem',
    require => [File['/etc/puppet/keys'], Package['hiera-eyaml']],
  }
}
