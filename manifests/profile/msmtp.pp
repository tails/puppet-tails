# @summary
#   profile to manage simple msmtp setup enabling nodes to send mail
#
# @param domain
#   the domainname we use for sending e-mail
#
# @param host
#   the server we send our mail to
#
# @param port
#   the port on abovementioned server we send our mail to
#
# @param user
#   username for submission authentication
#
# @param pass
#   password for submission authentication
#
class tails::profile::msmtp (
  String $user,
  String $pass,
  String $port    = '587',
  String $host    = 'mta.tails.net',
  String $domain  = 'tails.net',
) {
  ensure_packages(['msmtp-mta'])

  file { '/etc/msmtprc':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "defaults
tls on
tls_starttls on
auth on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
set_from_header on

account tails
port ${port}
host ${host}
from %U-${facts['networking']['hostname']}@${domain}
user ${user}
password ${pass}

account default : tails",
  }
}
