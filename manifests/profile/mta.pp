# @summary
#   profile to manage simple postfix setup enabling nodes to send mail
#
# @param myhostname
#   postfix's myhostname
#
# @param myorigin
#   postfix's myorigin
#
# @param mydestination
#   postfix's mydestination
#
# @param mynetworks
#   postfix's mynetworks
#
# @param allow_trusted_subnets
#   whether trusted subnets may send unauthenticated mail
#
# @param monitor
#   whether the mailqueue should be monitored
#
# @param extra_params
#   extra parameters to set in postfix
#
class tails::profile::mta (
  String  $myhostname            = $facts['networking']['fqdn'],
  String  $myorigin              = $facts['networking']['fqdn'],
  Array   $mydestination         = [$facts['networking']['fqdn'], $myhostname, 'localhost'],
  Array   $mynetworks            = ['127.0.0.0/8'],
  Boolean $allow_trusted_subnets = true,
  Boolean $monitor               = true,
  Hash    $extra_params          = {},
) {
  if $allow_trusted_subnets {
    $allmynetworks = $mynetworks + lookup('tirewall::trusted_subnets', Array, 'first', [])
  }
  else {
    $allmynetworks = $mynetworks
  }

  $default_params = {
    myhostname                            => $myhostname,
    myorigin                              => $myorigin,
    mydestination                         => $mydestination,
    mynetworks                            => $allmynetworks,
    alias_maps                            => 'hash:/etc/aliases',
    alias_database                        => 'hash:/etc/aliases',
    always_add_missing_headers            => 'yes',
    append_dot_mydomain                   => 'no',
    biff                                  => 'no',
    bounce_queue_lifetime                 => '1w',
    compatibility_level                   => 2,
    default_destination_concurrency_limit => 2,
    default_destination_rate_delay        => '1s',
    delay_warning_time                    => '4h',
    disable_vrfy_command                  => 'yes',
    dovecot_destination_recipient_limit   => 1,
    inet_interfaces                       => 'all',
    inet_protocols                        => 'ipv4',
    mailbox_size_limit                    => 0,
    maximal_queue_lifetime                => '1w',
    message_size_limit                    => 40960000,
    milter_default_action                 => 'accept',
    non_smtpd_milters                     => 'inet:localhost:11332',
    readme_directory                      => 'no',
    recipient_bcc_maps                    => 'hash:/etc/postfix/maps/recipient_bcc',
    recipient_delimiter                   => '+',
    schleuder_destination_recipient_limit => 1,
    sender_canonical_maps                 => 'hash:/etc/postfix/maps/sender_canonical',
    show_user_unknown_table_name          => 'no',
    smtp_dns_support_level                => 'dnssec',
    smtp_tls_CApath                       => '/etc/ssl/certs',
    smtp_tls_ciphers                      => 'high',
    smtp_tls_exclude_ciphers              => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA, AES128-SHA256, AES256-SHA256, AES128-GCM-SHA256, AES128-GCM-SHA384, DHE-RSA-DES-CBC3-SHA, ECDHE-RSA-DES-CBC3-SHA, ECDHE-ECDSA-DES-CBC3-SHA, ADH-AES256-SHA256', # lint:ignore:140chars
    smtp_tls_loglevel                     => 1,
    smtp_tls_mandatory_protocols          => '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1,TLSv1.2,TLSv1.3',
    smtp_tls_mandatory_ciphers            => 'high',
    smtp_tls_mandatory_exclude_ciphers    => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA, AES128-SHA256, AES256-SHA256, AES128-GCM-SHA256, AES128-GCM-SHA384, DHE-RSA-DES-CBC3-SHA, ECDHE-RSA-DES-CBC3-SHA, ECDHE-ECDSA-DES-CBC3-SHA, ADH-AES256-SHA256', # lint:ignore:140chars
    smtp_tls_policy_maps                  => 'hash:/etc/postfix/maps/tls_policy',
    smtp_tls_protocols                    => '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1,TLSv1.2,TLSv1.3',
    smtp_tls_security_level               => 'dane',
    smtp_tls_session_cache_database       => "btree:\${data_directory}/smtp_scache",
    smtp_tls_CAfile                       => '/etc/ssl/certs/ca-certificates.crt',
    smtpd_banner                          => '$myhostname ESMTP $mail_name (Debian/GNU)',
    smtpd_client_auth_rate_limit          => 6,
    smtpd_client_connection_rate_limit    => 60,
    smtpd_client_message_rate_limit       => 120,
    smtpd_client_restrictions             => 'permit_mynetworks, permit_sasl_authenticated',
    smtpd_data_restrictions               => 'permit_mynetworks, permit_sasl_authenticated, reject_unauth_pipelining',
    smtpd_etrn_restrictions               => 'reject',
    smtpd_helo_required                   => 'yes',
    smtpd_helo_restrictions               => 'permit_mynetworks, permit_sasl_authenticated, reject_invalid_helo_hostname, reject_non_fqdn_helo_hostname, reject_unknown_helo_hostname', # lint:ignore:140chars
    smtpd_milters                         => 'inet:localhost:11332',
    smtpd_recipient_restrictions          => 'permit_mynetworks, permit_sasl_authenticated, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname', # lint:ignore:140chars
    smtpd_relay_restrictions              => 'permit_mynetworks, permit_sasl_authenticated, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname', # lint:ignore:140chars
  }

  $all_params = deep_merge($default_params,$extra_params)

  class { 'postfix':
    parameters => $all_params,
    maps       => {
      tls_policy       => {
        type     => 'hash',
        contents => [
          '*          dane',
        ],
      },
      recipient_bcc    => {
        type     => 'hash',
        contents => [],
      },
      sender_canonical => {
        type     => 'hash',
        contents => [],
      },
      transport        => {
        type     => 'hash',
        contents => [],
      },
      virtual_alias    => {
        type     => 'hash',
        contents => [],
      },
      vmailbox         => {
        type     => 'hash',
        contents => [],
      },
    },
  }

  include tails::profile::rspamd
  if $monitor {
    include monitoring::checkcommands::mailqueue
  }
}
