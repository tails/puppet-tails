# @summary
#   Manage Schleuder and its MTA
#
# @param superadmin_alias
#   the superadmin address
#
# @param myhostname
#   postfix's myhostname
#
# @param sslname
#   CN to use for ssl certs
#
# @param myorigin
#   postfix's myorigin
#
# @param schleuder_domain
#   the maildomain
#
# @param mynetworks
#   array of postfix mynetworks
#
# @param extra_transport
#   add this line to the transport maps
#
# @param local_web
#   if true, set up a local web server for LE certs and mta-sts
#
# @param lists
#   hash of all unfiltered schleuder lists
#
# @param sieved_lists
#   hash of all schleuder lists that are filtered with sieve
#
# @param mailboxes
#   array of all imap mailboxes
#
# @param virtual_domains
#   domains besides the schleuder_domain we host lists for
#
class tails::profile::schleuder (
  String  $superadmin_alias,
  String  $myhostname       = 'lizard.tails.net',
  String  $myorigin         = 'mail.tails.net',
  String  $sslname          = $myhostname,
  String  $schleuder_domain = 'boum.org',
  Array   $mynetworks       = ['127.0.0.0/8'],
  Array   $virtual_domains  = [],
  String  $extra_transport  = undef,
  Boolean $local_web        = false,
  Hash    $lists            = {},
  Hash    $sieved_lists     = {},
  Hash    $mailboxes        = {},
) {
  ### TODO

  ### Sanity checks

  if $facts['os']['name'] != 'Debian' or versioncmp($facts['os']['distro']['release']['major'], '11') < 0 {
    fail('The schleuder profile only supports Debian Bullseye or higher.')
  }

  ### Dependencies

  include yapgp
  ensure_packages(['ruby-base32','dovecot-managesieved','dovecot-antispam'])

  ### Monitoring

  include monitoring::checkcommands::mailqueue

  ### TLS

  unless $local_web {
    # Manage {cert,fullchain,privkey}.pem by collecting resources
    # exported by tails::nginx::exportcert
    File <<| tag == "tails_mail_tls_${sslname}" |>>

    $smtpd_tls_cafile    = "/etc/ssl/${sslname}/fullchain.pem"
    $smtpd_tls_cert_file = "/etc/ssl/${sslname}/cert.pem"
    $smtpd_tls_key_file  = "/etc/ssl/${sslname}/privkey.pem"

    # Restart postfix when TLS cert is changed
    File[$smtpd_tls_cafile] ~> Service['postfix']
    File[$smtpd_tls_cert_file] ~> Service['postfix']
    File[$smtpd_tls_key_file] ~> Service['postfix']
  }

  ### Postfix

  $virtual_mailbox_domains = [$schleuder_domain] + $virtual_domains

  $extra_params = {
    smtpd_sasl_type                       => 'dovecot',
    smtpd_sasl_path                       => 'private/auth',
    smtpd_sasl_auth_enable                => 'yes',
    smtpd_use_tls                         => 'yes',
    smtpd_tls_exclude_ciphers             => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA, AES128-SHA256, AES256-SHA256, AES128-GCM-SHA256, AES128-GCM-SHA384, DHE-RSA-DES-CBC3-SHA, ECDHE-RSA-DES-CBC3-SHA, ECDHE-ECDSA-DES-CBC3-SHA, ADH-AES256-SHA256', # lint:ignore:140chars
    smtpd_tls_mandatory_exclude_ciphers   => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA, AES128-SHA256, AES256-SHA256, AES128-GCM-SHA256, AES128-GCM-SHA384, DHE-RSA-DES-CBC3-SHA, ECDHE-RSA-DES-CBC3-SHA, ECDHE-ECDSA-DES-CBC3-SHA, ADH-AES256-SHA256', # lint:ignore:140chars
    smtpd_tls_mandatory_protocols         => '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1,TLSv1.2,TLSv1.3',
    smtpd_tls_protocols                   => '!SSLv2,!SSLv3,!TLSv1,!TLSv1.1,TLSv1.2,TLSv1.3',
    smtpd_tls_auth_only                   => 'yes',
    smtpd_tls_CAfile                      => $smtpd_tls_cafile,
    smtpd_tls_cert_file                   => $smtpd_tls_cert_file,
    smtpd_tls_key_file                    => $smtpd_tls_key_file,
    smtpd_tls_session_cache_database      => "btree:\${data_directory}/smtpd_scache",
    transport_maps                        => 'hash:/etc/postfix/maps/transport',
    virtual_alias_maps                    => 'hash:/etc/postfix/maps/virtual_alias',
    virtual_mailbox_domains               => $virtual_mailbox_domains,
    virtual_mailbox_base                  => '/var/spool/mail',
  }

  class { 'tails::profile::mta':
    myhostname   => $myhostname,
    myorigin     => $myorigin,
    mynetworks   => $mynetworks,
    extra_params => $extra_params,
  }

  concat::fragment { "postfix::map: bcc to monitor spam on ${schleuder_domain}":
    target  => '/etc/postfix/maps/recipient_bcc',
    content => "@${schleuder_domain} spamcatcher@${schleuder_domain}\n",
  }

  concat::fragment { "postfix::map: transport map for spamcatcher on ${schleuder_domain}":
    target  => '/etc/postfix/maps/transport',
    content => "spamcatcher@${schleuder_domain} dovecot:\n",
  }

  concat::fragment { "postfix::map: virtual aliasses for ${schleuder_domain}":
    target  => '/etc/postfix/maps/virtual_alias',
    content => "postmaster@${schleuder_domain}    ${tails::profile::sysadmins::email}
MAILER-DAEMON@${schleuder_domain} ${tails::profile::sysadmins::email}
webmaster@${schleuder_domain}     ${tails::profile::sysadmins::email}
www@${schleuder_domain}           ${tails::profile::sysadmins::email}
root@${schleuder_domain}          ${tails::profile::sysadmins::email}
hostmaster@${schleuder_domain}    ${tails::profile::sysadmins::email}
abuse@${schleuder_domain}         ${tails::profile::sysadmins::email}
noc@${schleuder_domain}           ${tails::profile::sysadmins::email}
security@${schleuder_domain}      ${tails::profile::sysadmins::email}",
  }

  postfix::config::service { 'schleuder':
    master_cf_file => '/etc/postfix/master.cf',
    command        => 'pipe',
    unpriv         => 'n',
    chroot         => 'n',
    args           => ["flags=DRhu user=schleuder argv=/usr/bin/schleuder work \${recipient}"],
  }

  postfix::config::service { 'dovecot':
    master_cf_file => '/etc/postfix/master.cf',
    command        => 'pipe',
    unpriv         => 'n',
    chroot         => 'n',
    args           => ["flags=DRhu user=vmail:vmail argv=/usr/lib/dovecot/dovecot-lda -f \${sender} -d \${recipient}"],
  }

  postfix::config::service { 'submission':
    master_cf_file => '/etc/postfix/master.cf',
    type           => 'inet',
    command        => 'smtpd',
    priv           => 'n',
    chroot         => 'y',
    args           => ['-o syslog_name=postfix/submission',
      '-o smtpd_tls_security_level=encrypt',
      '-o smtpd_sasl_auth_enable=yes',
      '-o smtpd_relay_restrictions=permit_sasl_authenticated,reject',
      '-o smtpd_recipient_restrictions=permit_sasl_authenticated,reject',
      '-o smtpd_sasl_type=dovecot',
      '-o smtpd_sasl_path=private/auth',
    ],
  }

  if $extra_transport {
    concat::fragment { "postfix::map: list transport fragment ${schleuder_domain}":
      target  => '/etc/postfix/maps/transport',
      content => "${extra_transport}\n",
      order   => 999,
    }
  }

  ### Schleuder
  include rbac

  class { 'schleuder':
    cli_api_key   => sha1("${fqdn_rand(1204, 'cli')}"),
    gpg_use_tor   => false,
    superadmin    => $superadmin_alias,
    gpg_keyserver => 'hkps://keys.openpgp.org',
    require       => Package['ruby-base32'],
  }

  # create lists

  $all_lists = $lists + $sieved_lists

  keys($all_lists).each |String $list_name| {
    $admin_email = $rbac::users[$all_lists[$list_name]['admin']]['email']
    $admin_fp = $rbac::users[$all_lists[$list_name]['admin']]['pgpkey']

    pgp_key { "${list_name}-${admin_fp}":
      ensure => present,
      fp     => $admin_fp,
      user   => 'schleuder',
      trust  => 4,
    }

    if Deferred('export_pgp_key', [$admin_fp, 'schleuder']) {
      file { "/tmp/${list_name}-admin.asc":
        ensure  => file,
        content => Deferred('export_pgp_key', [$admin_fp, 'schleuder']),
        mode    => '0644',
      }

      schleuder::list { $list_name:
        admin           => $admin_email,
        admin_publickey => "/tmp/${list_name}-admin.asc",
        require         => [Class['schleuder'], File["/tmp/${list_name}-admin.asc"]],
      }
    }
  }

  # set up postfix transports

  keys($lists).each |String $list_name| {
    ['', '-request', '-owner', '-bounce', '-sendkey'].each |String $suffix| {
      $address = join(split($list_name, '@'), "${suffix}@")
      concat::fragment { "postfix::map: list transport fragment ${address}":
        target  => '/etc/postfix/maps/transport',
        content => "${address} schleuder:\n",
      }
    }
  }

  # set up postfix transports for mailboxes

  concat { '/usr/local/etc/virtual_accounts_passwd':
    ensure => present,
    owner  => 'root',
    group  => 'dovecot',
    mode   => '0640',
  }

  concat::fragment { "dovecot passwd entry for ${address}":
    target  => '/usr/local/etc/virtual_accounts_passwd',
    content => "spamcatcher@${schleuder_domain}:{SSHA512}AOzW3N41p77TjDRkAMbi1HkBIFKsZX2usA3xPu+ZzlvUbl0CTOVKE5ems2qX9pwx9eg8MVN5iPDgx1JACmoQmSe5E5U=\n",  # lint:ignore:140chars
  }

  $mailboxes.each |String $address, String $passhash| {
    concat::fragment { "postfix::map: list transport fragment ${address}":
      target  => '/etc/postfix/maps/transport',
      content => "${address} dovecot:\n",
    }
    concat::fragment { "dovecot passwd entry for ${address}":
      target  => '/usr/local/etc/virtual_accounts_passwd',
      content => "${address}:${passhash}\n",
    }
  }

  # clean up the old spamcatcher mails

  exec { "find /srv/mail/spamcatcher@${schleuder_domain}/Maildir/new -type f -ctime +3 -delete":
    user  => vmail,
  }

  # set up postfix transports and sieve filters

  keys($sieved_lists).each |String $list_name| {
    ['-request', '-owner', '-bounce', '-sendkey'].each |String $suffix| {
      $address = join(split($list_name, '@'), "${suffix}@")
      concat::fragment { "postfix::map: list transport fragment ${address}":
        target  => '/etc/postfix/maps/transport',
        content => "${address} schleuder:\n",
      }
    }
    concat::fragment { "postfix::map: list transport fragment ${list_name}":
      target  => '/etc/postfix/maps/transport',
      content => "${list_name} dovecot:\n",
    }
    $finaldest = join(split($list_name, '@'), '-final@')
    $finaltransport = split($list_name, '@')[0]
    concat::fragment { "postfix::map: list transport fragment ${finaldest}":
      target  => '/etc/postfix/maps/transport',
      content => "${finaldest} schleuder-final-${finaltransport}:\n",
    }
    postfix::config::service { "schleuder-final-${finaltransport}":
      master_cf_file => '/etc/postfix/master.cf',
      command        => 'pipe',
      unpriv         => 'n',
      chroot         => 'n',
      args           => ["flags=DRhu user=schleuder argv=/usr/bin/schleuder work ${list_name}"],
    }
    $subject = $sieved_lists[$list_name]['autoreply_subject']
    $content = $sieved_lists[$list_name]['autoreply_content']
    if 'autoreply_customcondition' in $sieved_lists[$list_name] {
      $custom = $sieved_lists[$list_name]['autoreply_customcondition']
    }
    else {
      $custom = ''
    }
    if 'autoreply_onlyto' in $sieved_lists[$list_name] {
      $onlyto = $sieved_lists[$list_name]['autoreply_onlyto']
      $addresses = ":addresses [\"${onlyto}\"] "
    }
    else {
      $addresses = ''
    }
    file { "/srv/mail/${list_name}":
      ensure => directory,
      mode   => '0700',
      owner  => 'vmail',
      group  => 'vmail',
    }
    file { "/srv/mail/${list_name}/.dovecot.sieve":
      ensure  => file,
      mode    => '0640',
      owner   => 'vmail',
      group   => 'vmail',
      content => "require [\"fileinto\", \"envelope\", \"vacation\"];\n${custom}\nvacation ${addresses}:subject \"${subject}\"\n  \"${content}\";\nredirect \"${finaldest}\";\n", # lint:ignore:140chars
      require => File["/srv/mail/${list_name}"],
    }
  }

  ### Whitelist for automated messages

  $filters_dir = '/usr/local/lib/schleuder/filters'

  file { "${filters_dir}/pre_decryption":
    ensure => directory,
    owner  => 'root',
    group  => 'staff',
    mode   => '0755',
  }

  file { "${filters_dir}/pre_decryption/01_automated_messages_whitelist.rb":
    ensure  => file,
    source  => 'puppet:///modules/tails/schleuder/01_automated_messages_whitelist.rb',
    owner   => 'root',
    group   => 'staff',
    mode    => '0644',
    require => File["${filters_dir}/pre_decryption"],
    notify  => Service['schleuder-api-daemon'],
  }

  ### Dovecot (for sieve filtering)

  user { 'vmail':
    ensure     => present,
    home       => '/srv/mail',
    managehome => true,
    shell      => '/usr/sbin/nologin',
  }

  class { 'dovecot':
    plugins => ['imap', 'sieve'],
    config  => {
      protocols     => 'imap sieve',
      listen        => '*, ::',
      mail_uid      => 'vmail',
      mail_home     => '/srv/mail/%u',
      mail_location => 'maildir:~/Maildir',
    },
    configs => {
      '10-auth'      => {
        passdb          => {
          driver => 'passwd-file',
          args   => 'scheme=ssha512 username_format=%u /usr/local/etc/virtual_accounts_passwd',
        },
        userdb          => {
          driver => 'static',
          args   => 'uid=vmail gid=vmail home=/srv/mail/%u',
        },
        auth_mechanisms => 'plain login',
      },
      '10-master'    => {
        'service auth'              => {
          user                                            => 'root',
          'unix_listener /var/spool/postfix/private/auth' => {
            mode  => '0660',
            user  => 'postfix',
            group => 'postfix',
          },
        },
        'service managesieve-login' => {
          'inet_listener sieve' => {
            port => 4190,
          },
        },
      },
      '10-ssl'       => {
        ssl      => 'required',
        ssl_cert => "<${smtpd_tls_cafile}",
        ssl_key  => "<${smtpd_tls_key_file}",
      },
      '15-mailboxes' => {
        'namespace inbox' => {
          inbox            => 'yes',
          separator        => '/',
          subscriptions    => 'yes',
          'mailbox Trash'  => {
            special_use => '\Trash',
            auto        => 'subscribe',
            autoexpunge => '30d',
          },
          'mailbox Sent'   => {
            special_use => '\Sent',
            auto        => 'subscribe',
          },
          'mailbox Junk'   => {
            special_use => '\Junk',
            auto        => 'subscribe',
            autoexpunge => '30d',
          },
          'mailbox Drafts' => {
            special_use => '\Drafts',
            auto        => 'subscribe',
          },
        },
      },
      '15-lda'       => {
        'protocol lda' => {
          mail_plugins => '$mail_plugins sieve',
        },
      },
      '20-imap'      => {
        'protocol imap'    => {
          mail_plugins => '$mail_plugins imap_sieve antispam',
        },
      },
      '25-stats'     => {
        'service stats' => {
          'unix_listener stats-reader' => {
            user  => 'vmail',
            group => 'vmail',
            mode  => '0660',
          },
          'unix_listener stats-writer' => {
            user  => 'vmail',
            group => 'vmail',
            mode  => '0660',
          },
        },
      },
      '90-sieve'     => {
        plugin => {
          sieve => 'file:/srv/mail/%u/sieve;active=/srv/mail/%u/.dovecot.sieve',
        },
      },
      '90-antispam'  => {
        plugin => {
          antispam_backend            => 'pipe',
          antispam_spam               => 'Junk',
          antispam_trash              => 'Trash',
          antispam_mail_sendmail      => '/usr/bin/rspamc',
          antispam_mail_spam          => 'learn_spam',
          antispam_mail_notspam       => 'learn_ham',
          antispam_mail_sendmail_args => '-h;localhost:11334',
        },
      },
    },
    require => Package['dovecot-managesieved'],
  }
}
