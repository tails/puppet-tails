# @summary
#   Manage a nginx vhost that reverse proxies to a LimeSurvey instance.
#   This needs to be refactored into the limesurvey profile and removed.
#
# @param public_hostname
#   the hostname
#
# @param upstream_hostname
#   where to proxy to
#
# @param upstream_port
#   the port to proxy to
#
class tails::profile::limesurvey::reverse_proxy (
  Stdlib::Fqdn $public_hostname     = 'survey.tails.net',
  Stdlib::Fqdn $upstream_hostname   = 'survey.lizard',
  Stdlib::Port $upstream_port       = 80,
) {
  tails::profile::nginx::reverse_proxy { 'limesurvey':
    public_hostname   => $public_hostname,
    upstream_hostname => $upstream_hostname,
    upstream_port     => $upstream_port,
  }
}
