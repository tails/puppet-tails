# @summary
#   Manage vim.
#
# @example
#   include tails::profile::vim
#
class tails::profile::vim (
) {
  package { 'vim':
    ensure => present,
    name   => vim,
  }
  exec { 'update-alternatives --set editor /usr/bin/vim.basic':
    path   => '/bin:/sbin:/usr/bin:/usr/sbin',
    unless => 'test /etc/alternatives/editor -ef /usr/bin/vim.basic',
  }
  file { '/etc/vim/vimrc.local':
    content => 'syntax on
set background=dark
set showcmd            " Show (partial) command in status line.
set showmatch          " Show matching brackets.
set ignorecase         " Do case insensitive matching
set smartcase          " Do smart case matching
set incsearch          " Incremental search 
set autowrite          " Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
"set mouse=a            " Enable mouse usage (all modes) in terminals',
    owner   => root,
    group   => root,
    mode    => '0644',
  }
}
