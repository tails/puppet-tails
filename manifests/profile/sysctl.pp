# @summary
#   Manage kernel parameters.
#
# @example
#   include tails::profile::sysctl
#
# @param values
#   A hash with further sysctl values to set.
#
class tails::profile::sysctl (
  Hash $values = {},
) {
  sysctl::value { 'kernel.panic': value => 10 }
  sysctl::value { 'kernel.perf_event_paranoid': value => 2 }
  sysctl::value { 'kernel.unprivileged_bpf_disabled': value => 1 }

  $values.each | String $name, $value | {
    sysctl::value { $name: value => $value }
  }
}
