# @summary
#   A mail server for tests run by jenkins agents
#
# @param public_hostname
#   the public hostname of the mail server
#
class tails::profile::jenkins::mail (
  String $public_hostname = 'isoworkers-mail.tails.net',
) {
  class { 'tails::profile::testermta':
    email_user => "test@${public_hostname}",
    myhostname => $public_hostname,
    myorigin   => $public_hostname,
    ssl_cert   => "/etc/ssl/certs/${public_hostname}.pem",
    ssl_key    => "/etc/ssl/private/${public_hostname}.key",
  }

  class { 'tails::profile::jenkins::dovecot':
    hostname      => $public_hostname,
    ssl_cert      => "/etc/ssl/certs/${public_hostname}.pem",
    ssl_key       => "/etc/ssl/private/${public_hostname}.key",
    include_files => [],
  }

  tails::profile::nginx::server { $public_hostname:
    ssl       => false,
    locations => {
      "${public_hostname}/.well-known/acme-challenge" => {
        location    => '/.well-known/acme-challenge/',
        www_root    => '/var/www/html',
        index_files => [],
      },
    },
  }

  letsencrypt::certonly { $public_hostname:
    plugin               => 'webroot',
    webroot_paths        => ['/var/www/html'],
    manage_cron          => false,
    deploy_hook_commands => [],
  }

  groupmembership { 'ssl-cert':
    members => ['dovecot', 'postfix'],
  }

  file { "/etc/ssl/private/${public_hostname}.key":
    ensure => file,
    source => "file:///etc/letsencrypt/live/${public_hostname}/privkey.pem",
    links  => 'follow',
    owner  => 'root',
    group  => 'ssl-cert',
    mode   => '0640',
  }

  file { "/etc/ssl/certs/${public_hostname}.pem":
    ensure => file,
    source => "file:///etc/letsencrypt/live/${public_hostname}/fullchain.pem",
    links  => 'follow',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  ensure_resources('tirewall::public_service', {
      'HTTP' => {
        proto => tcp,
        dport => 80,
      },
      'HTTPS' => {
        proto => tcp,
        dport => 443,
      },
      'smtp' => {
        proto => tcp,
        dport => 25,
      },
      'pop3' => {
        proto => tcp,
        dport => 110,
      },
      'imap2' => {
        proto => tcp,
        dport => 143,
      },
      'submissions' => {
        proto => tcp,
        dport => 465,
      },
      'submission' => {
        proto => tcp,
        dport => 587,
      },
      'imaps' => {
        proto => tcp,
        dport => 993,
      },
      'pop3s' => {
        proto => tcp,
        dport => 995,
      },
  })
}
