# @summary
#   profile to manage an nginx vhost that reverse proxies to a Jenkins instance.
#
# @param public_hostname
#   the public hostname
#
# @param upstream_hostname
#   where to proxy to
#
# @param upstream_port
#   which port to proxy to
#
# @param basic_auth_exceptions
#   hash with exceptions for authentication
#
class tails::profile::jenkins::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'jenkins.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'jenkins.dragon',
  Stdlib::Port $upstream_port     = 8080,
  Hash $basic_auth_exceptions     = {},
) {
  include rbac
  $grantees = unique($rbac::roles['sysadmins'] + $rbac::roles['foundations-team-members'] + $rbac::roles['foundations-team-helpers'] + $rbac::roles['ux-team-members']) # lint:ignore:140chars
  $htpasswds = join($grantees.map | $user | { "${user}:${rbac::users[$user]['htpasswd']}" }, "\n")
  file { '/etc/nginx/auth.d/htpasswd.jenkins':
    ensure  => file,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0640',
    content => $htpasswds,
    require => File['/etc/nginx/auth.d'],
  } ~> Service['nginx']

  $auth_conf = '/etc/nginx/conf.d/jenkins_basic_auth.conf'

  concat { $auth_conf:
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Service['nginx'],
  }

  concat::fragment { 'jenkins basic_auth config header':
    target => $auth_conf,
    source => 'puppet:///modules/tails/profile/jenkins/basic_auth.header.conf',
    order  => '01',
  }

  Concat::Fragment <<| target == $auth_conf |>>

  concat::fragment { 'jenkins basic_auth config footer':
    target => $auth_conf,
    source => 'puppet:///modules/tails/profile/jenkins/basic_auth.footer.conf',
    order  => '99',
  }

  $auth_params = {
    auth_basic           => '$jenkins_basic_auth',
    auth_basic_user_file => 'auth.d/htpasswd.jenkins',
  }

  tails::profile::nginx::reverse_proxy { 'jenkins':
    public_hostname   => $public_hostname,
    upstream_hostname => $upstream_hostname,
    upstream_port     => $upstream_port,
    auth_params       => $auth_params,
    proxy_set_header  => [
      'Host $host',
      'X-Real-IP $remote_addr',
      'X-Forwarded-For $proxy_add_x_forwarded_for',
      'Proxy ""',
      'X-Forwarded-Proto $scheme',
      'Authorization ""',
      'X-Forwarded-User $remote_user',
      'X-Forwarded-Group ""',
    ],
    require           => [
      Concat[$auth_conf],
      File['/etc/nginx/auth.d/htpasswd.jenkins'],
    ],
  }
}
