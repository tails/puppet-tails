# @summary
#   Install the cronjob and scripts necessary to automatically update the
#   build_Tails_ISO_* jobs in Jenkins.
#
# @param active_days
#   how many days to stay active
#
# @param jenkins_jobs_repo
#   location of the jenkins jobs git repository
#
# @param ensure
#   whether we want the jobs generator
#
# @param tails_repo
#   location of the tails git repository
#
# @param tails_repo_rev
#   revision of the tails git repository
#
# @param clone_basedir
#   directory where we clone the tails repo
#
class tails::profile::jenkins::iso_jobs_generator (
  Integer $active_days,
  String $jenkins_jobs_repo,
  Enum['present', 'absent'] $ensure   = 'present',
  String $tails_repo                  = 'https://gitlab.tails.boum.org/tails/tails.git',
  String $tails_repo_rev              = 'stable',
  Stdlib::Absolutepath $clone_basedir = '/var/lib/tails_pythonlib',
) {
  file { '/var/lib/jenkins/.gitconfig':
    ensure  => $ensure,
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0640',
    content => "[user]\n  name = jenkins\n  email = jenkins@${facts['networking']['fqdn']}\n",
  }

  $wc_dir = "${clone_basedir}/tails"
  $pythonpath = "${wc_dir}/config/chroot_local-includes/usr/lib/python3/dist-packages"

  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }

  file { $clone_basedir:
    ensure => $directory_ensure,
    owner  => 'jenkins',
    group  => 'jenkins',
    mode   => '0755',
  }

  vcsrepo { $wc_dir:
    ensure   => $ensure,
    provider => git,
    source   => $tails_repo,
    revision => $tails_repo_rev,
    user     => 'jenkins',
    require  => File[$clone_basedir],
  }

  ensure_packages(
    ['python3-yaml', 'python3-sh'],
    { 'ensure' => $ensure }
  )

  file { '/usr/local/sbin/generate_tails_iso_jobs':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/generate_tails_iso_jobs',
    require => Vcsrepo[$wc_dir],
  }

  file { '/usr/local/sbin/update_tails_iso_jobs':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/update_tails_iso_jobs',
    require => File['/usr/local/sbin/generate_tails_iso_jobs'],
  }

  cron { 'update_tails_iso_jobs':
    ensure      => $ensure,
    minute      => '*/2',
    user        => 'jenkins',
    command     => "flock -n /run/lock/update_tails_iso_jobs /usr/local/sbin/update_tails_iso_jobs '${active_days}' '${tails_repo}' '${jenkins_jobs_repo}'",  # lint:ignore:140chars -- command
    environment => "PYTHONPATH='${pythonpath}'",
    require     => File['/usr/local/sbin/update_tails_iso_jobs'],
  }
}
