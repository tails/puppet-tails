# @summary
#   Manage a jenkins worker.
#
# @example
#   include tails::profile::isoworker
#
class tails::profile::jenkins::isoworker () {
# allow FT and helpers to ssh in as user jenkins
  rbac::ssh { 'foundations-team-helpers-to-jenkins':
    user    => 'jenkins',
    role    => 'foundations-team-helpers',
    require => User['jenkins'],
  }
  rbac::ssh { 'foundations-team-members-to-jenkins':
    user    => 'jenkins',
    role    => 'foundations-team-members',
    require => User['jenkins'],
  }

# set our role as fact to be able to access role-based hiera
  tails::profile::facts_d::file { 'role':
    content => {
      role => 'isoworker',
    },
  }

# install the necessary packages
  $packages = [
    'dnsmasq-base',
    'ebtables',
    'faketime',
    'git',
    'jq',
    'libvirt-daemon-system',
    'pigz',
    'python3-gitlab',
    'qemu-system-x86',
    'qemu-utils',
    'rake',
    'sudo',
    'vagrant',
    'vagrant-libvirt',
    'vmdb2',
  ]

  ensure_packages($packages)

  @@sshkeys::create_key { "root@${facts['networking']['fqdn']}":
    keytype => 'rsa',
    length  => 4096,
    tag     => 'ssh_keymaster',
  }

# allow unauthenticated access to the Jenkins web interface
  include tails::profile::jenkins::isoworker::reverse_proxy_exception

# firewall config
  include tails::profile::jenkins::isoworker::firewall_exports
  include tails::profile::jenkins::isoworker::firewall_rules

  include tails::profile::jenkins::isotester
  include tails::profile::jenkins::isobuilder
# For the build_IUKs Jenkins job
  include tails::profile::jenkins::iuk_builder

# For the build_website_* Jenkins jobs
  include tails::profile::website::builder
}
