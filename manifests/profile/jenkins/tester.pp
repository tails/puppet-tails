# @summary
#   Set up what's necessary to run automated tests
#   This should be refactored
#
# @param manage_temp_dir_mount
#   whether puppet should manage the mounting of the temp dir
#
# @param temp_dir
#   the path to the temp dir
#
# @param temp_dir_backing_device
#   in case we manage the mount here, what to mount
#
# @param temp_dir_fs_type
#   in case we manage the mount here, the filesystem type
#
# @param temp_dir_mount_options
#   in case we manage the mount here, the mount options
#
class tails::profile::jenkins::tester (
  Boolean $manage_temp_dir_mount            = false,
  String $temp_dir                          = '/tmp/TailsToaster',
  Optional[String] $temp_dir_backing_device = undef,
  String $temp_dir_fs_type                  = 'ext4',
  String $temp_dir_mount_options            = 'relatime,acl',
) {
  ### Sanity checks

  if $facts['os']['distro']['codename'] !~ /^(bookworm)$/ {
    warning('The tails::profile::jenkins::tester class only supports Debian Bookworm.')
  }

  $tester_packages = [
    'cucumber',
    'devscripts',
    'dnsmasq-base',
    'ffmpeg',
    'gawk',
    'git',
    'imagemagick',
    'libcap2-bin',
    'libnss3-tools',
    'libvirt0',
    'libvirt-clients',
    'libvirt-daemon-system',
    'libvirt-dev',
    'obfs4proxy',
    'ovmf',
    'pry',
    'python3-opencv',
    'python3-pil',
    'python3-slixmpp',
    'qemu-system-x86',
    'qrencode',
    'redir',
    'ruby-bindex',
    'ruby-binding-of-caller',
    'ruby-guestfs',
    'ruby-json',
    'ruby-libvirt',
    'ruby-net-dns',
    'ruby-net-irc',
    'ruby-packetfu',
    'ruby-rb-inotify',
    'ruby-rjb',
    'ruby-rspec',
    'ruby-test-unit',
    'seabios',
    'tcpdump',
    'tcplay',
    'tesseract-ocr',
    'tesseract-ocr-eng',
    'unclutter',
    'virt-viewer',
    'x11vnc',
    'x264',
    'xdotool',
    'xtightvncviewer',
    'xvfb',
  ]

  ensure_packages($tester_packages)

  # Not needed
  package { 'qemu-system-gui': ensure => absent }

  file { '/etc/tmpfiles.d/TailsToaster.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "D  ${temp_dir}  0755  root  root  -\n",
  }

  if $manage_temp_dir_mount {
    validate_string(
      $temp_dir_backing_device,
      $temp_dir_fs_type,
      $temp_dir_mount_options
    )

    mount { $temp_dir:
      ensure  => mounted,
      device  => $temp_dir_backing_device,
      fstype  => $temp_dir_fs_type,
      options => $temp_dir_mount_options,
      require => File['/etc/tmpfiles.d/TailsToaster.conf'],
    }
  }
}
