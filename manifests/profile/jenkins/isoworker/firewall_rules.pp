# @summary
#   Configure firewall rules needed for an agent to function properly.
#
# @example
#   include tails::profile::jenkins::isoworker::firewall_rules
#
# @param local_resolver
#   The IP address of a local DNS resolver to use when trying to reach OpenDNS.
#
class tails::profile::jenkins::isoworker::firewall_rules (
  Stdlib::IP::Address::V4 $local_resolver = '192.168.122.1',
) {
  firewall {
    # See: https://tails.net/contribute/release_process/test/setup/#index4h2
    '300 allow connections from VMs back to the host system':
      table   => 'filter',
      chain   => 'INPUT',
      iniface => 'virbr+',
      action  => 'accept';
    # Redirect to a local resolver the UDP DNS requests that isoworker VMs
    # send to the OpenDNS nameserver when running the test suite
    # (incoming UDP traffic is blocked due to DDoS => #16351)
    '300 ISO worker OpenDNS to local resolver':
      table       => 'nat',
      chain       => 'OUTPUT',
      proto       => 'udp',
      destination => '208.67.222.222', # OpenDNS (see SOME_DNS_SERVER in tails.git:features/support/config.rb)
      dport       => 53,
      todest      => $local_resolver,
      jump        => 'DNAT';
    # For all the rules below, see: https://gitlab.tails.boum.org/tails/sysadmin/-/issues/17972
    '300 ISO tester to puppet-git.lizard (SSH)':
      table       => 'nat',
      chain       => 'OUTPUT',
      destination => '192.168.122.1',
      dport       => 3004,
      todest      => '192.168.122.2:22',  # puppet-git.lizard
      jump        => 'DNAT';
    '300 ISO tester to misc-git.lizard (SSH)':
      table       => 'nat',
      chain       => 'OUTPUT',
      destination => '192.168.122.1',
      dport       => 3006,
      todest      => '192.168.122.14:22',  # misc.lizard
      jump        => 'DNAT';
  }
}
