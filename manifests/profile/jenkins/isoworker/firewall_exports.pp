# @summary
#   Export firewall rules needed for an agent to function properly.
#
# @example
#   include tails::profile::jenkins::isoworker::firewall_exports
#
class tails::profile::jenkins::isoworker::firewall_exports () {
  $export = {
    'Jenkins Orchestrator (API ports)'        => {
      dport  => [8080, 42585],
      target => 'jenkins.dragon',
    },
    'DNS'                                     => {
      dport  => 53,
      target => 'lizard.tails.net',
    },
    'Test Mail Server'                        => {
      dport  => [80, 443, 25, 465, 587, 993, 995],
      target => 'isoworkers-mail.iguana',
    },
  }
  $export.each | String $name, Hash $params | {
    $_params = {
      table  => 'filter',
      chain  => 'INPUT',
      source => $facts['networking']['ip'],
      dport  => $params['dport'],
      action => 'accept',
    }
    @@tirewall::rule { "300 allow Jenkins agent into ${name} (exported from ${trusted['certname']})":
      params => $_params,
      tag    => "target_node:${params['target']}",
    }
  }
}
