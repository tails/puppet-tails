# @summary
#   Export an exception for the Jenkins reverse proxy HTTP Basic Auth config
class tails::profile::jenkins::isoworker::reverse_proxy_exception () {
  @@concat::fragment { "jenkins basic_auth config for ${facts['networking']['fqdn']}":
    target  => '/etc/nginx/conf.d/jenkins_basic_auth.conf',
    content => "  ${facts['networking']['ip']}/32 off;\n",
  }
}
