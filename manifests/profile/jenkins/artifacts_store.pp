# @summary
#   Serve Jenkins artifacts over HTTP.
#
# @param artifacts_store_path
#   path to where artifacts are stored
#
# @param hostnames
#   a list of hostnames where to serve artifacts
#
# @param ensure
#   to serve or not to serve
#
class tails::profile::jenkins::artifacts_store (
  Stdlib::Absolutepath $artifacts_store_path,
  Array[Stdlib::Fqdn] $hostnames,
  Enum['present', 'absent'] $ensure = 'present',
) {
  $hostnames.each | Stdlib::Fqdn $hostname | {
    tails::profile::nginx::server { $hostname:
      ssl        => true,
      www_root   => $artifacts_store_path,
      raw_append => [
        'types {',
        '  text/plain apt-sources;',
        '  text/plain binpkgs;',
        '  text/plain build-manifest;',
        '  text/plain buildlog;',
        '  text/plain packages;',
        '  text/plain shasum;',
        '  text/plain asc;',
        '  text/plain srcpkgs;',
        '}',
      ],
      locations  => {
        "${hostname} root"      => {
          location             => '/',
          ssl_only             => true,
          index_files          => [],
          location_cfg_prepend => {
            fancyindex             => 'on',
            fancyindex_exact_size  => 'off',
            fancyindex_name_length => '120',
          },
        },
        "${hostname} debug.log" => {
          ssl_only            => true,
          index_files         => [],
          location            => '~ /(debug[.])?log$',
          location_cfg_append => { 'return' => 403 },
          priority            => 501,
        },
        "${hostname} buildlogs" => {
          ssl_only            => true,
          index_files         => [],
          location            => '~ [.]buildlog$',
          location_cfg_append => { 'return' => 403 },
          priority            => 502,
        },
      },
      require    => [
        Package['libnginx-mod-http-fancyindex'],
        File[$artifacts_store_path],
      ],
    }
  }
}
