# @summary
#   Manage a jenkins isobuilder.
#
# @example
#   include tails::profile::isobuilder
#
# @param gitlab_apikey
#   the API key for gitlab access
#
class tails::profile::jenkins::isobuilder (
  String $gitlab_apikey = 'secret',
) {
  include tails::profile::website::builder

# install the necessary packages
  $packages = [
    'dnsmasq-base',
    'ebtables',
    'faketime',
    'git',
    'jq',
    'libvirt-daemon-system',
    'pigz',
    'qemu-system-x86',
    'qemu-utils',
    'rake',
    'sudo',
    'vagrant',
    'vagrant-libvirt',
    'vmdb2',
  ]

  ensure_packages($packages)

# allow unauthenticated access to the Jenkins web interface
  include tails::profile::jenkins::isoworker::reverse_proxy_exception

# For the build_IUKs Jenkins job
  include tails::profile::jenkins::iuk_builder

  include tails::profile::jenkins::agent

  User <| title == jenkins |> { groups +> 'libvirt' }
  User <| title == jenkins |> { groups +> 'libvirt-qemu' }
  User <| title == jenkins |> { groups +> 'kvm' }
  User <| title == jenkins |> { require +> Package['libvirt-daemon-system'] }
  realize User['jenkins']

  file { '/usr/local/bin/compare_artifacts':
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/compare_artifacts',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/collect_build_environment':
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/collect_build_environment',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/cleanup_build_jobs_leftovers':
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/cleanup_build_jobs_leftovers',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/sign_artifacts':
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/sign_artifacts',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/decide_if_reproduce':
    source  => 'puppet:///modules/tails/jenkins/slaves/isobuilders/decide_if_reproduce',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => [
      File['/etc/jenkins/gitlab_apikey'],
      Package['python3-requests'],
    ],
  }

  ### Email notifications

  ensure_packages([
      'python3-requests',
      'python3-sh',
  ])

  file { '/usr/local/bin/output_ISO_builds_and_tests_notifications':
    source  => 'puppet:///modules/tails/jenkins/slaves/isobuilders/output_ISO_builds_and_tests_notifications',
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    require => [
      File['/etc/jenkins/gitlab_apikey'],
      Package[
        'python3-requests',
        'python3-sh'
      ],
    ],
  }

  file { '/etc/jenkins/gitlab_apikey':
    ensure  => file,
    content => "${gitlab_apikey}\n",
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
  }
}
