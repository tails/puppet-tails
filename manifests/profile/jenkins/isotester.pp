# @summary
#   Manage a jenkins isotester.
#
# @example
#   include tails::profile::isotester
#
# @param pidgin_config
#   string with pidgin configuration
#
# @param ensure
#   to slave or not to slave
#
# @param config_basedir
#   configuration basedir
#
# @param manage_temp_dir_mount
#   whether to manage mounting of the temp dir (obsolete)
#
# @param root_ssh_pubkey_name
#   name of root ssh pubkey
#
# @param jenkins_master_ssh_pubkey_name
#   name of jenkins master pubkey
#
# @param temp_dir
#   temp dir
#
# @param temp_dir_backing_device
#   backing device for temp dir (obsolete)
#
# @param temp_dir_fs_type
#   fs type for temp dir (obsolete)
#
# @param temp_dir_mount_options
#   mount options for temp dir (obsolete)
#
# @param tester_gitlab_apikey
#   gitlab apikey for the isotester user
#
# @param gitlab_url
#   the url of our gitlab instance
#
# @param test_suite_shared_secrets_repo
#   git repository with shared secrets
#
# @param manage_email_server
#   whether to manage a mailserver
#
# @param email_password
#   the password for the mailserver
#
class tails::profile::jenkins::isotester (
  String $pidgin_config,
  Enum['present', 'absent'] $ensure         = 'present',
  Stdlib::Absolutepath $config_basedir      = '/etc/TailsToaster',
  Boolean $manage_temp_dir_mount            = false,
  String $root_ssh_pubkey_name              = "root@${facts['networking']['fqdn']}",
  String $jenkins_master_ssh_pubkey_name    = "jenkins@jenkins-master.${facts['networking']['domain']}",
  Stdlib::Absolutepath $temp_dir            = '/tmp/TailsToaster',
  Optional[String] $temp_dir_backing_device = undef,
  String $temp_dir_fs_type                  = 'ext4',
  String $temp_dir_mount_options            = 'relatime,acl',
  String $gitlab_url                        = 'https://gitlab.tails.boum.org',
  String $tester_gitlab_apikey              = 'secret',
  String $test_suite_shared_secrets_repo    = 'git@gitlab-ssh.tails.boum.org:tails/test-suite-shared-secrets.git',
  Boolean $manage_email_server              = true,
  String $email_password                    = 'secret',
) {
# allow unauthenticated access to the Jenkins web interface
  include tails::profile::jenkins::isoworker::reverse_proxy_exception

  realize User['jenkins']

  rbac::user { 'foundations-team-members': }
  rbac::group { 'foundations-team-members-to-sudo':
    group => 'sudo',
    role  => 'foundations-team-members',
  }
  rbac::group { 'foundations-team-members-to-libvirt':
    group              => 'libvirt',
    role               => 'foundations-team-members',
    additional_members => ['jenkins'],
    require            => User['jenkins'],
  }
  rbac::group { 'foundations-team-members-to-libvirt-qemu':
    group              => 'libvirt-qemu',
    role               => 'foundations-team-members',
    additional_members => ['libvirt-qemu', 'jenkins'],
    require            => User['jenkins'],
  }

  include tails::profile::jenkins::agent

  class { 'tails::profile::jenkins::tester':
    manage_temp_dir_mount   => $manage_temp_dir_mount,
    temp_dir_backing_device => $temp_dir_backing_device,
    temp_dir                => $temp_dir,
    temp_dir_fs_type        => $temp_dir_fs_type,
    temp_dir_mount_options  => $temp_dir_mount_options,
  }

  if $manage_email_server {
    include tails::profile::jenkins::dovecot
  }

  file {
    $config_basedir:
      ensure => directory,
      mode   => '0755',
      owner  => 'root',
      group  => 'root';
    "${config_basedir}/local.d":
      ensure => directory,
      mode   => '0755',
      owner  => 'jenkins',
      group  => 'jenkins';
    "${config_basedir}/local.d/pidgin.yml":
      owner   => 'jenkins',
      group   => 'jenkins',
      mode    => '0644',
      content => $pidgin_config;
  }

  sshkeys::set_client_key_pair { $root_ssh_pubkey_name:
    keyname => $root_ssh_pubkey_name,
    user    => 'root',
    home    => '/root',
  }

  vcsrepo { "${config_basedir}/common.d":
    ensure   => latest,
    owner    => jenkins,
    group    => jenkins,
    user     => root,
    provider => git,
    source   => $test_suite_shared_secrets_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$root_ssh_pubkey_name],
      Package['git'],
      File[$config_basedir],
    ],
  }

  # XXX workaround for sysadmin#17988, remove once node is upgraded to bookworm
  tails::profile::git::safe { "${config_basedir}/common.d": }

  file { '/usr/local/bin/wrap_test_suite':
    ensure  => $ensure,
    content => epp('tails/jenkins/wrap_test_suite.epp', { email_password => $email_password }),
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }

  file { '/usr/local/bin/talkback.py':
    ensure  => $ensure,
    content => epp('tails/jenkins/talkback.py.epp', {
        jenkins_user  => $tails::profile::jenkins::agent::api_user,
        jenkins_pass  => $tails::profile::jenkins::agent::api_token,
        jenkins_url   => $tails::profile::jenkins::agent::master_url,
        gitlab_url    => $gitlab_url,
        gitlab_apikey => $tester_gitlab_apikey,
    }),
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }

  file { '/usr/local/bin/post_test_cleanup':
    ensure => $ensure,
    source => 'puppet:///modules/tails/jenkins/slaves/isotesters/post_test_cleanup',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  # Wait for a shorter duration than the default 300s before forcibly
  # shutting down a leftover TailsToaster guest on poweroff/reboot.
  # This makes the tester VM available again faster after
  # a test_Tails_ISO_* job has been canceled.
  augeas { 'libvirt-guests-SHUTDOWN_TIMEOUT':
    context => '/files/etc/default/libvirt-guests',
    changes => 'set SHUTDOWN_TIMEOUT 30',
    require => Class['tails::profile::jenkins::tester'],
  }

  # Prevent some Recommends pulled by GTK3 to be installed.
  # Some of them imply running daemons we don't need, which slows down the boot
  # ... and we're going to reboot these VMs quite often.
  package { ['colord', 'libsane', 'sane-utils']:
    ensure => absent,
  }

  ensure_packages([
      'python3-yaml',
      'python3-requests',
      'python3-psycopg2',
      'python3-distro-info',
      'python3-debian',
  ])

  sshkeys::set_authorized_keys { 'jenkins_master_to_iso_tester':
    keyname => $jenkins_master_ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
  }
}
