# @summary
#   Manage services needed to provide a local email server that can be used
#   by Thunderbird in our automated test suite.
#
# @param hostname
#   the mailserver's hostname
#
# @param email_password
#   the password for the test mail user
#
# @param email_password_salt
#   seasoning for said password
#
# @param email_user
#   the username of the test mail user
#
# @param ssl_cert
#   path to the ssl certificate
#
# @param ssl_key
#   path to the ssl key
#
# @param include_files
#   array of paths to files to include in Nginx config
#
class tails::profile::jenkins::dovecot (
  String $hostname            = $facts['networking']['fqdn'],
  String $email_password      = 'secret',
  String $email_password_salt = $hostname,
  String $email_user          = "test@${hostname}",
  Stdlib::Absolutepath $ssl_cert = '/etc/ssl/certs/ssl-cert-snakeoil.pem',
  Stdlib::Absolutepath $ssl_key  = '/etc/ssl/private/ssl-cert-snakeoil.key',
  Array $include_files           = ['snippets/snakeoil.conf'],
) {
  ### Common resources

  $packages = [
    'dovecot-core',
    'dovecot-imapd',
    'dovecot-lmtpd',
    'dovecot-pop3d',
    'ssl-cert',
    'swaks',
  ]

  ensure_packages($packages)

  $hashed_email_password = pw_hash(
    $email_password, 'SHA-512', regsubst($email_password_salt, /-/, '', 'G')
  )

  ### Constants

  $dovecot_ssl_cert      = '/etc/dovecot/private/dovecot.pem'
  $dovecot_ssl_key       = '/etc/dovecot/private/dovecot.key'

  ### Dovecot

  service { 'dovecot':
    ensure  => running,
    enable  => true,
    require => [
      File['/etc/dovecot/conf.d/99-tails-tester-support.conf'],
      File_line['dovecot_disable_auth-system'],
      Package['dovecot-imapd'],
      Package['dovecot-pop3d'],
      Package['ssl-cert'],
      User['vmail'],
    ],
  }

  user { 'vmail':
    uid        => '5000',
    home       => '/var/vmail',
    managehome => true,
  }

  group { 'vmail':
    gid        => '5000',
  }

  file { $dovecot_ssl_cert:
    ensure  => link,
    target  => $ssl_cert,
    require => Package['ssl-cert'],
  }

  file { $dovecot_ssl_key:
    ensure  => link,
    target  => $ssl_key,
    require => Package['ssl-cert'],
  }

  file { '/etc/dovecot/conf.d/99-tails-tester-support.conf':
    content => template('tails/tester/support/email/dovecot/tails-tester-support.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => [
      File[$dovecot_ssl_cert],
      File[$dovecot_ssl_key],
      Package['dovecot-core'],
    ],
    notify  => Service['dovecot'],
  }

  file { '/etc/dovecot/passwd':
    content => "${email_user}:${hashed_email_password}::::::\n",
    owner   => 'root',
    group   => 'dovecot',
    mode    => '0640',
  }

  file_line { 'dovecot_disable_auth-system':
    path   => '/etc/dovecot/conf.d/10-auth.conf',
    line   => '#!include auth-system.conf.ext',
    match  => '#?!include auth\-system\.conf\.ext',
    notify => Service['dovecot'],
  }

  ### nginx

  tails::profile::nginx::server { $facts['networking']['fqdn']:
    server_name    => ['_'],
    listen_port    => 443,
    listen_options => 'http2 default_server',
    www_root       => '/var/www/html',
    ssl            => true,
    ssl_cert       => $ssl_cert,
    ssl_key        => $ssl_key,
    ssl_redirect   => false,
    letsencrypt    => false,
    include_files  => $include_files,
  }

  file { [
      '/var/www/html/.well-known',
      '/var/www/html/.well-known/autoconfig',
      '/var/www/html/.well-known/autoconfig/mail',
    ]:
      ensure  => directory,
      owner   => 'root',
      group   => 'www-data',
      mode    => '0750',
      require => Package['nginx'],
  }

  file { '/var/www/html/.well-known/autoconfig/mail/config-v1.1.xml':
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    content => epp('tails/tester/support/email/nginx/autoconfig.xml.epp', { hostname => $hostname }),
  }
}
