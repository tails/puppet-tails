# @summary
#   Manages the Tails Jenkins master.
#   Installs extra packages for nice features as well as Jenkins plugins.
#   If $automatic_iso_jobs_generator is 'present', $jenkins_jobs_repo must be
#   set to the URL of a jenkins-jobs git repo where the 'jenkins@jenkins-master'
#   SshKey has write access. It depends on $deploy_on_git_push being set to true
#   for the pushed configuration to be applied automatically.
#   For this to happen, hooks managed in tails::profile::gitolite::hooks::jenkins_jobs
#   also need to be installed in the jenkins-jobs repo.
#
# @param jenkins_jobs_repo
#   location of the jenkins-jobs repository
#
# @param api_token
#   the api token
#
# @param version
#   which version to run
#
# @param tails_repo
#   location of the tails repository
#
# @param deploy_jobs_on_git_push
#   whether to deploy jobs on git push
#
# @param automatic_iso_jobs_generator
#   whether to generate iso jobs automatically
#
# @param active_branches_max_age_in_days
#   max age of active branches
#
# @param gitolite_pubkey_name
#   name of the gitolite public key
#
# @param api_user
#   the api username
#
class tails::profile::jenkins::master (
  String $jenkins_jobs_repo,
  String $api_token,
  String $version                                         = '2.479.2',
  String $tails_repo                                      = 'https://gitlab.tails.boum.org/tails/tails.git',
  Boolean $deploy_jobs_on_git_push                        = true,
  Enum['present', 'absent'] $automatic_iso_jobs_generator = 'present',
  Integer $active_branches_max_age_in_days                = 49,
  String $gitolite_pubkey_name                            = 'gitolite@puppet-git',
  String $api_user                                        = 'Tails',
) {
  ### Sanity checks

  if $facts['os']['name'] != 'Debian' or versioncmp($facts['os']['distro']['release']['major'], '11') < 0 {
    fail('This module only supports Debian 11 or newer.')
  }

  ### Variables

  $ssh_pubkey_name = "jenkins@jenkins-master.${facts['networking']['domain']}"

  $base_packages = [
    'git',
    'jenkins-job-builder',
    'libmockito-java',
    'python3-jenkins-job-builder',
  ]

  ### Access control

  rbac::user { 'foundations-team-members': }

  ### Resources

  apt::pin { 'jenkins':
    packages => 'jenkins',
    version  => $version,
    priority => 991,
  }

  class { 'jenkins':
    repo            => true,
    lts             => true,
    install_java    => false,
    version         => $version,
    default_plugins => [],
    require         => [
      Package[$base_packages],
#      Apt::Conf['proxy_jenkins_repo'],
      Apt::Pin['jenkins'],
    ],
  }

  # Provides the jar command, which is used by Jenkins::Cli/Exec[jenkins-cli]
  package { 'default-jdk-headless':
    ensure => installed,
  }

#  # apt-cacher-ng does not support HTTPS repositories
#  apt::conf { 'proxy_jenkins_repo':
#    content  => 'Acquire::HTTP::Proxy::pkg.jenkins.io "DIRECT";',
#  }
#  apt::conf { 'proxy_prodjenkinsreleases_repo':
#    content  => 'Acquire::HTTP::Proxy::prodjenkinsreleases.blob.core.windows.net "DIRECT";',
#  }

  apt::pin { 'jenkins-job-builder':
    packages => [
      'jenkins-job-builder',
      'python3-jenkins-job-builder',
    ],
    codename => 'bookworm',
    priority => 991,
  }

  ensure_packages($base_packages)

  # XXX Patch to fix sysadmin#17962, remove after the following is fixed:
  #     https://storyboard.openstack.org/#!/story/2009943
  file { '/usr/local/src/jenkins-job-builder.patch':
    ensure => file,
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/tails/jenkins/master/jenkins-job-builder.patch',
  }

  exec { 'patch jenkins-job-builder to fix sysadmin#17962':
    command => '/usr/bin/patch /usr/lib/python3/dist-packages/jenkins_jobs/modules/publishers.py /usr/local/src/jenkins-job-builder.patch',
    unless  => '/usr/bin/patch --dry-run -R -s -f /usr/lib/python3/dist-packages/jenkins_jobs/modules/publishers.py /usr/local/src/jenkins-job-builder.patch', # lint:ignore:140chars
    require => [
      Package['jenkins-job-builder'],
      File['/usr/local/src/jenkins-job-builder.patch'],
    ],
  }

  include tails::profile::jenkins::plugins

  ## Uncomment this (or similar) once all this is moved to a proper class,
  ## that inherits jenkins::service and can thus append to its dependencies.
  # Service['jenkins'] {
  #   require +> File_line['jenkins_HTTP_HOST'],
  # }

  file { '/etc/jenkins':
    ensure  => directory,
    mode    => '0750',
    owner   => 'root',
    group   => 'jenkins',
    require => User['jenkins'],
  }

  # Used by the reboot_node macro
  file { '/etc/jenkins/jenkins_apikey':
    ensure  => file,
    content => $api_token,
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
  }

  file { '/etc/jenkins_jobs/jenkins_jobs.ini':
    owner   => root,
    group   => jenkins,
    mode    => '0640',
    content => template('tails/jenkins/orchestrator/jenkins_jobs.ini.erb'),
    require => [
      Package['jenkins'],
      Package['jenkins-job-builder'],
    ],
  }

  file { '/etc/jenkins_jobs':
    ensure => directory,
    owner  => root,
    group  => jenkins,
    mode   => '0770',
  }

  vcsrepo { '/etc/jenkins_jobs/jobs':
    ensure   => present,
    owner    => jenkins,
    group    => jenkins,
    user     => jenkins,
    provider => git,
    source   => $jenkins_jobs_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
      Package['git'],
      File['/etc/jenkins_jobs'],
    ],
  }

  # XXX workaround for sysadmin#17988, remove once node is upgraded to bookworm
  tails::profile::git::safe { '/etc/jenkins_jobs/jobs': }

  if $deploy_jobs_on_git_push {
    file { '/var/tmp/jenkins_jobs_test':
      ensure => directory,
      owner  => jenkins,
      group  => jenkins,
      mode   => '0700',
    }

    file { '/usr/local/sbin/deploy_jenkins_jobs':
      ensure  => file,
      source  => 'puppet:///modules/tails/jenkins/master/deploy_jenkins_jobs',
      owner   => root,
      group   => root,
      mode    => '0755',
      require => [
        File['/var/tmp/jenkins_jobs_test'],
        Ssh_authorized_key[$gitolite_pubkey_name],
      ],
    }

    sshkeys::set_authorized_keys { $gitolite_pubkey_name:
      user    => jenkins,
      home    => '/var/lib/jenkins',
      require => Package['jenkins'],
    }
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts':
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts',
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts_wrapper':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts_wrapper',
    require => File['/usr/local/bin/clean_old_jenkins_artifacts'],
  }

  cron { 'clean_old_jenkins_artifacts':
    command => '/usr/local/bin/clean_old_jenkins_artifacts_wrapper /var/lib/jenkins',
    user    => 'jenkins',
    hour    => '23',
    minute  => '50',
    require => [File['/usr/local/bin/clean_old_jenkins_artifacts_wrapper'], Package['jenkins']],
  }

  file { '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/deduplicate_reproducible_build_jobs_upstream_ISOs',
    require => Package['jenkins'],
  }

  cron { 'deduplicate_reproducible_build_jobs_upstream_ISOs':
    command => '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/6',
    require => File['/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs'],
  }

  file { '/usr/local/bin/manage_latest_iso_symlinks':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/manage_latest_iso_symlinks',
    require => Package['jenkins'],
  }

  cron { 'manage_latest_iso_symlinks':
    command => '/usr/local/bin/manage_latest_iso_symlinks /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/5',
    require => File['/usr/local/bin/manage_latest_iso_symlinks'],
  }

  class { 'tails::profile::jenkins::iso_jobs_generator':
    ensure            => $automatic_iso_jobs_generator,
    tails_repo        => $tails_repo,
    jenkins_jobs_repo => $jenkins_jobs_repo,
    active_days       => $active_branches_max_age_in_days,
    require           => [
      Class['jenkins'],
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
    ],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => Class['jenkins'],
  }

  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

  mailalias { 'jenkins':
    recipient => 'root',
  }
}
