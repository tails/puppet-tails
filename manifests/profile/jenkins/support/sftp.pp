# @summary
#   Manage a SFTP user for the automated test suite
#   This should be refactored
#
# @param password
#   the test user's password
#
# @param ssh_pub_key
#   an ssh public key to authorize
#
# @param ssh_pub_key_type
#   said ssh key's type
#
# @param user
#   the username of the test user
#
# @param home
#   the test user's homedir
#
class tails::profile::jenkins::support::sftp (
  String $password,
  String $ssh_pub_key,
  String $ssh_pub_key_type,
  String $user               = 'autotest-sftp',
  Stdlib::Absolutepath $home = '/var/lib/autotest-sftp',
) {
  ensure_packages(['openssh-sftp-server'])

  user { $user:
    ensure     => present,
    shell      => '/usr/lib/openssh/sftp-server',
    system     => true,
    home       => $home,
    managehome => true,
    password   => $password,
    require    => Package['openssh-sftp-server'],
  }
  file { [$home, "${home}/.ssh"]:
    ensure  => directory,
    owner   => $user,
    group   => $user,
    mode    => '0700',
    require => User[$user],
  }
  ssh_authorized_key { $user:
    ensure  => present,
    user    => $user,
    type    => $ssh_pub_key_type,
    key     => $ssh_pub_key,
    require => File["${home}/.ssh"],
  }
  sshkeys::set_authorized_keys { "monitoring_${user}":
    keyname => "monitoring@${user}",
    user    => $user,
    home    => $home,
    require => User[$user],
  }
}
