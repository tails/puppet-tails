# @summary
#   Manage Jenkins plugins
#
class tails::profile::jenkins::plugins () {
  jenkins::plugin { 'antisamy-markup-formatter':
    version       => '162.v0e6ec0fcfcf6',
    digest_string => '3d4144a78b14ccc4a8f370ccea82c93bd56fadd900b2db4ebf7f77ce2979efd6',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'apache-httpcomponents-client-4-api':
    version       => '4.5.14-208.v438351942757',
    digest_string => '9ed0ccda20a0ea11e2ba5be299f03b30692dd5a2f9fdc7853714507fda8acd0f',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'asm-api':
    version       => '9.7.1-97.v4cc844130d97',
    digest_string => '7f26c33883ea995b90a6e5c0f60cd1b4af0f863380ea42f7da4518960e04c393',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'bootstrap5-api':
    version       => '5.3.3-1',
    digest_string => 'e0d0f7c92dae2f7977c28ceb6a5b2562b7012d1704888bff3bc176abda0cb269',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'commons-text-api',
      'font-awesome-api',
    ],
  }

  jenkins::plugin { 'bouncycastle-api':
    version       => '2.30.1.78.1-248.ve27176eb_46cb_',
    digest_string => '052c437528458806a9464d59a577142dc5c8848e306b19cba865e027d1ad2031',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'build-symlink':
    version       => '1.1',
    digest_string => 'dc5e517743d872ceefb86199d18c68bef4885c02c275249308a7b37ded8d4504',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'build-timeout':
    version       => '1.33',
    digest_string => '6cee5f6899438de6d11fe08617d82b9517095fae67434f3fccbaba7c913e8883',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'json-path-api',
      'token-macro',
    ],
  }

  jenkins::plugin { 'caffeine-api':
    version       => '3.1.8-133.v17b_1ff2e0599',
    digest_string => 'a6c614655bc507345bf16b5c4615bb09b1a20f934c9bf0b15c02ccea4a5c0400',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'checks-api':
    version       => '2.2.1',
    digest_string => '2dc1e51c86e4b16c17e66a45d94ced9d0ee4a0b247df77a4a796bd3c93471b98',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'commons-text-api',
      'display-url-api',
      'plugin-util-api',
      'workflow-step-api',
      'workflow-support',
    ],
  }

  jenkins::plugin { 'cluster-stats':
    version       => '0.4.6',
    digest_string => 'd8f725c71d845d43939ca47ca9b13136c5f1a82dc9064adcc4353ca7a771bdd2',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bouncycastle-api',
      'command-launcher',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'jaxb',
      'jdk-tool',
      'junit',
      'matrix-project',
      'sshd',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'command-launcher':
    version       => '116.vd85919c54a_d6',
    digest_string => '1c615c184a70e3e2189dbaff0bf66f3cb027fa1e64396fc280e512071fba412b',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'script-security',
    ],
  }

  jenkins::plugin { 'commons-lang3-api':
    version       => '3.17.0-84.vb_b_938040b_078',
    digest_string => '90b15521b21ad1462b18a6f8894ff1a2c1080c5d398ca8bb928c062c992c3fc4',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'commons-text-api':
    version       => '1.12.0-129.v99a_50df237f7',
    digest_string => '2abe8a58c9a6a201121eda2494c2a5c7b5758536e61309b333ed72bdfba857c9',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
    ],
  }

  jenkins::plugin { 'conditional-buildstep':
    version       => '1.4.3',
    digest_string => 'd2ce40b86abc42372085ace0a6bb3785d14ae27f0824709dfc2a2b3891a9e8a8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'run-condition',
      'token-macro',
    ],
  }

  jenkins::plugin { 'copyartifact':
    version       => '757.v05365583a_455',
    digest_string => 'bbc5dc31b1f34b1126841ee26cf89beac06480945b8333ac68d7e06caba48f1c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'structs',
    ],
  }

  jenkins::plugin { 'credentials':
    version       => '1405.vb_cda_74a_f8974',
    digest_string => 'faaf55e8735973b835b2d45385c8ed85980f1d139832a73432652a714d16c7e1',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bouncycastle-api',
      'structs',
    ],
  }

  jenkins::plugin { 'credentials-binding':
    version       => '687.v619cb_15e923f',
    digest_string => '3a589c067bfc21e3792f2f60efa63a5a46ceedcb13af2b1ad4b1f631e4f37d0d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'plain-credentials',
      'ssh-credentials',
      'structs',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'cucumber-reports':
    version       => '5.8.5',
    digest_string => 'be034ff1ddcb214584a6d461cda495bdef409a551dd6e1e59e72e68ec4afe1ef',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'display-url-api':
    version       => '2.209.v582ed814ff2f',
    digest_string => '413075f95bb93769708a5d4d660ca454f10005f10af26f5213f788e9750e6825',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'echarts-api':
    version       => '5.5.1-4',
    digest_string => 'cb81fb709565e7a2484da17cbda239f2a1a516b6edc338f72deee35086175fe4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bootstrap5-api',
      'commons-lang3-api',
      'commons-text-api',
      'font-awesome-api',
      'jackson2-api',
      'jquery3-api',
      'plugin-util-api',
    ],
  }

  jenkins::plugin { 'eddsa-api':
    version       => '0.3.0-4.v84c6f0f4969e',
    digest_string => 'ab56adb71f31e5627ac6751c393e8692916c1b82bf6b5f8399f9a88cfd8cb944',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'email-ext':
    version       => '1866.v14fa_6d201654',
    digest_string => '9a79480e8bad653477927f72d651fec34753d3fff05409c6f201c5f8b73c3774',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'jackson2-api',
      'jakarta-mail-api',
      'junit',
      'mailer',
      'matrix-project',
      'scm-api',
      'script-security',
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'envinject':
    version       => '2.919.v009a_a_1067cd0',
    digest_string => '16e9dd2a6644581e48da0819842815da738e329ebea65dcb30fc5acc9c205eb0',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'envinject-api',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'envinject-api':
    version       => '1.199.v3ce31253ed13',
    digest_string => '463efd070b29d087d2db823720f6b4fab13f161d40dceeadabfbcf64e4a44c60',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'flatpickr-api':
    version       => '4.6.13-5.v534d8025a_a_59',
    digest_string => '474e4e6e885d9dfe2c4f7cfc5e0602c832e0fdc59077ba041dcf30333b2a8bdf',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'font-awesome-api':
    version       => '6.6.0-2',
    digest_string => '6c0d5b6688372403f98c2ad63eb3269d3a04b2d4df511a6e38b20c1ca253b5b0',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'commons-text-api',
      'plugin-util-api',
    ],
  }

  jenkins::plugin { 'git':
    version       => '5.6.0',
    digest_string => '345395d3abf4ca7c5bcbc99251ba5b6aede9756198104bf72f393346bd759bd0',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'credentials-binding',
      'git-client',
      'mailer',
      'scm-api',
      'script-security',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'git-client':
    version       => '6.1.0',
    digest_string => 'e855a000bede69c4c574bc57d48b1b2917c15a0ff219ae61b525586a2ac5f62a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'credentials',
      'gson-api',
      'mina-sshd-api-common',
      'mina-sshd-api-core',
      'script-security',
      'ssh-credentials',
      'structs',
    ],
  }

  jenkins::plugin { 'global-build-stats':
    version       => '316.vf8870f424d78',
    digest_string => 'c7f0cce5a39be92bc7b855d8021809c755ad2116a4574cf385436e024702807b',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'flatpickr-api',
      'ionicons-api',
    ],
  }

  jenkins::plugin { 'gson-api':
    version       => '2.11.0-85.v1f4e87273c33',
    digest_string => '74a3059af88301a7527458e57ab7b7a8ca91711cc02fe9c34fd41ca3133f5666',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'instance-identity':
    version       => '201.vd2a_b_5a_468a_a_6',
    digest_string => 'c43dc01e201fd37a38a6307dacc84ace60e1608f96623691e1dbe1fdc6d8a911',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bouncycastle-api',
    ],
  }

  jenkins::plugin { 'ionicons-api':
    version       => '74.v93d5eb_813d5f',
    digest_string => '681a9cc3083a089d52ef398206bfc521daabf3c682ef0a57be73e0feddc62e8f',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'jackson2-api':
    version       => '2.17.0-379.v02de8ec9f64c',
    digest_string => '5e2d919724da0a47cd01bdb9f614c8fc90862c09ce506d9b2ca340252bad225e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'javax-activation-api',
      'jaxb',
      'json-api',
      'snakeyaml-api',
    ],
  }

  jenkins::plugin { 'jakarta-activation-api':
    version       => '2.1.3-1',
    digest_string => 'ddc3df5d8c39a2a208661d69277120b1113373b04d06e2250615be2a65404b83',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'jakarta-mail-api':
    version       => '2.1.3-1',
    digest_string => '851ab22ff0647f4d82baab4e526c6d0ddb3e64ad4969c516116b374ef778e539',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'jakarta-activation-api',
    ],
  }

  jenkins::plugin { 'javax-activation-api':
    version       => '1.2.0-7',
    digest_string => 'c60ab7240dded219e2cd3002b30579dd993832c5d4683dca710f0f426776b063',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'javax-mail-api':
    version       => '1.6.2-10',
    digest_string => '61a90ff2f9fe244e581221659790e28c547b7ca742b97af0ab65517b63fadd75',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'javax-activation-api',
    ],
  }

  jenkins::plugin { 'jaxb':
    version       => '2.3.9-1',
    digest_string => '8c9f7f98d996ade98b7a5dd0cd9d0aba661acea1b99a33f75778bacf39a64659',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'javax-activation-api',
    ],
  }

  jenkins::plugin { 'jdk-tool':
    version       => '80.v8a_dee33ed6f0',
    digest_string => '6e92d1a6b80977da6cc79121daab1432eafa2c0eca7b7b3b2807c91c69c5a6c1',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
    ],
  }

  jenkins::plugin { 'jquery3-api':
    version       => '3.7.1-2',
    digest_string => '322d01e14a368e3131ff388dbc6fe062abaa6cb0a2bb761bc46516f1f7ca1066',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'commons-text-api',
    ],
  }

  jenkins::plugin { 'json-api':
    version       => '20240303-101.v7a_8666713110',
    digest_string => '4b10522d1eb618a1a24694a21f2b2dce0e2e9c8f7d66a6837a74a554163d97de',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'json-path-api':
    version       => '2.9.0-118.v7f23ed82a_8b_8',
    digest_string => 'cf71719ca21a060c4e96daea900d8873dd8efa946f9bf0c15e1bc40447a6738e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'asm-api',
    ],
  }

  jenkins::plugin { 'junit':
    version       => '1312.v1a_235a_b_94a_31',
    digest_string => '923ec47c5639e9f22f0fa428488d887705a4bdfd16b2344e40047ed26f7bb071',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bootstrap5-api',
      'checks-api',
      'display-url-api',
      'echarts-api',
      'ionicons-api',
      'jackson2-api',
      'plugin-util-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'mailer':
    version       => '489.vd4b_25144138f',
    digest_string => '1d836fe30c6515f3918f951d12a4f4aad1d9108eeaa059ff8beaae5e44527da0',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'display-url-api',
      'instance-identity',
      'jakarta-mail-api',
    ],
  }

  jenkins::plugin { 'matrix-project':
    version       => '840.v812f627cb_578',
    digest_string => '06e009b903e74d6142c51207b45b52a94a728fa21460e7cb0bc001eaa8dd51c2',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'ionicons-api',
      'junit',
      'script-security',
    ],
  }

  jenkins::plugin { 'mina-sshd-api-common':
    version       => '2.14.0-136.v4d2b_0853615e',
    digest_string => '1ad2b690ae0e544327127b8b9ec88586c7fddc082668088dd3b4554ade091ed5',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'mina-sshd-api-core':
    version       => '2.14.0-136.v4d2b_0853615e',
    digest_string => '41dbeeaacf38c2fb0a33967ec8452fbbbb3d7ee30f13d6b8a56a3daae16a098f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'mina-sshd-api-common',
    ],
  }

  jenkins::plugin { 'parameterized-trigger':
    version       => '806.vf6fff3e28c3e',
    digest_string => 'a771a5cc77e16270938af28e311970eec132cb0666b6b9af37422ae606c5aaf3',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'plain-credentials':
    version       => '183.va_de8f1dd5a_2b_',
    digest_string => '9422eaa765e6591e3c845bfec9c105f5600a058951a2940aec5be0ed76ea813a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
    ],
  }

  jenkins::plugin { 'plugin-util-api':
    version       => '5.1.0',
    digest_string => 'f3663009736c677afea03b566555459f8ef15164863743671e31f4f675474ceb',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'commons-text-api',
      'workflow-api',
      'workflow-step-api',
      'workflow-support',
    ],
  }

  jenkins::plugin { 'postbuildscript':
    version       => '3.4.1-695.vf6b_0b_8053979',
    digest_string => '2595692b5a9ef7fa27e7d8d348105a1b3fcd42b140f7dae0964f83e9c3f0c234',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
    ],
  }

  jenkins::plugin { 'PrioritySorter':
    version       => '5.2.0',
    digest_string => 'add57823615552ce57861e51c1ccdb0e8228070dd9f5a2ddeeb5631ae87a5acb',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'ionicons-api',
    ],
  }

  jenkins::plugin { 'resource-disposer':
    version       => '0.25',
    digest_string => '2794c871cc479ed37fd04a85fe8d4f4aa7fee1c7929c447c8b8fa239548e3bf2',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'run-condition':
    version       => '1.7',
    digest_string => 'd8601f47c021f8c6b8275735f5c023fec57b65189028e21abac91d42add0be42',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'token-macro',
    ],
  }

  jenkins::plugin { 'scm-api':
    version       => '698.v8e3b_c788f0a_6',
    digest_string => '49386b769c66f496ae7ddd5e8fe6140ff33a7ae40f5ff732d539a4e8a1258876',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'asm-api',
      'structs',
    ],
  }

  jenkins::plugin { 'scoring-load-balancer':
    version       => '139.v1f21a_7494a_a_2',
    digest_string => '77349411d29ea8c6f830da1383b3c1a38d0e395a22981e6cfeebf12bcc7c632e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
    ],
  }

  jenkins::plugin { 'script-security':
    version       => '1369.v9b_98a_4e95b_2d',
    digest_string => 'bf771fd9b14ff6a6c76b572832b8a7fa5824eb2a6e87392f6eb640224f5485e4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'caffeine-api',
    ],
  }

  jenkins::plugin { 'snakeyaml-api':
    version       => '2.3-123.v13484c65210a_',
    digest_string => 'd5b81a0d0cfd411de76daefd91b1dcdc0a573bc5be9ff6ed1f47411fadae13fe',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'ssh-credentials':
    version       => '349.vb_8b_6b_9709f5b_',
    digest_string => '9794ef186ef33735522b1915cad95f5d11196c9bf9ca4d0e46e0b1ef0464a3e7',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bouncycastle-api',
      'credentials',
      'variant',
    ],
  }

  jenkins::plugin { 'sshd':
    version       => '3.330.vc866a_8389b_58',
    digest_string => 'de916ad50d32721e9162363f86b9b12a013c3cee5e256401d0c116093bfa2183',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'eddsa-api',
      'instance-identity',
      'mina-sshd-api-common',
      'mina-sshd-api-core',
    ],
  }

  jenkins::plugin { 'structs':
    version       => '338.v848422169819',
    digest_string => '7cae811a39788f58a954774631f0a279d4bf5e32672837268a85382267a8af66',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'timestamper':
    version       => '1.28',
    digest_string => 'ae70f001f26fef032d0bfee7104443712b2d82e87ce8a7517cd0f8e721ac57ee',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'antisamy-markup-formatter',
      'commons-lang3-api',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'token-macro':
    version       => '400.v35420b_922dcb_',
    digest_string => '822726088a5893f248b7bba1aea92ef6df1534b64acc0a23e2fc976db33439c8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'json-path-api',
      'structs',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'trilead-api':
    version       => '2.147.vb_73cc728a_32e',
    digest_string => '25b8858b595b75db10248f8a2dfdbedf049d4356f9b1c096573f845c9a962e4d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'eddsa-api',
      'gson-api',
    ],
  }

  jenkins::plugin { 'variant':
    version       => '60.v7290fc0eb_b_cd',
    digest_string => 'acbf1aebb9607efe0518b33c9dde9bd50c03d6a1a0fa62255865f3cf941fa458',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'workflow-api':
    version       => '1336.vee415d95c521',
    digest_string => '2c63792ec7adad277d070afd65c9550179de7c3690a9ec39de5db9969d60361e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-scm-step':
    version       => '427.v4ca_6512e7df1',
    digest_string => '4a06c4667e1bc437e89107abd9a316adaf51fca4fd504d12a525194777d34ad8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-step-api':
    version       => '678.v3ee58b_469476',
    digest_string => '40e450133a095bb9ccf058b6bf2cfa3f0938a9889f5e494d61dfb0adea518b3d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
    ],
  }

  jenkins::plugin { 'workflow-support':
    version       => '936.v9fa_77211ca_e1',
    digest_string => '2dfc7f9c984e2bfcd2753b26afbc3f9b23c72a0dd8b9c254507cd777a21472d5',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'caffeine-api',
      'scm-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'ws-cleanup':
    version       => '0.48',
    digest_string => 'a0f0b871a7c2aa894d529fa222ebef8e591c7f418ff65f164f3ceb0b876dd983',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
      'resource-disposer',
      'structs',
    ],
  }
}
