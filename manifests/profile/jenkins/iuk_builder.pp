# @summary
#   profile to make a system ready to build Tails IUKs.
#   Design doc: https://tails.net/contribute/design/incremental_upgrades/
#
# @param temp_dir
#   the path to the temp dir
#
class tails::profile::jenkins::iuk_builder (
  String $temp_dir = '/var/tmp/tails-create-iuks',
) {
  $iuk_builder_packages = [
    'attr',
    'libarchive-tar-wrapper-perl',
    'libarchive-tools',
    'libcarp-assert-more-perl',
    'libcarp-assert-perl',
    'libclass-xsaccessor-perl',
    'libdevice-cdio-perl',
    'libdpkg-perl',
    'libfile-which-perl',
    'libfilesys-df-perl',
    'libfunction-parameters-perl',
    'libgnupg-interface-perl',
    'libipc-system-simple-perl',
    'libmoo-perl',
    'libmoox-handlesvia-perl',
    'libmoox-late-perl',
    'libmoox-options-perl',
    'libnamespace-clean-perl',
    'libpath-tiny-perl',
    'libstring-errf-perl',
    'libtry-tiny-perl',
    'libtypes-path-tiny-perl',
    'libyaml-libyaml-perl',
    'libyaml-perl',
    'nocache',
    'python3-xattr',
    'rsync',
    'squashfs-tools-ng',
  ]

  ensure_packages($iuk_builder_packages)

  file { '/usr/local/bin/wrap_tails_create_iuks':
    ensure => file,
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/wrap_tails_create_iuks',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  $tmpfiles_conf = '/etc/tmpfiles.d/tails-create-iuks.conf'
  file { $tmpfiles_conf:
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "D  ${temp_dir}  0755  root  root  -\n",
  } ~> exec { "systemd-tmpfiles --remove --create ${tmpfiles_conf}":
    refreshonly => true,
  }
}
