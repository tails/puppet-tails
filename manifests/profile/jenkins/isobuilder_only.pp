# @summary
#   Manage a jenkins isobuilder that doesn't do testing.
#
# @example
#   include tails::profile::isobuilder_only
#
class tails::profile::jenkins::isobuilder_only (
) {
# set our role as fact to be able to access role-based hiera
  tails::profile::facts_d::file { 'role':
    content => {
      role => 'isobuilder',
    },
  }

  include tails::profile::jenkins::isobuilder
}
