# @summary
#   Manage resources that are common to all Tails Jenkins slaves
#
# @param api_token
#   the api token
#
# @param api_user
#   the api user
#
# @param master_url
#   url to jenkins master
#
# @param node_name
#   the node's name
#
class tails::profile::jenkins::agent (
  String $api_token       = 'secret',
  String $api_user        = 'Tails',
  String $master_url      = "http://jenkins.${facts['networking']['domain']}:8080",
  Stdlib::Fqdn $node_name = $facts['networking']['fqdn'],
) {
  ### Sanity checks

  if $facts['os']['name'] != 'Debian' {
    fail('The tails::profile::jenkins::agent class only supports Debian.')
  }

  ### Resources

  $unit_file = '/etc/systemd/system/jenkins-slave.service'

  tails::profile::apt::signed_source { 'tails-jenkins-slave':
    key_fp     => 'D68F87149EBA77541573C1C12453AA9CE4123A9A',
    key_source => 'puppet:///modules/tails/D68F87149EBA77541573C1C12453AA9CE4123A9A.asc',
    location   => 'http://deb.tails.boum.org/',
    release    => 'jenkins-slave',
    repos      => 'main',
  }

  apt::pin { 'jenkins-slave':
    packages => 'jenkins-slave',
    origin   => 'deb.tails.boum.org',
    priority => 991,
  }

  tails::profile::apt::signed_source { 'tails-isotester-bookworm':
    key_fp     => 'D68F87149EBA77541573C1C12453AA9CE4123A9A',
    key_source => 'puppet:///modules/tails/D68F87149EBA77541573C1C12453AA9CE4123A9A.asc',
    location   => 'http://deb.tails.boum.org/',
    release    => 'isotester-bookworm',
    repos      => 'main',
  }

  apt::pin { 'virt-viewer':
    packages => 'virt-viewer',
    codename => 'isotester-bookworm',
    priority => 991,
  }

  package { 'jenkins-slave':
    ensure  => present,
    require => [
      Tails::Profile::Apt::Signed_source['tails-jenkins-slave'],
      Apt::Pin['jenkins-slave'],
    ],
  }

  file { '/usr/local/share/jenkins-slave-download':
    source => 'puppet:///modules/tails/jenkins/slaves/jenkins-slave-download',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/usr/local/share/jenkins-enable-node':
    source  => 'puppet:///modules/tails/jenkins/slaves/jenkins-enable-node',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['python3-jenkins'],
  }

  file { '/etc/jenkins':
    ensure  => directory,
    mode    => '0750',
    owner   => 'root',
    group   => 'jenkins',
    require => User['jenkins'],
  }

  file { '/etc/jenkins/jenkins_apikey':
    ensure  => file,
    content => $api_token,
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
    notify  => Service['jenkins-slave'],
  }

  file { '/etc/tmpfiles.d/tails-jenkins-slave.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "d  /run/jenkins  0775  root  jenkins  -\n",
    require => User['jenkins'],
  }

  file { '/etc/systemd/system/jenkins-slave.service.d':
    ensure => directory,
  }
  file { '/etc/systemd/system/jenkins-slave.service.d/cleanup-workspace.conf':
    content => "[Service]\nExecStartPre=/usr/bin/sudo /bin/rm -rf /var/lib/jenkins/workspace/\n",
  }

  package { 'python3-jenkins':
    ensure  => present,
  }

  file { $unit_file:
    content => template('tails/jenkins/jenkins-slave.service.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => [
      File['/usr/local/share/jenkins-enable-node'],
      File['/usr/local/share/jenkins-slave-download'],
      File['/etc/jenkins/jenkins_apikey'],
    ],
  }

  service { 'jenkins-slave':
    ensure   => running,
    enable   => true,
    provider => systemd,
    require  => File[$unit_file],
  }

  @user { 'jenkins':
    membership => minimum,
    require    => Package['jenkins-slave'],
    home       => '/var/lib/jenkins',
    system     => true,
  }

  rbac::group { 'jenkins-to-sudo':
    group              => 'sudo',
    additional_members => ['jenkins'],
    require            => User['jenkins'],
  }

  file { '/var/lib/jenkins':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0755',
    require => Package['jenkins-slave'],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => File['/var/lib/jenkins'],
  }

  mailalias { 'jenkins':
    recipient => 'root',
  }

  sshkeys::set_client_key_pair { 'jenkins@jenkins-slave':
    keyname => 'jenkins@jenkins-slave',
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }
}
