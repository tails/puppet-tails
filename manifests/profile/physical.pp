# @summary
#   Tweaks for physical machines
#
# @example
#   include tails::profile::physical
#
class tails::profile::physical () {
  # Packages

  $packages = [
    hwloc-nox,
    memlockd,
    'memtest86+',
    numactl,
    numad,
    parted,
  ]

  ensure_packages($packages)

  sysctl::value { 'kernel.numa_balancing': value => 1 }
  service { 'numad':
    ensure => running,
    enable => true,
  }
  file_line { 'numad_DAEMON_ARGS':
    path   => '/etc/default/numad',
    match  => '#?DAEMON_ARGS=',
    line   => 'DAEMON_ARGS="-u 95"',
    notify => Service['numad'],
  }
  # Start libvirtd.service after numad.service, so that the latter
  # can advise the former wrt. CPU/memory placement:
  file { '/etc/systemd/system/libvirtd.service.d':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }
  file { '/etc/systemd/system/libvirtd.service.d/after-numad.conf':
    content => "[Unit]\nAfter=numad.service\n",
  }

  case $facts['processors']['models'][0] {
    /^Intel/: {
      ensure_packages(['intel-microcode'])
    }
    /^AMD/: {
      apt::source { 'bullseye-nonfree':
        location => 'http://ftp.us.debian.org/debian',
        release  => 'bullseye',
        repos    => 'non-free',
        pin      => {
          originator => 'Debian',
          codename   => 'bullseye',
          component  => 'non-free',
          priority   => 990,
        },
      }
      package { 'amd64-microcode':
        require => Apt::Source['bullseye-nonfree'],
      }
    }
    default: {}
  }

# Smartmontools

  package { 'smartmontools': ensure => installed }
  service { 'smartmontools':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Package['smartmontools'],
  }

# Use deadline I/O scheduler

  systemd::unit_file { 'tweak-io-scheduler.service':
    content => '[Unit]
Description=Tweak per-disk block I/O scheduler

[Service]
Type=oneshot
RemainAfterExit=yes
User=root
ExecStart=/bin/sh -c \'for sysblock in /sys/block/nvme* /sys/block/sd* ; do \
                          if [ -d "$sysblock" ] && [ $(cat $sysblock/queue/rotational) = 0 ] ; then \
                             echo deadline > $sysblock/queue/scheduler ; \
                          fi ; \
                       done\'
[Install]
WantedBy=multi-user.target
',
  }
  service { 'tweak-io-scheduler':
    ensure  => running,
    enable  => true,
    require => Systemd::Unit_file['tweak-io-scheduler.service'],
  }

# Manage CPU frequency settings

  if $facts['processors']['models'][0] =~ /^AMD/ {
    class { 'tails::profile::cpufreq': energy_perf_policy => false }
  } else {
    include tails::profile::cpufreq
  }

# Install tools to control NMVe drives when needed and adjust smartmontools accordingly

  if $facts['disks'].filter | $k, $v | { $k =~ /nvme/ } != {} {
    ensure_packages(['nvme-cli'])
    file_line { 'smartmontools scan nvme':
      path    => '/etc/smartd.conf',
      match   => '^DEVICESCAN',
      line    => 'DEVICESCAN -d nvme -n standby -m root -M exec /usr/share/smartmontools/smartd-runner',
      require => Package['smartmontools'],
      notify  => Service['smartmontools'],
    }
  }
}
