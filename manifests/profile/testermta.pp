# @summary
#   Manage simple Postfix setup enabling nodes to send mail
#
# @param myhostname
#   postfix's myhostname
#
# @param myorigin
#   postfix's myorigin
#
# @param mydestination
#   postfix's mydestination
#
# @param email_user
#   the test user's username
#
# @param ssl_cert
#   path to ssl certificate
#
# @param ssl_key
#   path to ssl key
#
class tails::profile::testermta (
  String $myhostname             = $facts['networking']['fqdn'],
  String $myorigin               = $facts['networking']['fqdn'],
  Array  $mydestination          = ['localhost'],
  String $email_user             = "test@${facts['networking']['fqdn']}",
  Stdlib::Absolutepath $ssl_cert = '/etc/ssl/certs/ssl-cert-snakeoil.pem',
  Stdlib::Absolutepath $ssl_key  = '/etc/ssl/private/ssl-cert-snakeoil.key',
) {
  class { 'tails::profile::mta':
    myhostname            => $myhostname,
    myorigin              => $myorigin,
    mydestination         => $mydestination,
    allow_trusted_subnets => false,
    monitor               => false,
    extra_params          => {
      smtpd_sasl_type                  => 'dovecot',
      smtpd_sasl_path                  => 'private/auth',
      smtpd_use_tls                    => 'yes',
      smtpd_tls_auth_only              => 'yes',
      smtpd_tls_cert_file              => $ssl_cert,
      smtpd_tls_key_file               => $ssl_key,
      smtpd_tls_session_cache_database => "btree:\${data_directory}/smtpd_scache",
      virtual_transport                => 'lmtp:unix:private/dovecot-lmtp',
      virtual_mailbox_domains          => $myorigin,
      virtual_mailbox_maps             => 'hash:/etc/postfix/maps/vmailbox',
    },
  }

  concat::fragment { "postfix::map: vmailbox for ${email_user}":
    target  => '/etc/postfix/maps/vmailbox',
    content => "${email_user}  whatever",
  }

  postfix::config::service { 'submission':
    master_cf_file => '/etc/postfix/master.cf',
    type           => 'inet',
    priv           => 'n',
    chroot         => 'y',
    command        => 'smtpd',
    args           => [
      '-o syslog_name=postfix/submission',
      '-o smtpd_tls_security_level=encrypt',
      '-o smtpd_sasl_auth_enable=yes',
      '-o smtpd_reject_unlisted_recipient=no',
      '-o smtpd_recipient_restrictions=',
      '-o smtpd_relay_restrictions=permit_sasl_authenticated,reject',
      '-o milter_macro_daemon_name=ORIGINATING',
    ],
  }

  postfix::config::service { 'smtps':
    master_cf_file => '/etc/postfix/master.cf',
    type           => 'inet',
    priv           => 'n',
    chroot         => 'y',
    command        => 'smtpd',
    args           => [
      '-o syslog_name=postfix/smtps',
      '-o smtpd_tls_wrappermode=yes',
      '-o smtpd_sasl_auth_enable=yes',
      '-o smtpd_reject_unlisted_recipient=no',
      '-o smtpd_recipient_restrictions=',
      '-o smtpd_relay_restrictions=permit_sasl_authenticated,reject',
      '-o milter_macro_daemon_name=ORIGINATING',
    ],
  }
}
