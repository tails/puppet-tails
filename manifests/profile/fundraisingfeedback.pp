# @summary 
#   Sets a daily timer to extract feedback from website logs and mail data
#   to fundraising team
#
# @example
#   include tails::profile::fundraisingfeedback
#
class tails::profile::fundraisingfeedback () {
  # lint:ignore:strict_indent lint:ignore:140chars
  $_script = @(EOT)
  #!/usr/bin/python3
  
  import urllib.parse
  import smtplib
  
  messages = ''
  for logline in open('/tmp/yesterdayslogs.txt').readlines():
    if '/donate/canceled/submit' in logline:
      loglinepieces = logline.split()
      timestamp = loglinepieces[1] + ' ' + loglinepieces[2]
      message = urllib.parse.unquote_plus(loglinepieces[4][34:])
      messages += timestamp + "\n" + message + "\n"
      messages += "================================\n\n"
  if messages:
    content = "From: noreply@tails.net\nTo: fundraising@tails.net\nSubject: cancelled payments\n\nThe following feedback has been submitted:\n\n"
    content += messages
    smtpObj = smtplib.SMTP('localhost')
    smtpObj.sendmail('noreply@tails.net', 'fundraising@tails.net', content)
  | EOT
  # lint:endignore

  file { '/usr/local/bin/fundraisingfeedback.py':
    owner   => root,
    group   => root,
    mode    => '0755',
    content => $_script,
  }

  # lint:ignore:strict_indent
  $_service = @(EOT)
  [Service]
  Type=oneshot
  User=nobody
  Group=nogroup
  ExecStartPre=+/usr/bin/install -o nobody -g nogroup  /var/log/nginx/tails.net/access.log.1 /tmp/yesterdayslogs.txt
  ExecStart=/usr/local/bin/fundraisingfeedback.py
  ExecStopPost=/bin/rm -f /tmp/yesterdayslogs.txt
  | EOT

  $_timer = @(EOT)
  [Timer]
  OnCalendar=daily
  | EOT
  # lint:endignore

  systemd::timer { 'fundraisingfeedback.timer':
    timer_content   => $_timer,
    service_content => $_service,
    active          => true,
    enable          => true,
  }
}
