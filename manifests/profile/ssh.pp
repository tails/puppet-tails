# @summary
#   profile for ssh service
#
# @param sshd_tcp_forwarding
#   whether tcp forwarding is allowed
#
# @param sshd_agent_forwarding
#   whether agent forwarding is allowed
#
# @param sshd_x11_forwarding
#   whether x11 forwarding is allowed
#
# @param sshkeys
#   keys that should be in the known_hosts file
#
# @param sshd_port
#   the port on which sshd should listen
#
# @param sshd_config_extra
#   extra configuration for the SSH daemon
#
class tails::profile::ssh (
  String $sshd_tcp_forwarding   = 'no',
  String $sshd_agent_forwarding = 'no',
  String $sshd_x11_forwarding   = 'no',
  Hash $sshkeys                 = {},
  Integer $sshd_port            = 22,
  Hash $sshd_config_extra       = {},
) {
# install ssh daemon

  ensure_packages(['openssh-server'])

# set up the service

  service { 'ssh':
    ensure => running,
    enable => true,
  }

# configure sshd

  sshd_config { 'AllowTcpForwarding':
    ensure  => present,
    value   => $sshd_tcp_forwarding,
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }
  sshd_config { 'AllowAgentForwarding':
    ensure  => present,
    value   => $sshd_agent_forwarding,
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }
  sshd_config { 'X11Forwarding':
    ensure  => present,
    value   => $sshd_x11_forwarding,
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }
  sshd_config { 'PermitRootLogin':
    ensure  => present,
    value   => 'without-password',
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }
  sshd_config { 'ChallengeResponseAuthentication':
    ensure  => present,
    value   => 'no',
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }
  sshd_config { 'PasswordAuthentication':
    ensure  => present,
    value   => 'no',
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }
  sshd_config { 'DebianBanner':
    ensure  => present,
    value   => 'no',
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }

  ensure_resources('sshd_config', $sshd_config_extra, {
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  })

# knoweth thy neighbour, a.k.a. fill our known_hosts file

  $::facts['ssh'].each | String $keyname, Hash $keydata | {
    @@sshkey { "${facts['networking']['fqdn']}_${keyname}":
      host_aliases => [
        $facts['networking']['hostname'],
        $facts['networking']['fqdn'],
      ],
      key          => $keydata['key'],
      type         => $keydata['type'],
      tag          => 'hello_neighbour',
    }
    Sshkey <<| tag == 'hello_neighbour' |>>
  }

  ensure_resources('sshkey',$sshkeys)

# allow incoming connections to SSH port

  tirewall::accept_trusted_subnets { 'SSH':
    dport => $sshd_port,
  }

  firewall { '100 allow SSH access':
    dport  => $sshd_port,
    proto  => 'tcp',
    action => 'accept',
  }

# set up tor onion service for ssh

  tor::daemon::onion_service { 'ssh-hidden-v3':
    ports => [String($sshd_port)],
  }
}
