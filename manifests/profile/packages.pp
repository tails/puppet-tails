# @summary
#   Install the debian packages we want on every node and remove the
#   ones we don't want
#
# @example
#   include tails::profile::packages
#
class tails::profile::packages () {
  $base_packages = [
    apparmor,
    apparmor-profiles-extra,
    apparmor-utils,
    augeas-tools,
    bash,
    bash-completion,
    bzip2,
    ca-certificates,
    curl,
    gnupg,
    gnutls-bin,
    haveged,
    htop,
    iotop,
    iproute2,
    iputils-ping,
    less,
    linux-image-amd64,
    lvm2,
    molly-guard,
    nload,
    pinentry-curses,
    pwgen,
    rsync,
    safe-rm,
    sash,
    screen,
    sysstat,
    tmux,
    torsocks,
    virt-what,
    w3m,
    zsh,
  ]

  $unwanted_packages = [
    acpid,
    apparmor-profiles,
    debian-faq,
    doc-debian,
    icinga2-doc,
    pinentry-gtk2,
    rdiff-backup,
    task-english,
  ]

  ensure_packages($base_packages)
  ensure_packages($unwanted_packages, { 'ensure' => 'absent' })
}
