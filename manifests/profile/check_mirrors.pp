# @summary
#   profile to manage checking of Tails mirrors
#
# @param email_recipient
#   whom to mail if something is wrong
#
# @param homedir
#   the homedir of the user who runs the checks
#
# @param user
#   the posix user who runs the checks
#
# @param repo_user
#   the username with which to check out the git repo
#
# @param repo_host
#   the server we get the git repo from
#
# @param repo_name
#   the git repository's name
#
# @param repo_rev
#   the revision we want to check out
#
# @param repo_ensure
#   set to latest to ensure being up to date
#
# @param cronjob_script
#   path to the cron script
#
# @param cronjob_timeout
#   timeout after which to fail the checks
#
# @param cronjob_ensure
#   whether we want the cronjob to exist
#
class tails::profile::check_mirrors (
  String $email_recipient,
  Stdlib::Absolutepath $homedir             = '/var/lib/tails_check_mirrors',
  Pattern[/\A[a-z_]+\z/] $user              = 'tails_check_mirrors',
  Pattern[/\A[a-z_]+\z/] $repo_user         = 'git',
  Stdlib::Host $repo_host                   = 'gitlab-ssh.tails.boum.org',
  Pattern[/\A[a-z\/_-]+\z/] $repo_name      = 'tails/check-mirrors',
  String $repo_rev                          = 'master',
  String $repo_ensure                       = 'latest',
  String $cronjob_script                    = '/usr/local/bin/check_mirrors_cronjob.sh',
  String $cronjob_timeout                   = '300m',
  Enum['present', 'absent'] $cronjob_ensure = true,
) {
  validate_email_address($email_recipient)

  $repo_checkout    = "${homedir}/check-mirrors"
  $needed_packages  = ['curl', 'ruby-nokogiri', 'wget']

  ensure_packages($needed_packages)

  user { $user:
    ensure     => present,
    home       => $homedir,
    managehome => true,
    system     => true,
  }

  mailalias { $user:
    recipient => $email_recipient,
  }

  exec { "SSH key pair for user ${user}":
    command => "ssh-keygen -t rsa -b 4096 -N '' -f \"${homedir}/.ssh/id_rsa\"",
    user    => $user,
    creates => "${homedir}/.ssh/id_rsa",
  }

  include yapgp

  pgp_key { 'tails-signing-key':
    ensure => present,
    fp     => 'A490D0F4D311A4153E2BB7CADBB802B258ACD84F',
    source => 'https://tails.net/tails-signing.key',
    user   => $user,
  }

  vcsrepo { $repo_checkout:
    ensure   => $repo_ensure,
    provider => git,
    source   => "${repo_user}@${repo_host}:${repo_name}.git",
    revision => $repo_rev,
    user     => $user,
    require  => [
      User[$user],
      # NEEDFIX Requiring sshkeys is currently broken
      #Sshkey[$repo_host],
      Exec["SSH key pair for user ${user}"]
    ],
  }

  # XXX workaround for sysadmin#17988, remove once node is upgraded to bookworm
  tails::profile::git::safe { $repo_checkout: }

  file { $cronjob_script:
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0755',
    content => epp('tails/check_mirrors/cronjob.sh.epp', {
        command         => "${repo_checkout}/check-mirrors.rb",
        cronjob_script  => $cronjob_script,
        cronjob_timeout => $cronjob_timeout,
    }),
  }

  $flock_timeout = 30
  $error_msg = "Cron run of `check-mirrors.rb` timed out waiting for lock after ${flock_timeout}s."
  $args = ['--minimum-speed', '"7 MB/s"']
  $args_str = join($args, ' ')
  $hour = 0
  $minute = 16

  cron { 'tails_check_mirrors Full run':
    ensure      => $cronjob_ensure,
    command     => "sleep \$(( \$( </dev/urandom od -N2 -t u2 -A none ) >> 5 )) && flock --wait ${flock_timeout} check-mirrors.lock ${cronjob_script} ${args_str} || echo '${error_msg}' >&2", # lint:ignore:140chars -- command
    user        => $user,
    hour        => $hour,
    minute      => $minute,
    require     => [
      Vcsrepo[$repo_checkout],
      Package[$needed_packages],
      Mailalias[$user],
      Pgp_key['tails-signing-key'],
      File[$cronjob_script],
    ],
    # By default, check-mirrors.rb would create its per-run temporary
    # directory in $CWD, i.e. in this context $HOME, which is never
    # cleaned up automatically; so interrupted runs would leave
    # temporary files behind them forever. Let's ask check-mirrors.rb
    # to instead create its per-run temporary directory in a place
    # that is cleaned up on boot. This requires ~2.5 GiB free space
    # in /tmp so if we ever need to include this class on a system
    # that has a smaller tmpfs mounted there, we'll need to make
    # this configurable.
    environment => ['TMPDIR=/tmp'],
  }
}
