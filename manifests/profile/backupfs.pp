# @summary
#   profile to manage borg filesystem backups
#
# @param backupserver
#   server to write backups to
#
# @param excludes
#   array with exclusions from backups
#
class tails::profile::backupfs (
  String $backupserver,
  Array  $excludes = ['proc','dev','tmp','sys'],
) {
  borgbackup::backup_fs { 'rootfs':
    backupserver => $backupserver,
    excludes     => $excludes,
  }
}
