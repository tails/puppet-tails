# @summary
#   A GitLab Runner service, including a local proxy/caching registry.
#  
#       # GitLab Runner
#       mod 'puppetlabs-apt'
#       mod 'puppet-gitlab_ci_runner'
#       mod 'puppetlabs-stdlib'
#       
#       # Docker
#       mod 'puppetlabs-docker'
#       mod 'puppetlabs-reboot'
#       mod 'puppetlabs-translate'
#       
#   Use Hiera to add instances:
#  
#       tails::profile::gitlab_runner::concurrent: 4
#       tails::profile::gitlab_runner::runners:
#         Docker@my-server: {}
#       tails::profile::gitlab_runner::runner_defaults:
#         url: https://gitlab.tails.boum.org/
#         registration-token: <secret>
#         executor: docker
#         docker-image: debian:bullseye
#
#   TODO:
#  
#     - Support rootless containers.
#
# @param concurrent
#   amount of concurrent runners
#
# @param runners
#   hash with runners
#
# @param runner_defaults
#   hash with default parameters for runners
#
class tails::profile::gitlab_runner (
  Integer $concurrent = 4,
  Hash $runners = {},
  Hash $runner_defaults = {},
) {
  tails::profile::facts_d::file { 'role':
    content => {
      role => 'gitlab_runner',
    },
  }

  include tails::profile::gitlab_runner::docker
  include tails::profile::gitlab_runner::cache

  class { 'gitlab_ci_runner':
    concurrent      => $concurrent,
    manage_docker   => false,  # we want to pass our own defaults to the Docker class
    manage_repo     => true,
    runners         => $runners,
    runner_defaults => $runner_defaults,
  }
}
