# @summary
#   profile to remove nfs & rpc crap
#
class tails::profile::nonfs () {
  ### Optimize boot time and security
  package { ['nfs-common', 'rpcbind', 'portmap']: ensure => purged }
}
