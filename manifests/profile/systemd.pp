# @summary
#   Manage systemd.
#
# @example
#   include tails::profile::systemd
#
# @param ntp_servers
#   An array containing NTP servers.
#
# @param journald_storage
#   Controls where to store journal data.
#
# @param journald_maxretentionsec
#   The maximum time to store journal entries.
#
class tails::profile::systemd (
  Array $ntp_servers = ['0.pool.ntp.org', '1.pool.ntp.org'],
  String $journald_storage = 'volatile',
  String $journald_maxretentionsec = '5day',
) {
  package { 'ntp': ensure => absent }

  class { 'systemd':
    manage_timesyncd  => true,
    ntp_server        => $ntp_servers,
    manage_journald   => true,
    journald_settings => {
      'Storage'         => $journald_storage,
      'MaxRetentionSec' => $journald_maxretentionsec,
    },
  }
}
