# @summary
#   Manage CPU frequency settings
#
# @param cpufreq_governor
#   the governor (unmanaged when set to false)
#
# @param energy_perf_policy
#   the perf policy (unmanaged when set to false)
#
class tails::profile::cpufreq (
  Variant[Boolean, String] $cpufreq_governor   = 'performance',
  Variant[Boolean, String] $energy_perf_policy = 'performance',
) {
  ### Sanity checks

  [$cpufreq_governor, $energy_perf_policy].each |$param| {
    if type($param) =~ Type[Boolean] and $param {
      fail('The only supported boolean value is `false`')
    }
  }

  ### Resources

  ensure_packages(['linux-cpupower'])

  if $cpufreq_governor {
    systemd::unit_file { 'cpufreq-governor.service':
      require => Package['linux-cpupower'],
      content => "[Unit]
Description=Tweak cpufreq governor

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/cpupower -c all frequency-set --governor ${cpufreq_governor}

[Install]
WantedBy=multi-user.target
",
    }
    service { 'cpufreq-governor':
      ensure  => running,
      enable  => true,
      require => Systemd::Unit_file['cpufreq-governor.service'],
    }
  } else {
    # The service will be disabled on next boot; no way to revert to the default
    # system settings immediately: they are hardware-dependent
    systemd::unit_file { 'cpufreq-governor.service':
      ensure => absent,
    }
  }

  if $energy_perf_policy {
    file { '/etc/modules-load.d/msr.conf':
      owner   => root,
      group   => root,
      mode    => '0644',
      content => "msr\n",
    }
    systemd::unit_file { 'energy-perf-policy.service':
      require => [Package['linux-cpupower'], File['/etc/modules-load.d/msr.conf']],
      content => "[Unit]
Description=Tweak CPU energy/perf policy

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/sbin/x86_energy_perf_policy ${energy_perf_policy}

[Install]
WantedBy=multi-user.target
",
    }
    service { 'energy-perf-policy':
      ensure  => running,
      enable  => true,
      require => Systemd::Unit_file['energy-perf-policy.service'],
    }
  } else {
    # The service will be disabled on next boot; no way to revert to the default
    # system settings immediately: they are hardware-dependent
    systemd::unit_file { 'energy-perf-policy.service':
      ensure => absent,
    }
  }
}
