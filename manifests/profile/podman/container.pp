# @summary
#   A Podman container.
#
# @param uid
#   The uid of the rootless container user.
#
# @param gid
#   The gid of the rootless user.
#
# @param user
#   User for running rootless containers.
#
# @param group
#   Group for running rootless containers.
#
# @param home
#   The user's home directory.
#
# @param image
#   Container registry source of the image being deployed. 
#
# @param flags
#   Flags for the 'podman container create' command.
#
# @param subdirs
#   A list of subdirectories of $home to create.
define tails::profile::podman::container (
  Integer $uid,
  Integer $gid,
  String $user  = $name,
  String $group = $name,
  String $home  = "/srv/${name}",
  String $image = undef,
  Hash $flags   = {},
  Array[Hash] $subdirs = [],
) {
  #
  # System user and group
  #

  group { $group:
    system => true,
    gid    => $gid,
  }

  user { $user:
    system     => true,
    home       => $home,
    uid        => $uid,
    gid        => $gid,
    groups     => ['systemd-journal'],
    managehome => true,
    require    => Group[$user],
  }

  file { $home:
    ensure  => directory,
    owner   => $user,
    group   => $group,
    mode    => '0750',
    require => [
      User[$user],
      Group[$group],
    ],
  }

  $subdirs.each | Hash $subdir | {
    file { "${home}/${subdir['path']}":
      ensure  => directory,
      owner   => $subdir['uid'],
      group   => $subdir['gid'],
      mode    => '0755',
      require => [
        File[$home],
      ],
    }
  }

  podman::subuid { $user:
    subuid  => $uid + 1,
    count   => 65536,
    require => User[$user],
  }

  podman::subgid { $group:
    subgid  => $gid + 1,
    count   => 65536,
    require => Group[$group],
  }

  #
  # Podman config
  #

  file { "${home}/.config/containers":
    ensure  => 'directory',
    owner   => $user,
    group   => $group,
    mode    => '0755',
    require => File["${home}/.config"],
  }

  file { "${home}/.config/containers/storage.conf":
    ensure  => 'file',
    # lint:ignore:strict_indent
    content => @(EOT),
      [storage]
      driver = "overlay"
      rootless_storage_path = "${HOME}/.local/share/containers/storage"

      [storage.options.overlay]
      mount_program = "/usr/bin/fuse-overlayfs"
      mountopt = "nodev"
      | EOT
    # lint:endignore
    owner   => $user,
    group   => $group,
    mode    => '0644',
    require => File["${home}/.config/containers"],
  }

  #
  # Container
  #

  $_subdirs = $subdirs.map | $subdir | { "${home}/${subdir['path']}" }

  podman::container { $name:
    image   => $image,
    user    => $user,
    flags   => $flags,
    require => [
      File[$home],
      File[$_subdirs],
      Sysctl::Value['kernel.unprivileged_userns_clone'],
      Package['fuse-overlayfs'],
    ],
  }
}
