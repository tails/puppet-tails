# @summary
#   Manage a system's firewall.
#
# @example
#   include tails::profile::firewall
#
# @param rules
#   A Hash containing firewall rules to be applied to this machine.
#
# @param public_services
#   A Hash containing description of public services that run in this node.
#
# @param accept_forwarding
#   A Hash containing description of `tirewall::accept_forwarding` defined resources.
#
class tails::profile::firewall (
  Hash $rules = {},
  Hash $public_services = {},
  Hash $accept_forwarding = {},
) {
  include tirewall
  create_resources('firewall', $rules)
  create_resources('tirewall::public_service', $public_services)
  create_resources('tirewall::accept_forwarding', $accept_forwarding)
}
