# @summary
#   Manage a mirror of a git-annex repository via a mounted device
#
# @param user
#   the posix user to do the mirroring
#
# @param home
#   the user's homedir
#
# @param ssh_keyname
#   the ssh keyname
#
# @param checkout_dir
#   directory of local mirror
#
# @param remote_repo
#   the remote repo to mirror
#
# @param mount_point
#   the target directory for the mount
#
# @param mount_device
#   the source device of the mount
#
# @param mount_fstype
#   the filesystem type of the mount
#
# @param mount_options
#   the options for the mount
#
# @param ensure
#   to mirror or not to mirror
#
# @param mode
#   mirror mode
#
# @param direct_mode
#   direct or not
#
# @param pull_hour
#   which hours to pull
#
# @param pull_minute
#   which minutes to pull
define tails::profile::git_annex::mirror (
  Pattern[/\A[a-z_-]+\z/] $user,
  Stdlib::Absolutepath $home,
  String $ssh_keyname,
  Stdlib::Absolutepath $checkout_dir,
  String $remote_repo,
  String $mount_point,
  String $mount_device,
  String $mount_fstype,
  String $mount_options,
  Enum['present', 'absent'] $ensure           = 'present',
  String $mode                                = 'mirror',
  Boolean $direct_mode                        = true,
  Array[String] $pull_hour                    = ['*'],
  Array[String] $pull_minute                  = ['14', '29', '44', '59'],
) {
  ### Resources

  vcsrepo { $checkout_dir:
    ensure   => $ensure,
    provider => git,
    source   => $remote_repo,
    user     => $user,
    require  => Sshkeys::Set_client_key_pair[$ssh_keyname],
  }

  file { $checkout_dir:
    ensure  => directory,
    owner   => $user,
    group   => $user,
    mode    => '0755',
    require => Vcsrepo[$checkout_dir],
  }

  exec { "Switch ${name} to a locked adjusted branch":
    command => '/usr/bin/git annex adjust --lock',
    user    => $user,
    cwd     => $checkout_dir,
    unless  => "test `git -C '${checkout_dir}' branch --show-current` = 'adjusted/master(locked)'",
    require => [
      Package['git-annex'],
      Vcsrepo[$checkout_dir],
    ],
  }

  $service_ensure = $ensure ? {
    'absent' => stopped,
    default  => running,
  }
  $daemon_reload_exec = "systemctl-daemon-reload-git-annex-mirror-${name}"
  exec { $daemon_reload_exec:
    refreshonly => true,
    command     => '/bin/systemctl daemon-reload',
  }
  $timer = "git-annex-mirror-${name}.timer"
  file { "/etc/systemd/system/${timer}":
    ensure  => $ensure,
    content => template('tails/git_annex/mirror.timer.erb'),
    notify  => [
      Exec[$daemon_reload_exec],
      Service[$timer],
    ],
  }
  $command = "/usr/local/bin/pull-git-annex ${mode}"
  file { "/etc/systemd/system/git-annex-mirror-${name}.service":
    content => template('tails/git_annex/mirror.service.erb'),
    notify  => Exec[$daemon_reload_exec],
  }
  service { $timer:
    ensure   => $service_ensure,
    provider => 'systemd',
    enable   => true,
    require  => [
      Exec["Switch ${name} to a locked adjusted branch"],
      File['/usr/local/bin/pull-git-annex'],
      Exec[$daemon_reload_exec],
    ],
  }

  user { $user:
    ensure => $ensure,
    system => true,
    home   => $home,
  }

  file { [$home, "${home}/.ssh"]:
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  sshkeys::set_client_key_pair { $ssh_keyname:
    keyname => $ssh_keyname,
    user    => $user,
    home    => $home,
    require => [
      User[$user],
      File["${home}/.ssh"],
    ],
  }

  file { $mount_point:
    ensure  => directory,
    owner   => $user,
    group   => $user,
    mode    => '0755',
    require => Mount[$mount_point],
  }

  mount { $mount_point:
    ensure  => mounted,
    device  => $mount_device,
    fstype  => $mount_fstype,
    options => $mount_options,
  }
}
