# @summary
#   An Nginx frontend for git_annex mirrors
#
# @param www_root
#   the webroot
#
# @param ssl_redirect
#   whether to redirect http to https
#
# @param locations
#   the locations
#
# @param server_cfg_prepend
#   hash with config prepends
#
define tails::profile::git_annex::nginx_frontend (
  Stdlib::Absolutepath $www_root,
  Boolean $ssl_redirect,
  Hash $locations = {},
  Optional[Hash] $server_cfg_prepend = undef,
) {
  ensure_packages(['libnginx-mod-http-fancyindex'])

  tails::profile::nginx::server { $name:
    www_root           => $www_root,
    ssl                => true,
    ssl_redirect       => $ssl_redirect,
    locations          => $locations,
    server_cfg_prepend => $server_cfg_prepend,
  }
}
