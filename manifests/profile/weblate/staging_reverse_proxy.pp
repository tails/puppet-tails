# @summary
#   Manage a nginx vhost that reverse proxies to a weblate instance.
#
# @param public_hostname
#   the public hostname of the staging site
#
# @param upstream_hostname
#   the backend to proxy to
#
# @param upstream_port
#   the http port to proxy to
#
class tails::profile::weblate::staging_reverse_proxy (
  Stdlib::Fqdn $public_hostname     = 'staging.tails.net',
  Stdlib::Fqdn $upstream_hostname   = 'translate',
  Stdlib::Port $upstream_port       = 80,
) {
  tails::profile::nginx::reverse_proxy { 'weblate_staging':
    public_hostname   => $public_hostname,
    upstream_hostname => $upstream_hostname,
    upstream_port     => $upstream_port,
  }

  nginx::resource::location { 'weblate_staging_nophp':
    server                     => $public_hostname,
    location                   => '~* \.php.*$',
    ssl_only                   => true,
    index_files                => [],
    location_custom_cfg_append => { return   => '403;' },
  }
}
