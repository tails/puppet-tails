# @summary
#   A copy of the Tails website to preview translations
#
class tails::profile::weblate::staging_website (
) inherits weblate::params { #lint:ignore:inherits_across_namespaces
  include tails::profile::website::builder

  file { $weblate::params::staging_www_dir:
    ensure => directory,
    mode   => '0755',
    owner  => $weblate::params::system_uid,
    group  => $weblate::params::system_gid,
  }

  file { "${weblate::params::weblate_config_dir}/ikiwiki.setup":
    content => epp('tails/website/ikiwiki.setup.epp', {
        src_dir                      => "${weblate::params::weblate_repos_dir}/staging/wiki/src",
        web_dir                      => $weblate::params::staging_www_dir,
        url                          => "https://${weblate::params::staging_site_domain}",
        is_staging                   => true,
        weblate_additional_languages => $weblate::params::weblate_additional_languages,
        po_slave_languages           => $weblate::params::weblate_slave_languages,
        underlays                    => [],
    }),
    mode    => '0644',
    owner   => weblate,
    group   => weblate,
  }
}
