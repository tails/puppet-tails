# @summary
#   Manage a nginx vhost that reverse proxies to a weblate instance.
#
# @param upstream_hostname
#   the backend to proxy to
#
# @param upstream_port
#   the http port to proxy to
#
# @param htpasswd
#   an optional htpasswd file content to protect the instance
#
class tails::profile::weblate::reverse_proxy (
  Stdlib::Fqdn $upstream_hostname   = 'translate.lizard',
  Stdlib::Port $upstream_port       = 80,
  Optional[String] $htpasswd        = undef,
) inherits weblate::params { #lint:ignore:inherits_across_namespaces
  $auth_file_name = "/etc/nginx/auth.d/htpasswd.${upstream_hostname}"

  case $htpasswd {
    String: {
      $auth_params = {
        auth_basic => 'Tails Translation Platform',
        auth_basic_user_file => "auth.d/htpasswd.${upstream_hostname}",
      }
      file { $auth_file_name:
        ensure  => 'file',
        owner   => 'www-data',
        group   => 'www-data',
        mode    => '0640',
        content => $htpasswd,
        require => File['/etc/nginx/auth.d'],
      }
    }
    default: {
      $auth_params = {}
      file { $auth_file_name:
        ensure  => 'absent',
      }
    }
  }

  tails::profile::nginx::reverse_proxy { 'weblate':
    auth_params       => $auth_params,
    public_hostname   => $weblate::params::weblate_site_domain,
    upstream_hostname => $upstream_hostname,
    upstream_port     => $upstream_port,
  }

  nginx::resource::location { 'weblate_nophp':
    server                     => $weblate::params::weblate_site_domain,
    location                   => '~* \.php.*$',
    ssl_only                   => true,
    index_files                => [],
    location_custom_cfg_append => { return   => '403;' },
  }
}
