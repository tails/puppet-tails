# @summary
#   Enable discard (aka. TRIM) operations (#11788)
#
# @example
#   include tails::profile::fstrim
#
class tails::profile::fstrim () {
  augeas { 'lvm.conf_issue_discards':
    context => '/files/etc/lvm/lvm.conf',
    changes => 'set devices/dict/issue_discards/int 1',
    require => Package['lvm2'],
  }
  service { 'fstrim.timer':
    ensure   => running,
    enable   => true,
    provider => systemd,
  }
}
