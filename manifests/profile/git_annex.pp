# @summary
#   Manage the git-annex package
#
# @param ensure
#   to git-annex or not to git-annex
#
# @param with_recommends
#   with or without recommends
#
class tails::profile::git_annex (
  Enum['present', 'absent'] $ensure         = present,
  Variant[Boolean, String] $with_recommends = 'default',
) {
  $install_options = $with_recommends ? {
    false   => ['--no-install-recommends'],
    default => [],
  }

  package { 'git-annex':
    ensure          => $ensure,
    install_options => $install_options,
  }

  file { '/usr/local/bin/pull-git-annex':
    ensure => $ensure,
    source => 'puppet:///modules/tails/git-annex/pull-git-annex',
    owner  => root,
    group  => root,
    mode   => '0755',
  }
}
