# @summary
#   Manage unbound
#
class tails::profile::unbound (
) {
  class { 'unbound':
    interface => ['0.0.0.0'],
    access    => ['0.0.0.0'],
  }
}
