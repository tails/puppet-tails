# @summary
#   profile for release tasks, needs to be refactored
class tails::profile::release_misc (
) {
  rbac::user { 'foundations-team-members': }
  include tails::profile::git_annex
}
