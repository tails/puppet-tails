# @summary
#   Manage installation of Git
class tails::profile::git () {
  ensure_packages(['git'])

  # XXX Remove once all infra is upgraded to Bookworm.
  #     See: https://github.com/puppetlabs/puppetlabs-vcsrepo/issues/535
  case $::facts['os']['distro']['codename'] {
    'bullseye': {
      concat { '/etc/gitconfig':
        owner => 'root',
        group => 'root',
        mode  => '0644',
      }
    }
    default: {}
  }
}
