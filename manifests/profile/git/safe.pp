# Configure a "safe" directory, as a workaround for:
#    https://gitlab.tails.boum.org/tails/sysadmin/-/issues/17988
#    https://github.com/puppetlabs/puppetlabs-vcsrepo/issues/535
define tails::profile::git::safe {
  include tails::profile::git

  case $::facts['os']['distro']['codename'] {
    'bullseye': {
      concat::fragment { "gitconfig ${name}":
        target  => '/etc/gitconfig',
        content => "[safe]\n\tdirectory = ${name}\n\n",
        before  => Vcsrepo[$name],
      }
    }
    default: {}
  }
}
