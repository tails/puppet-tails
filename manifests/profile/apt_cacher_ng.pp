# @summary
#   profile to manage apt-cacher-ng
#
# @param tails_git_user
#   posix user to maintain local clone of tails.git
#
# @param tails_git_home
#   homedir of tails_git_user
#
# @param tails_git_remote
#   where to get tails.git from
#
# @param tails_git_revision
#   which revision of tails.git we want
#
class tails::profile::apt_cacher_ng (
  Pattern[/\A[a-z_-]+\z/] $tails_git_user = 'tails-git',
  Stdlib::Absolutepath $tails_git_home    = '/var/lib/tails-git',
  String $tails_git_remote                = 'https://gitlab.tails.boum.org/tails/tails.git',
  String $tails_git_revision              = 'devel',
) {
  ### Sanity checks

  if $facts['os']['name'] != 'Debian' or versioncmp($facts['os']['distro']['release']['major'], '11') < 0 {
    fail('This module only supports Debian 11 or newer.')
  }

  ### Resources

  package { 'apt-cacher-ng':
    ensure  => present,
  }

  file_line { 'apt-cacher-ng allow ports 80 and 443':
    path    => '/etc/apt-cacher-ng/acng.conf',
    match   => 'AllowUserPorts:',
    line    => 'AllowUserPorts: 80 443',
    require => Package['apt-cacher-ng'],
    notify  => Service['apt-cacher-ng'],
  }

  file { '/var/cache/apt-cacher-ng':
    ensure  => directory,
    owner   => 'apt-cacher-ng',
    group   => 'apt-cacher-ng',
    mode    => '2755',
    require => Package['apt-cacher-ng'],
    notify  => Service['apt-cacher-ng'],
  }

  service { 'apt-cacher-ng':
    subscribe => File['/etc/apt-cacher-ng/backends_debian'],
    require   => Package['apt-cacher-ng'],
  }

  tirewall::accept_trusted_subnets { 'tails::apt_cacher_ng':
    dport => 3142,
  }

  file { '/etc/apt-cacher-ng/backends_debian':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "http://ftp.us.debian.org/debian/\n",
    require => Package['apt-cacher-ng'],
    notify  => Service['apt-cacher-ng'],
  }

  # lint:ignore:strict_indent lint:ignore:140chars
  $_isobuildsconf = @(EOT)
  ExTreshold: 50
  PfilePattern = .*(\.d?deb|\.rpm|\.dsc|\.tar(\.gz|\.bz2|\.lzma|\.xz)(\.gpg)?|\.diff(\.gz|\.bz2|\.lzma|\.xz)|\.o|\.jigdo|\.template|changelog|copyright|\.udeb|\.debdelta|\.diff/.*\.gz|(Devel)?ReleaseAnnouncement(\?.*)?|[a-f0-9]+-(susedata|updateinfo|primary|deltainfo).xml.gz|fonts/(final/)?[a-z]+32.exe(\?download.*)?|/dists/.*/installer-[^/]+/[0-9][^/]+/images/.*|/artifacts/raw/arti-linux)$
  VfilePatternEx = .*/project/trace/[a-z-]+$
  | EOT
  # lint:endignore

  # Custom apt-cacher-ng configuration to support ISO builds.
  file { '/etc/apt-cacher-ng/builds.conf':
    content => $_isobuildsconf,
    require => Package['apt-cacher-ng'],
    owner   => root,
    group   => root,
    mode    => '0644',
    notify  => Service['apt-cacher-ng'],
  }

  # Keep the acng configuration up-to-date for ISO builds

  $tails_git_dir = "${tails_git_home}/git"

  user { $tails_git_user:
    home       => $tails_git_home,
    managehome => true,
    system     => true,
  }

  vcsrepo { $tails_git_dir:
    ensure   => 'latest',
    provider => git,
    source   => $tails_git_remote,
    revision => $tails_git_revision,
    user     => $tails_git_user,
  }

  # XXX workaround for sysadmin#17988, remove once node is upgraded to bookworm
  tails::profile::git::safe { $tails_git_dir: }

  cron { 'update-acng-config':
    command => "cd '${tails_git_dir}' && ./auto/scripts/update-acng-config",
    user    => root,
    hour    => '18',
    minute  => '35',
    notify  => Service['apt-cacher-ng'],
    require => [
      Package['apt-cacher-ng'],
      Vcsrepo[$tails_git_dir],
    ],
  }
  # Periodically empty the parts of acng's cache that cannot easily be
  # cleaned up in a more clever way, and would otherwise be growing
  # forever. Ideally we would instead use DontCache, but it causes
  # lots of additional network traffic, network connection book-keeping,
  # and seems to work quite bad generally.
  #
  # At of 2016-07-15, these are:
  #  * deb.tails.boum.org: cached dists+pool for our custom APT repository;
  #    it would be cleaned up automatically by acng, but it happens
  #    to be caught by the glob, which is no big deal -- KISS!
  #  * tagged.snapshots.deb.tails.boum.org,
  #    time-based.snapshots.deb.tails.boum.org: dists for our APT snapshots;
  #    the "upstream" APT snapshots repository deletes old dists on its side,
  #    so acng's regular expiration process gets 404 and errs on the safe side
  #    i.e. does not delete any content; hence we have to delete these dists
  #    on this side ourselves; let's hope that the content of the corresponding
  #    pools (tailssnapshots*) will be properly garbage-collected by acng's
  #    regular expiration process, once the files in there are not referenced
  #    by any remaining dist
  #  * torbrowser-archive.tails.boum.org: as its name says; not an APT repo,
  #    so can't be automatically cleaned up by acng

  cron { 'prune-acng-cache':
    command => 'rm -rf /var/cache/apt-cacher-ng/*.tails.boum.org',
    user    => 'apt-cacher-ng',
    weekday => 'Thursday',
    hour    => '23',
    minute  => '57',
    require => Package['apt-cacher-ng'],
  }
}
