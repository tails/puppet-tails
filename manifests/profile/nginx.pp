# @summary
#   profile to manage nginx
#
# @param servers
#   hash with servers to be deployed
#
# @param reverse_proxies
#   hash with reverse proxies to be deployed
#
class tails::profile::nginx (
  Hash $servers = {},
  Hash $reverse_proxies = {},
) {
  class { 'nginx':
    log_format                => {
      noip => '$remote_user [$time_local]  "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"',
    },
    http_format_log           => 'noip',
    include_modules_enabled   => true,
    proxy_set_header          => [
      'Host $host',
      'X-Real-IP $remote_addr',
      'X-Forwarded-For $proxy_add_x_forwarded_for',
      'Proxy ""',
      'X-Forwarded-Proto $scheme',
    ],
    server_purge              => true,
    ssl_prefer_server_ciphers => 'on',
    ssl_dhparam               => '/etc/nginx/dhparams.pem',
    ssl_ciphers               => 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256',  # lint:ignore:140chars -- config param
    ssl_protocols             => 'TLSv1.2 TLSv1.3',
    names_hash_bucket_size    => 128,  # support .onion v3 addresses
  }

  openssl::dhparam { '/etc/nginx/dhparams.pem':
    require => Package['nginx'],
  } ~> Service['nginx']

  file { '/etc/nginx/auth.d':
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0750',
    require => Package['nginx'],
  } -> Service['nginx']

  # Facilities for TLS certs

  ensure_packages(['ssl-cert'])

  file { '/usr/local/sbin/install-tls-cert':
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/profile/nginx/install-tls-cert.sh',
  }

  file { '/etc/nginx/ssl':
    ensure => 'directory',
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0550',
  }

  file { '/etc/nginx/include.d':
    ensure => 'directory',
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0755',
  }

  ensure_packages(['libnginx-mod-http-fancyindex'])

  include tails::profile::letsencrypt

  # Declare resources passed as class parameters

  ensure_resources('tails::profile::nginx::server', $servers)
  ensure_resources('tails::profile::nginx::reverse_proxy', $reverse_proxies)

  # XXX Workaround https://github.com/voxpupuli/puppet-nginx/issues/1372
  #     Remove after we upgrade to Puppet 7 and latest voxpupuli/nginx.
  systemd::dropin_file { 'runtime_directory.conf':
    unit    => 'nginx.service',
    # lint:ignore:strict_indent
    content => @(EOT),
      # Managed by Puppet
      [Service]
      RuntimeDirectory=nginx
      | EOT
    # lint:endignore
  }
}
