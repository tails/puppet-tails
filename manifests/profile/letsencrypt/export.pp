# @summary
#   Export Let's Encrypt certificates as file resources
#
# @param tag
#   The tag to use in the exported resources
#
define tails::profile::letsencrypt::export (
  Optional[String] $tag = undef,
) {
  @@file { "/etc/ssl/${name}":
    ensure => directory,
    tag    => $tag,
    owner  => 'root',
    group  => 'root',
    mode   => '0500',
  }

  @@file { "/etc/ssl/${name}/cert.pem":
    tag     => $tag,
    content => $facts['tails_live_certs'][$name]['cert.pem'],
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    require => File["/etc/ssl/${name}"],
  }

  @@file { "/etc/ssl/${name}/fullchain.pem":
    tag     => $tag,
    content => $facts['tails_live_certs'][$name]['fullchain.pem'],
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    require => File["/etc/ssl/${name}"],
  }

  @@file { "/etc/ssl/${name}/privkey.pem":
    tag     => $tag,
    content => $facts['tails_live_certs'][$name]['privkey.pem'],
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
    require => File["/etc/ssl/${name}"],
  }
}
