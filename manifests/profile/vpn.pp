# @summary
#   Configure a node's VPN connection
#
# @example
#   include tails::profile::vpn
#
# @param address
#   The node's IP address in the VPN.
#
# @param connect_to
#   The node to connect to
#
# @param announce_routes
#   A Hash used to export network routes to the VPN. Each key is an identifier
#   and each corresponding value is a destination subnet.
#
# @param collect_routes
#   Either a Boolean or an Array[String] that indicates whether or which
#   network routes exported to the VPN should be collected in this node.
#   If `true`, all exported network routes are collected (except the ones
#   exported by this very node). If an Array[String] is passed, only routes
#   tagged with one of the items in the Array are collected.
#
# @param proxy
#   A potential proxy
#
# @param accept_from
#   A Hash mapping source names to IP addresses from which we should accept
#   incoming connections to the Tinc port.
#
# @param ensure
#   whether tinc should be installed or not
#   NB: setting this to absent doesn't clean up properly
#
# @param vpn_nodes
#   an array with all the names of nodes in the vpn
#
class tails::profile::vpn (
  Stdlib::IP::Address::V4::Nosubnet $address,
  Array[String] $connect_to                       = [],
  Hash $announce_routes                           = {},
  Variant[Boolean, Array[String]] $collect_routes = true,
  Optional[String] $proxy                         = undef,
  Hash $accept_from                               = {},
  Enum['present', 'absent'] $ensure               = 'present',
  Array[String] $vpn_nodes                        = [],
) {
  $interface = 'tailsvpn'
  $_connect_to = $connect_to.filter | $target | { $target != $facts['networking']['hostname'] }

  package { 'tinc':
    ensure  => $ensure,
  }

  concat { '/etc/tinc/nets.boot':
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Package['tinc'],
  }

  service { 'tinc':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    require    => Package['tinc'],
  }

  concat::fragment { 'nets_boot_debian_comment':
    target  => '/etc/tinc/nets.boot',
    content => "## This file contains all names of the networks to be started on system startup.\n",
    order   => '00',
  }

  $vpn_nodes.each |String $node| {
    file { "/etc/tinc/tailsvpn/hosts/${node}":
      ensure => $ensure,
      owner  => 'root',
      group  => 'root',
      mode   => '0600',
      source => "puppet:///modules/tails/vpn/tailsvpn/hosts/${node}",
    }
  }

  tails::profile::vpn::instance { "${facts['networking']['hostname']}@${interface}":
    vpn_address    => "${address}/32",
    vpn_subnet     => '10.10.0.0/255.255.255.0',
    connect_to     => $_connect_to,
    proxy          => $proxy,
    collect_routes => $collect_routes,
  }

  $announce_routes.each | String $route_name, String $subnet | {
    @@concat::fragment { "${route_name} tinc-up ${interface}":
      target  => "/etc/tinc/${interface}/tinc-up",
      content => "# ${route_name}\nip route add ${subnet} via ${address} dev ${interface}\n",
      order   => 1,
      tag     => $trusted['certname'],
    }

    @@concat::fragment { "${route_name} tinc-down ${interface}":
      target  => "/etc/tinc/${interface}/tinc-down",
      content => "# ${route_name}\nip route del ${subnet} via ${address} dev ${interface}\n",
      order   => 1,
      tag     => $trusted['certname'],
    }

    @@tails::profile::vpn::firewall_rule { "300 accept routing to ${route_name} via ${interface} (exported by ${trusted['certname']})":
      params => {
        table       => 'filter',
        chain       => 'OUTPUT',
        destination => $subnet,
        outiface    => $interface,
        action      => 'accept',
      },
      tag    => $trusted['certname'],
    }
  }

  $accept_from.each | String $name, Stdlib::IP::Address::V4 $address | {
    tails::profile::vpn::accept_from { $name: address => $address }
  }
}
