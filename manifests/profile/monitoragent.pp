# @summary
#   profile for our monitoring agents
#
class tails::profile::monitoragent () {
  $plugin_packages = [
    'monitoring-plugins',
    'monitoring-plugins-basic',
    'monitoring-plugins-common',
    'monitoring-plugins-standard',
    'nagios-plugins-contrib',
    'python3-nagiosplugin',
  ]
  ensure_packages( $plugin_packages, { ensure => latest })

  include monitoring::agent

  tirewall::accept_trusted_subnets { 'Icinga2 Agent':
    dport => 5665,
  }
}
