# @summary
#   Install a `mirrors.json` file.
#
# @param path
#   The full path of the mirrors.json file (note: the parent directory must
#   already exist).
#
# @param mirrors
#   A list of mirrors in `mirrors.json` v1 format.
define tails::profile::mirrors_json (
  Stdlib::Absolutepath $path = $name,
  Array[Struct[{
        url_prefix      => Pattern[/^https:\/\/.*\/$/],
        rsync_url       => Pattern[/^rsync:\/\/.*\/$/],
        email           => Pattern[/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/],
        weight          => Integer[0],
        Optional[notes] => String,
  }]] $mirrors = lookup('tails::mirrors', Array, 'first', undef),
) {
  file { $path:
    ensure  => file,
    content => stdlib::to_json({ version => 1, mirrors => $mirrors }),
    owner   => root,
    group   => root,
    mode    => '0644',
  }
}
