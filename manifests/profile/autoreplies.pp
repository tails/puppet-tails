# @summary
#   Manage autoreplies
#
# @param autoreplies
#   hash of all addresses that require an autoreply
#
class tails::profile::autoreplies (
  Hash $autoreplies = {},
) {
  ### Sanity checks

  if $facts['os']['name'] != 'Debian' or versioncmp($facts['os']['distro']['release']['major'], '11') < 0 {
    fail('The schleuder profile only supports Debian Bullseye or higher.')
  }

  include tails::profile::schleuder

  keys($autoreplies).each |String $address| {
    concat::fragment { "postfix::map: list transport fragment ${address}":
      target  => '/etc/postfix/maps/transport',
      content => "${address} dovecot:\n",
    }
    concat::fragment { "dovecot passwd entry for ${address}":
      target  => '/usr/local/etc/virtual_accounts_passwd',
      content => "${address}:{SSHA512}k1HXgoyZyn9iGVBYMwXT03Z+4hn+buVFH75g1eei59CuWFSWV8SV/M9adySF+gXpD6jIpe51P0hJQoDXHRrXKaoIYz5=\n",
    }

    $subject = $autoreplies[$address]['subject']
    $content = $autoreplies[$address]['content']

    file { "/srv/mail/${address}":
      ensure => directory,
      mode   => '0700',
      owner  => 'vmail',
      group  => 'vmail',
    }
    file { "/srv/mail/${address}/.dovecot.sieve":
      ensure  => file,
      mode    => '0640',
      owner   => 'vmail',
      group   => 'vmail',
      content => "require [\"fileinto\", \"envelope\", \"vacation\"];\n\nvacation :subject \"${subject}\"\n  \"${content}\";\ndiscard;\n", # lint:ignore:140chars
      require => File["/srv/mail/${address}"],
    }
  }
}
