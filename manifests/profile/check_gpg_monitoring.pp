# @summary
#   Checks whether all PGP keys shipped with tails are monitored
#
#   Sets a daily timer to compare the keys shipped with tails with the
#   monitoring configuration in hiera.
#
# @example
#   include tails::profile::check_gpg_monitoring
#
class tails::profile::check_gpg_monitoring () {
  ensure_packages(['git'])

  # lint:ignore:strict_indent lint:ignore:140chars
  $_script = @(EOT)
  #!/bin/bash
  
  TMPDIR=$(mktemp -d)
  
  cleanup()
  {
    /bin/rm -rf $TMPDIR
  }
  trap cleanup EXIT
  
  cd $TMPDIR
  
  /usr/bin/git clone -q https://gitlab.tails.boum.org/tails/tails.git
  /usr/bin/git clone -q https://gitlab.tails.boum.org/tails/puppet-code.git
  
  MISSING=""
  EXCLUDES="mirrors-tails-net.key tails-accounting.key tails-board.key tails-bugs.key tails-foundations.key tails-fundraising.key tails-mirrors.key tails-press.key tails-sysadmins.key tails-weblate.key"
  
  for i in `ls tails/wiki/src/*key |xargs -n 1 basename|grep -v tails-signing.key`; do
    if ! [[ $EXCLUDES == *$i* ]]; then
      if ! grep -q https://gitlab.tails.boum.org/tails/tails/-/raw/stable/wiki/src/$i puppet-code/hieradata/common.yaml ; then
        MISSING="${MISSING}missing monitoring of $i , "
      fi
    fi
  done
  
  if [ -n "${MISSING}" ]; then
    echo $MISSING
    exit 1
  fi
  
  exit 0
  | EOT
  # lint:endignore

  file { '/usr/local/bin/check_gpg_monitoring.sh':
    owner   => root,
    group   => root,
    mode    => '0755',
    content => $_script,
  }

  # lint:ignore:strict_indent
  $_service = @(EOT)
  [Service]
  Type=oneshot
  User=nobody
  Group=nogroup
  ExecStart=/usr/local/bin/check_gpg_monitoring.sh
  | EOT

  $_timer = @(EOT)
  [Timer]
  OnCalendar=daily
  | EOT
  # lint:endignore

  systemd::timer { 'check_gpg_monitoring.timer':
    timer_content   => $_timer,
    service_content => $_service,
    active          => true,
    enable          => true,
  }
}
