# @summary
#   Manage Git hooks that are not handled by another tails::profile::gitolite::hooks::*
class tails::profile::gitolite::hooks::common () {
  file {
    # Tails common hook directory
    #
    # We don't use the gitolite one, because when gl-setup is run, all
    # hooks in the gitolite common hook directory will be installed in
    # all repos
    '/var/lib/gitolite3/.gitolite/hooks/tails_common':
      ensure => directory,
      mode   => '0755',
      owner  => gitolite3,
      group  => gitolite3;
    # GitLab mirror push
    '/var/lib/gitolite3/.gitolite/hooks/tails_common/gitlab_mirror-post-update.hook':
      source => 'puppet:///modules/tails/gitolite/hooks/gitlab_mirror-post-update.hook',
      mode   => '0700',
      owner  => gitolite3,
      group  => gitolite3;
    # hook to run everything in post-update.d/
    '/var/lib/gitolite3/.gitolite/hooks/tails_common/post_update.d.hook':
      source => 'puppet:///modules/tails/gitolite/hooks/post-update.d.hook',
      mode   => '0700',
      owner  => gitolite3,
      group  => gitolite3;
    # script to run periodically to make sure the hooks are in the right place
    '/usr/local/sbin/gitolite_hooks':
      source => 'puppet:///modules/tails/gitolite/install_hooks.sh',
      mode   => '0744',
      owner  => gitolite3,
      group  => gitolite3;
    # script to run periodically to make sure the remotes are configured
    '/usr/local/sbin/gitolite_remotes':
      source => 'puppet:///modules/tails/gitolite/install_remotes.sh',
      mode   => '0744',
      owner  => gitolite3,
      group  => gitolite3;
  }

  cron { 'install_gitolite_remotes_and_hooks':
    command => '/usr/local/sbin/gitolite_remotes && /usr/local/sbin/gitolite_hooks',
    user    => gitolite3,
    hour    => '*',
    minute  => '12',
    require => File[
      '/usr/local/sbin/gitolite_remotes',
      '/usr/local/sbin/gitolite_hooks',
    ],
  }

  ensure_packages(['logrotate'])

  file { '/etc/logrotate.d/gitolite3-hooks':
    source  => 'puppet:///modules/tails/gitolite/hooks/logrotate',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['logrotate'],
  }
}
