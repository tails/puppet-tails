# @summary
#   Manage Git hooks specific to tails.git
#
# @param jenkins_master
#   hostname of jenkins master
#
# @param notifycommit_access_token
#   access token for notifying jenkins
#
class tails::profile::gitolite::hooks::tails (
  String $notifycommit_access_token,
  String $jenkins_master = 'jenkins.dragon',
) {
  file { '/var/lib/gitolite3/repositories/tails.git/hooks/post-receive':
    content => template(
      'tails/gitolite/hooks/tails-post-receive.erb'
    ),
    mode    => '0700',
    owner   => gitolite3,
    group   => gitolite3,
  }
}
