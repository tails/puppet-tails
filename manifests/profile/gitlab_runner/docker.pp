# @summary
#   Manage Docker installation for a GitLab Runner setup.
#
# @param prune_on_calendar
#   How often to prune
#
class tails::profile::gitlab_runner::docker (
  String $prune_on_calendar = 'weekly',
) {
  package { 'docker':
    ensure => 'installed',
    name   => 'docker.io',
  } -> Class['docker']

  file { '/usr/bin/dockerd':
    ensure  => 'link',
    target  => '/usr/sbin/dockerd',
    owner   => 'root',
    group   => 'root',
    require => Package['docker'],
  } -> Class['docker']

  class { 'docker':
    use_upstream_package_source => false,
    manage_package              => false,
    registry_mirror             => 'http://127.0.0.1:5000',
    extra_parameters            => ['--insecure-registry=127.0.0.1:5000'],
  }

  ### A proxy registry

  ensure_packages(['docker-registry'])

  service { 'docker-registry':
    ensure => running,
  }

  file { '/etc/docker/registry/config.yml':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    source  => 'puppet:///modules/tails/gitlab_runner/docker-registry/config.yml',
    notify  => Service['docker-registry'],
    require => Package['docker-registry'],
  }

  ### Periodic cleanup to save disk space

  systemd::timer { 'clear-docker-cache.timer':
    active         => true,
    enable         => true,
    timer_content  => epp('tails/profile/gitlab_runner/clear-docker-cache.timer.epp', {
        on_calendar => $prune_on_calendar,
    }),
    service_source => 'puppet:///modules/tails/profile/gitlab_runner/clear-docker-cache.service',
    require        => Class['gitlab_ci_runner'],
  }
}
