# @summary
#  Provide caching facilities for GitLab Runners
#
# @param prune_on_calendar
#   OnCalendar option for Docker System Prune Systemd timer
#
# @param cache_dir
#   Directory to use as GitLab Runner's cache
class tails::profile::gitlab_runner::cache (
  String               $prune_on_calendar = 'monthly',
  Stdlib::Absolutepath $cache_dir         = '/var/local/gitlab-runner-cache',
) {
  file { $cache_dir:
    ensure  => directory,
    owner   => 'gitlab-runner',
    group   => 'gitlab-runner',
    mode    => '0755',
    require => Class['gitlab_ci_runner'],
  }

  ### Periodic cleanup to save disk space

  systemd::timer { 'clear-runner-cache.timer':
    active          => true,
    enable          => true,
    timer_content   => epp('tails/profile/gitlab_runner/clear-runner-cache.timer.epp', {
        on_calendar => $prune_on_calendar,
    }),
    service_content => epp('tails/profile/gitlab_runner/clear-runner-cache.service.epp', {
        cache_dir       => $cache_dir,
    }),
    require         => Class['gitlab_ci_runner'],
  }
}
