# @summary
#   Class to create sysadmin users
#
# @param email
#   The sysadmins email address
#
class tails::profile::sysadmins (
  String $email = 'sysadmins@example.org',
) {
  include rbac

# manage root, n.b. this is also needed to use
# sshkeys::set_client_key_pair with parameter user => 'root'.

  user { 'root': ensure => present }
  file { '/root':
    ensure => directory,
    owner  => root,
    group  => staff,
    mode   => '0750',
  }
  mailalias { 'root':
    recipient => $email,
  }

# make sure root has our PGP keys

  include yapgp
  $rbac::roles['sysadmins'].each | String $username | {
    $fp = $rbac::users[$username]['pgpkey']

    pgp_key { "root-${fp}":
      ensure => present,
      fp     => $fp,
      user   => root,
      trust  => 5,
    }
  }

# create sysadmin users

  rbac::user { 'sysadmins': }

# grant ssh access to root

  rbac::ssh { 'sysadmins-to-root':
    user => 'root',
    role => 'sysadmins',
  }

# ensure proper group membership

  $groups = ['adm', 'src', 'staff', 'sudo']

  $groups.each |String $group| {
    rbac::group { "sysadmins-${group}":
      group => $group,
      role  => 'sysadmins',
    }
  }

# intrigeri-specific preferences

#  include intrigeri::home
#  include intrigeri::packages::base
#  include intrigeri::packages::emacs

# zen-specific preferences

  ensure_packages(['lynx'])
  ensure_packages(['fish'], { require => Package['lynx'] })

  file { '/home/zen/.hushlogin':
    ensure  => file,
    mode    => '0644',
    owner   => 'zen',
    group   => 'zen',
    require => User['zen'],
  }

  file { ['/home/zen/.config', '/home/zen/.config/fish']:
    ensure  => directory,
    mode    => '0755',
    owner   => 'zen',
    group   => 'zen',
    require => User['zen'],
  }

  file { '/home/zen/.config/fish/config.fish':
    ensure  => file,
    mode    => '0644',
    owner   => 'zen',
    group   => 'zen',
    require => File['/home/zen/.config/fish'],
  }

  file_line { 'fish_no_greeting_zen':
    ensure  => present,
    path    => '/home/zen/.config/fish/config.fish',
    line    => 'set fish_greeting',
    match   => '^set fish_greeting',
    replace => true,
    require => [
      File['/home/zen/.config/fish/config.fish'],
      Package['fish'],
    ],
  }
}
