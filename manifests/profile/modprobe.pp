# @summary
#   Manage modprobe options
#
# @example
#   class { 'tails::profile::modprobe':
#     conf => [{
#       module  => 'kvm',
#       options => 'ignore_msrs=1',
#     }],
#   }
#
# @param conf
#   array of hashes containing:
#     - the affected module
#     - the options to be set for the affected module
#
class tails::profile::modprobe (
  Array $conf = {},
) {
  $conf.each |Hash $modconf| {
    $module = $modconf['module']
    $options = $modconf['options']
    file { "/etc/modprobe.d/${module}.conf":
      content => "options ${module} ${options}\n",
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }
  }
}
