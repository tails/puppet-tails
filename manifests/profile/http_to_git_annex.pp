# @summary
#   Manage HTTP to git-annex gateways
#
class tails::profile::http_to_git_annex (
) {
  include tails::profile::git_annex

  tails::profile::git_annex::mirror { 'iso-history':
    checkout_dir  => '/srv/iso-history/git',
    remote_repo   => 'gitolite3@puppet-git.lizard:isos.git',
    user          => 'tails-iso-history',
    pull_minute   => ['14', '29', '44', '59'],
    home          => '/var/lib/tails-iso-history',
    ssh_keyname   => 'tails-iso@www',
    mount_point   => '/srv/iso-history',
    mount_device  => '/dev/vdb',
    mount_fstype  => 'ext4',
    mount_options => 'relatime,acl',
    require       => [
      Letsencrypt::Certonly['iso-history.tails.boum.org'],
      Letsencrypt::Certonly['iso-history.tails.net'],
    ],
  }

  $domains = ['tails.boum.org', 'tails.net']

  $domains.each | String $domain | {
    tails::profile::git_annex::nginx_frontend { "iso-history.${domain}":
      www_root           => '/srv/iso-history/git',
      ssl_redirect       => true,
      server_cfg_prepend => {
        fancyindex => 'on',
      },
    }
  }

  tails::profile::git_annex::mirror { 'torbrowser-archive':
    mode          => 'get',
    checkout_dir  => '/srv/torbrowser-archive/git',
    remote_repo   => 'gitolite3@puppet-git.lizard:torbrowser-archive.git',
    user          => 'torbrowser-archive',
    pull_minute   => ['0', '5', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'],
    home          => '/var/lib/torbrowser-archive',
    ssh_keyname   => 'torbrowser-archive@www',
    mount_point   => '/srv/torbrowser-archive',
    mount_device  => '/dev/vdc',
    mount_fstype  => 'ext4',
    mount_options => 'relatime,acl',
    require       => [
      Letsencrypt::Certonly['torbrowser-archive.tails.boum.org'],
      Letsencrypt::Certonly['torbrowser-archive.tails.net'],
    ],
  }

  $domains.each | String $domain | {
    tails::profile::git_annex::nginx_frontend { "torbrowser-archive.${domain}":
      www_root           => '/srv/torbrowser-archive/git',
      ssl_redirect       => false,
      locations          => {
        "torbrowser-archive.${domain} deny dotfiles" => {
          location      => '~ ^/[.].*',
          location_deny => ['all'],
          index_files   => [],
          www_root      => undef,
        },
      },
      server_cfg_prepend => {
        fancyindex            => 'on',
        fancyindex_exact_size => 'off',
      },
    }
  }
}
