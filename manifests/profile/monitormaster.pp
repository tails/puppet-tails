# @summary
#   profile for our monitoring master
#
# @param spamaddress
#   the address where notifications are to be sent to
#
class tails::profile::monitormaster (
  String $spamaddress = $tails::profile::sysadmins::email,
) {
  include tails::profile::sysadmins

  $plugin_packages = [
    'monitoring-plugins',
    'monitoring-plugins-basic',
    'monitoring-plugins-common',
    'monitoring-plugins-standard',
    'nagios-plugins-contrib',
    'python3-nagiosplugin',
  ]
  ensure_packages( $plugin_packages, { ensure => latest })

  class { 'monitoring::master':
    spamaddress => $spamaddress,
  }

  tor::daemon::onion_service { "icinga-api-${facts['networking']['fqdn']}":
    ports   => ['5665'],
  }

  include backupninja
  backupninja::mysql { 'backup-monitoring-db':
    ensure => present,
  }

  tirewall::accept_trusted_subnets { 'Icinga2 Master':
    dport => 5665,
  }
}
