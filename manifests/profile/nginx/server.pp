# @summary
#   An Nginx server wrapper using our custom defaults:
#
#     - Handle TLS certificate generation when SSL is set
#     - Redirect to HTTPS version of the website when SSL is set
#     - Do not log IPs
#     - Do not set index files
#     - Do not use a default location
#
# @param access_log
#   location of access log
#
# @param error_log
#   location of error log
#
# @param error_pages
#   hash of error pages
#
# @param add_header
#   hash with headers to add
#
# @param autoindex
#   whether to autoindex
#
# @param format_log
#   the log format
#
# @param gzip_types
#   mime types to be gziped
#
# @param include_files
#   files to include
#
# @param index_files
#   index files
#
# @param letsencrypt
#   whether to use letsencrypt
#
# @param listen_options
#   options for the listen directive
#
# @param listen_port
#   which port to listen on
#
# @param locations
#   hash of locations
#
# @param raw_append
#   strings to append to the server directive
#
# @param raw_prepend
#   strings to prepend to the server directive
#
# @param server_name
#   the domainnames
#
# @param server_cfg_append
#   custom directives to put after everything else inside server
#
# @param server_cfg_prepend
#   custom directives to put before everything else inside server
#
# @param ssl
#   whether to use https
#
# @param ssl_redirect
#   whether to redirect http to https
#
# @param ssl_stapling
#   whether to use ssl stapling
#
# @param ssl_stapling_file
#   the ssl stapling file
#
# @param ssl_stapling_responder
#   the ssl stapling responder
#
# @param ssl_stapling_verify
#   whether to verify OCSP responses
#
# @param ssl_cert
#   custom path to ssl certificate (or false)
#
# @param ssl_key
#   custom path to ssl key (or false)
#
# @param use_default_location
#   whether to use default location
#
# @param www_root
#   the webroot
#
define tails::profile::nginx::server (
  Optional[Variant[String, Array]] $access_log  = undef,
  Variant[String, Array]  $error_log            = 'absent',
  Optional[Hash]          $error_pages          = undef,
  Hash                    $add_header           = {},
  Optional[String]        $autoindex            = undef,
  String                  $format_log           = 'noip', # upstream default is: 'combined'
  Optional[String]        $gzip_types           = undef,
  Optional[Array[String]] $include_files        = undef,
  Array                   $index_files          = [], # upstream default is: [ 'index.html', 'index.htm', 'index.php' ]
  Boolean                 $letsencrypt          = true,
  Optional[String]        $listen_options       = undef,
  Integer                 $listen_port          = 80,
  Hash                    $locations            = {},
  Optional[Array[String]] $raw_append           = undef,
  Optional[Array[String]] $raw_prepend          = undef,
  Array[String]           $server_name          = [$name],
  Optional[Hash]          $server_cfg_append    = undef,
  Optional[Hash]          $server_cfg_prepend   = undef,
  Boolean                 $ssl                  = false,
  Variant[String, Boolean, Array[String]] $ssl_cert = false,
  Variant[String, Boolean, Array[String]] $ssl_key  = false,
  Boolean                 $ssl_redirect         = $ssl, # upstream default is: false
  Boolean $ssl_stapling                         = false,
  Optional[String] $ssl_stapling_file           = undef,
  Optional[String] $ssl_stapling_responder      = undef,
  Boolean $ssl_stapling_verify                  = false,
  Boolean                 $use_default_location = false, # upstream default is: true
  Optional[String]        $www_root             = undef,
) {
  include tails::profile::nginx

  if $ssl and $letsencrypt {
    tails::profile::nginx::letsencrypt { $name: domains => $server_name }
    $_ssl_cert = "/etc/nginx/ssl/${name}.pem"
    $_ssl_key = "/etc/nginx/ssl/${name}.key"
    $server_require = [
      File["/etc/nginx/ssl/${name}.pem"],
      File["/etc/nginx/ssl/${name}.key"],
    ]
  } else {
    $_ssl_cert = $ssl_cert
    $_ssl_key = $ssl_key
    $server_require = undef
  }

  nginx::resource::server { $name:
    access_log             => $access_log,
    error_log              => $error_log,
    error_pages            => $error_pages,
    add_header             => $add_header,
    autoindex              => $autoindex,
    format_log             => $format_log,
    gzip_types             => $gzip_types,
    include_files          => $include_files,
    index_files            => $index_files,
    listen_options         => $listen_options,
    listen_port            => $listen_port,
    locations              => $locations,
    raw_append             => $raw_append,
    raw_prepend            => $raw_prepend,
    server_name            => $server_name,
    server_cfg_append      => $server_cfg_append,
    server_cfg_prepend     => $server_cfg_prepend,
    ssl                    => $ssl,
    ssl_cert               => $_ssl_cert,
    ssl_key                => $_ssl_key,
    ssl_redirect           => $ssl_redirect,
    ssl_stapling           => false,
    ssl_stapling_file      => undef,
    ssl_stapling_responder => undef,
    ssl_stapling_verify    => false,
    use_default_location   => $use_default_location,
    www_root               => $www_root,
    require                => $server_require,
  }
}
