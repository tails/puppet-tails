# @summary
#   Manage a nginx vhost to get Let's Encrypt certificates for our mail server
#
# @param public_hostname
#   the hostname
#
class tails::profile::nginx::exportcert (
  Stdlib::Fqdn $public_hostname = 'mail.tails.boum.org',
) {
  tails::profile::nginx::server { $public_hostname:
    ssl       => false,
    locations => {
      "${public_hostname}/.well-known/acme-challenge" => {
        location    => '/.well-known/acme-challenge/',
        www_root    => '/var/www/html',
        index_files => [],
      },
    },
  }

  letsencrypt::certonly { $public_hostname:
    plugin               => 'webroot',
    webroot_paths        => ['/var/www/html'],
    additional_args      => ['--reuse-key'],
    manage_cron          => false,
    deploy_hook_commands => [],
  }

  # Export Let's Encrypt file resources that tails::schleuder will collect
  tails::profile::letsencrypt::export { $public_hostname:
    tag => "tails_mail_tls_${public_hostname}",
  }
}
