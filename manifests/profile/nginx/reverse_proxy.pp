# @summary
#   An Nginx reverse proxy
#
# @param public_hostname
#   the public hostname
#
# @param upstream_hostname
#   where we want to proxy to
#
# @param upstream_port
#   the port we want to proxy to
#
# @param auth_params
#   authentication parameters
#
# @param server_name
#   array with server names, defaults to public_hostname
#
# @param proxy_set_header
#   array with headers to set
#
# @param ssl
#   whether to use https
#
# @param ssl_redirect
#   whether to redirect http to https
#
# @param extra_locations
#   hash with extra locations
#
define tails::profile::nginx::reverse_proxy (
  Stdlib::Fqdn $public_hostname,
  Stdlib::Fqdn $upstream_hostname,
  Stdlib::Port $upstream_port,
  Hash $auth_params = {},
  Array[String] $server_name = [$public_hostname],
  Array $proxy_set_header = ['Host $host',
    'X-Real-IP $remote_addr',
    'X-Forwarded-For $proxy_add_x_forwarded_for',
    'Proxy ""',
    'X-Forwarded-Proto $scheme',
  ],
  Boolean $ssl = true,
  Boolean $ssl_redirect = $ssl,
  Hash $extra_locations = {},
) {
  tails::profile::nginx::server { $public_hostname:
    server_name  => $server_name,
    ssl          => $ssl,
    ssl_redirect => $ssl_redirect,
    locations    => {
      "${public_hostname} reverse_proxy root"   => {
        location         => '/',
        proxy            => "http://${upstream_hostname}:${upstream_port}",
        ssl_only         => $ssl_redirect,
        proxy_set_header => $proxy_set_header,
      } + $auth_params,
      "${public_hostname} reverse_proxy static" => {
        location         => '~* \.(?:ico|css|js|gif|jpe?g|png)$',
        proxy            => "http://${upstream_hostname}:${upstream_port}",
        ssl_only         => $ssl_redirect,
        proxy_set_header => $proxy_set_header,
        expires          => '7d',
        add_header       => { 'Cache-Control' => 'public' },
      } + $auth_params,
    } + $extra_locations,
  }
}
