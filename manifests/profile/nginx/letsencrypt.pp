# @summary
#   A Let's Encrypt TLS certificate
#
# @param domains
#   array of domains to get a certificate for
#
# @param server
#   the servername
#
# @param monitoring_ensure
#   whether monitoring should be present for this certificate
#
define tails::profile::nginx::letsencrypt (
  Array[Stdlib::Fqdn] $domains = [$name],
  String $server = $name,
  Enum['absent', 'present'] $monitoring_ensure = 'present',
) {
  nginx::resource::location { "${name}/.well-known/acme-challenge":
    server      => $server,
    location    => '^~ /.well-known/acme-challenge/',
    index_files => [],
    www_root    => '/var/www/html',
    ssl         => false,
    ssl_only    => false,
    priority    => 401,
  }

  letsencrypt::certonly { $name:
    plugin               => 'webroot',
    webroot_paths        => ['/var/www/html'],
    domains              => $domains,
    manage_cron          => false,
    deploy_hook_commands => [
      "/usr/local/sbin/install-tls-cert ${name}",
      '/bin/systemctl reload nginx',
    ],
    require              => [
      Nginx::Resource::Location["${name}/.well-known/acme-challenge"],
      File['/usr/local/sbin/install-tls-cert'],
      Service['nginx'],
    ],
  }

  exec { "Bootstrap TLS cert: ${name}":
    command => "/usr/local/sbin/install-tls-cert ${name}",
    creates => "/etc/nginx/ssl/${name}.pem",
    require => [
      File['/usr/local/sbin/install-tls-cert'],
      Package['ssl-cert'],
      Nginx::Resource::Location["${name}/.well-known/acme-challenge"],
    ],
  }

  file { ["/etc/nginx/ssl/${name}.pem", "/etc/nginx/ssl/${name}.key"]:
    ensure  => 'file',
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0640',
    require => Exec["Bootstrap TLS cert: ${name}"],
  }

  @@monitoring::services::tls { $name:
    ensure => $monitoring_ensure,
    cn     => $domains,
  }
}
