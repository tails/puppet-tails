# @summary
#   Manage static archives of Tails' Redmine
#
# @param ensure
#   whether the archive should exist
#
# @param http_user
#   the username for the private archive
#
# @param http_password_hash
#   the password hash for the private archive
#
# @param public_archive_subdomain
#   the public archive subdomain
#
# @param private_archive_subdomain
#   the private archive subdomain
#
class tails::profile::redmine (
  String $http_password_hash,
  Enum['present', 'absent'] $ensure      = 'present',
  String $http_user                      = 'Tails',
  String $public_archive_subdomain  = 'public-redmine-archive',
  String $private_archive_subdomain = 'private-redmine-archive',
) {
  include tails::profile::nginx

  tails::profile::redmine::archive_vhost { $public_archive_subdomain:
    public             => true,
  }

  tails::profile::redmine::archive_vhost { $private_archive_subdomain:
    public             => false,
    http_user          => $http_user,
    http_password_hash => $http_password_hash,
  }
}
