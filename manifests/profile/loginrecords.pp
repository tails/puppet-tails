# @summary
#   Ensure we don't log login attempts.
#
# @example
#   include tails::profile::loginrecords
#
class tails::profile::loginrecords (
) {
  include loginrecords
}
