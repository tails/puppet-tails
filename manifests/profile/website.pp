# @summary
#   Profile for the Tails website
#
# @param ensure
#   Whether we want the website
#
# @param user
#   The posix user that owns the website
#
# @param home_dir
#   Path to the user's home directory
#
# @param public_hostname
#   The website's hostname
#
# @param onion_hostname
#   The onion address
#
class tails::profile::website (
  Enum['present', 'absent'] $ensure = present,
  String $user                      = 'tails-website',
  Stdlib::Absolutepath $home_dir    = '/srv/tails.boum.org',
  Stdlib::Fqdn $public_hostname     = 'tails.net',
  String $onion_hostname            = 'tzoz3bensgxyzs7da7lpgsn3a74h7hlbm4wa6ytq2tg6ktd57w22vqqd.onion',
) {
  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }

  file { $home_dir:
    ensure => $directory_ensure,
    owner  => root,
    group  => $user,
    mode   => '0751',
  }

  class { 'tails::profile::website::rss2email':
    public_hostname => 'tails.net',
    user            => $user,
    home_dir        => $home_dir,
    email_recipient => 'amnesia-news@boum.org',
  }

  include tails::profile::website::mirror

  # Onion service

  # XXX The following onion service was created before the implementation of
  # tails.net, that's why we still use the tails.boum.org in the service name.
  tor::daemon::onion_service { 'http-hidden-tails.boum.org':
    ports   => ['80'],
  }

  tails::profile::website::webserver::instance { $onion_hostname:
    web_dir      => "/srv/${public_hostname}",
    ssl          => false,
    ssl_redirect => false,
    stats        => false,
  }

  # Grant UX workers access to the website logs

  rbac::ssh { 'ux-team-members-to-www-data':
    user    => 'www-data',
    role    => 'ux-team-members',
    options => ['command="/usr/bin/rsync --server --sender -vlogDtprxe.iLsfxCIvu . /var/log/nginx"'],
  }

  # For automatic deployment of the website via GitLab CI, the following SSH
  # key needs to be manually configured as a CI/CD Variable in the
  # `tails/tails` project. The private key can be found in the SSH keymaster at
  # `sshkeys::var::keymaster_storage` (which at the time of writing corresponds
  # to `puppet.lizard:/var/lib/puppet-sshkeys`).
  @@sshkeys::create_key { 'tails::profile::website':
    keytype => 'rsa',
    length  => 4096,
    tag     => 'ssh_keymaster',
  }

  # Firewall

  tirewall::public_service { 'tails::profile::website HTTP':
    dport => 80,
  }

  tirewall::public_service { 'tails::profile::website HTTPS':
    dport => 443,
  }
}
