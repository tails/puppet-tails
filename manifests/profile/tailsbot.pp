# @summary
#   A friendly bot to help ensure XMPP is useful for us.
class tails::profile::tailsbot {
  include tails::profile::podman

  $user = 'tailsbot'
  $group = 'tailsbot'
  $home = '/srv/tailsbot'
  $uid = 4000000
  $gid = 4000000

  tails::profile::podman::container { 'tailsbot':
    uid     => $uid,
    gid     => $gid,
    image   => 'docker.io/errbotio/errbot:latest ',
    flags   => {
      'volume' => ["${home}/errbot:/home/errbot:z"],
      'label'  => [
        'io.containers.autoupdate=registry',
        'PODMAN_SYSTEMD_UNIT=podman-tailsbot.service',
      ],
    },
    subdirs => [
      {
        path => 'errbot',
        uid  => $uid,
        gid  => $gid,
      },
      {
        path => 'errbot/data',
        uid  => $uid + 1000,
        gid  => $gid + 1000,
      },
      {
        path => 'errbot/plugins',
        uid  => $uid + 1000,
        gid  => $gid + 1000,
      },
      {
        path => 'errbot/plugins/tails',
        uid  => $uid + 1000,
        gid  => $gid + 1000,
      },
    ],
  }

  # XXX add service restart when these files change (avoiding dependency cycles)
  ['plugins/tails/tails.plug', 'plugins/tails/tails.py'].each | String $f | {
    file { "${home}/errbot/${f}":
      ensure => file,
      owner  => $uid + 1000,
      group  => $gid + 1000,
      mode   => '0644',
      source => "puppet:///modules/tails/profile/tailsbot/${f}",
    }
  }
}
