# @summary
#   Add hosts entries for our nodes
#
# @example
#   include tails::profile::hosts
#
# @param default_hosts
#   A hash with all hosts entries, typically defined in common.yaml
#
# @param custom_hosts
#   A hash adding to or overriding the default hosts
#
class tails::profile::hosts (
  Hash $default_hosts = {},
  Hash $custom_hosts  = {},
) {
  $hosts = $default_hosts + $custom_hosts
  $hosts.each | String $host, Hash $params | {
    host { $host:
      * => $params,
    }
  }

  Host <<| tag == 'hello_neighbour' |>>
}
