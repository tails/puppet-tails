# @summary
#   Manage a Weblate service
#
# @param system_user
#   the posix user weblate should run as
#
# @param system_group
#   the posix group weblate should run as
#
class tails::profile::weblate (
  String $system_user  = 'weblate',
  String $system_group = 'weblate',
) {
  # install weblate

  include weblate
  include tails::profile::weblate::staging_website

  # ensure backups

  include backupninja
  backupninja::pgsql { 'backup-puppetdb':
    ensure => present,
  }

  # arrange access

  User <| title == $system_user |> {
    purge_ssh_keys => true,
  }

  group { 'weblate_admin':
    ensure => present,
    system => true,
  }

  sudo::conf { 'weblate-admin':
    content => 'Cmnd_Alias WEBLATE_ADMIN_CMDS =                \
  /bin/systemctl restart weblate-integration.service, \
  /bin/systemctl start weblate-integration.service,   \
  /bin/systemctl stop weblate-integration.service,    \
  /bin/systemctl restart weblate-integration.timer, \
  /bin/systemctl start weblate-integration.timer,   \
  /bin/systemctl stop weblate-integration.timer,    \
  /bin/systemctl force-reload apache2.service, \
  /bin/systemctl reload apache2.service,       \
  /bin/systemctl restart apache2.service,      \
  /bin/systemctl start apache2.service,        \
  /bin/systemctl stop apache2.service,         \
  /usr/bin/tail -f /var/log/apache2/error.log,     \
  /bin/less /var/log/apache2/error.log,        \
  /usr/bin/tail -f /var/log/apache2/access.log,    \
  /bin/less /var/log/apache2/access.log,        \
  /bin/less /var/log/messages,  \
  /usr/bin/tail -f /var/log/messages \
  /var/lib/weblate/scripts/check_weblate_permission.py

Cmnd_Alias PODMAN_WEBLATE_CMDS = \
  /bin/systemctl --user start podman-weblate, \
  /bin/systemctl --user stop podman-weblate, \
  /bin/systemctl --user restart podman-weblate, \
  /bin/systemctl --user status podman-weblate, \
  /usr/bin/podman logs -f --tail=1 weblate, \
  /usr/bin/podman ps \

%weblate_admin ALL = (root)    NOPASSWD: WEBLATE_ADMIN_CMDS
%weblate_admin ALL = (weblate) CWD=/var/lib/weblate NOPASSWD: WEBPODMAN_WEBLATE_CMDS
%weblate_admin ALL = (weblate) NOPASSWD: ALL',
    require => Group['weblate_admin'],
  }

  sudo::conf { 'puppeteers':
    content => 'Cmnd_Alias PUPPETEER_CMDS =        \
  /usr/bin/puppet agent --disable, \
  /usr/bin/puppet agent --enable

%weblate_admin ALL = (root)    NOPASSWD: PUPPETEER_CMDS',
    require => Group['weblate_admin'],
  }

  rbac::user { 'translation-platform-maintainers': }

  rbac::group { "translation-platform-maintainers-to-${system_group}":
    group   => $system_group,
    role    => 'translation-platform-maintainers',
    require => Group[$system_group],
  }

  rbac::group { 'translation-platform-maintainers-to-weblate_admin':
    group   => 'weblate_admin',
    role    => 'translation-platform-maintainers',
    require => Group['weblate_admin'],
  }
}
