# @summary
#   Manage Postfix setup for whisperback
#
# @param inet_interface
#   the interfaces to receive mail on
# @param onion_address
#   the onion address
# @param whisperback_allowed_recipients
#   for whom we receive mail
#
class tails::profile::whisperback (
  String        $onion_address,
  Array         $whisperback_allowed_recipients,
  String        $inet_interface = '127.0.0.1',
) {
  package { 'swaks': ensure => installed }

  class { 'postfix':
    parameters => {
      myhostname                            => $onion_address,
      myorigin                              => $onion_address,
      mydestination                         => $onion_address,
      mynetworks                            => ['0.0.0.0/0'],
      alias_maps                            => 'hash:/etc/aliases',
      alias_database                        => 'hash:/etc/aliases',
      always_add_missing_headers            => 'yes',
      append_dot_mydomain                   => 'no',
      biff                                  => 'no',
      compatibility_level                   => 2,
      default_destination_concurrency_limit => 2,
      default_destination_rate_delay        => '1s',
      delay_warning_time                    => '4h',
      disable_vrfy_command                  => 'yes',
      inet_interfaces                       => $inet_interface,
      inet_protocols                        => 'ipv4',
      mailbox_size_limit                    => 0,
      message_size_limit                    => 40960000,
      readme_directory                      => 'no',
      recipient_delimiter                   => '+',
      sender_canonical_maps                 => 'hash:/etc/postfix/maps/sender_canonical',
      show_user_unknown_table_name          => 'no',
      smtp_dns_support_level                => 'dnssec',
      smtp_tls_CApath                       => '/etc/ssl/certs',
      smtp_tls_ciphers                      => 'high',
      smtp_tls_loglevel                     => 1,
      smtp_tls_mandatory_protocols          => 'TLSv1.2',
      smtp_tls_mandatory_ciphers            => 'high',
      smtp_tls_mandatory_exclude_ciphers    => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA',
      smtp_tls_policy_maps                  => 'hash:/etc/postfix/maps/tls_policy',
      smtp_tls_protocols                    => 'TLSv1.2',
      smtp_tls_security_level               => 'dane',
      smtp_tls_session_cache_database       => "btree:\${data_directory}/smtp_scache",
      smtpd_banner                          => '$myhostname ESMTP $mail_name (Debian/GNU)',
      smtpd_client_connection_rate_limit    => 60,
      smtpd_client_message_rate_limit       => 120,
      smtpd_client_restrictions             => 'permit_mynetworks',
      smtpd_data_restrictions               => 'permit_mynetworks, reject_unauth_pipelining',
      smtpd_etrn_restrictions               => 'reject',
      smtpd_helo_required                   => 'yes',
      smtpd_helo_restrictions               => 'permit_mynetworks',
      smtpd_recipient_restrictions          => 'check_recipient_access hash:/etc/postfix/maps/access, reject',
      smtpd_relay_restrictions              => 'permit_mynetworks, reject_unauth_destination, reject_unknown_recipient_domain',
      smtpd_use_tls                         => 'no',
      transport_maps                        => 'hash:/etc/postfix/maps/transport',
      virtual_alias_maps                    => 'hash:/etc/postfix/maps/virtual',
    },
    maps       => {
      access           => {
        type     => 'hash',
        contents => $whisperback_allowed_recipients,
      },
      tls_policy       => {
        type     => 'hash',
        contents => [
          '*          dane',
        ],
      },
      sender_canonical => {
        type     => 'hash',
        contents => [],
      },
      transport        => {
        type     => 'hash',
        contents => [
          'boum.org smtp:mail.lizard',
          'tails.net smtp:mta.chameleon',
        ],
      },
      virtual          => {
        type     => 'hash',
        contents => [
          'tails-bugs@boum.org support@tails.net',
        ],
      },
    },
  }

  include monitoring::checkcommands::mailqueue
}
