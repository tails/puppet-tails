# @summary
#   profile for using puppet-rbac role based access contol
#
class tails::profile::rbac (
) {
  include rbac
}
