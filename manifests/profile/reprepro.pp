# @summary
#   Manage all Tails APT repositories
#
# @param snapshots_architectures
#   the architectures
#
# @param signing_key
#   the signing key path (this could use refactoring)
#
# @param signing_key_content
#   the content of the signing key (e.g., to be pulled from hiera-eyaml)
#
class tails::profile::reprepro (
  Hash $snapshots_architectures,
  String $signing_key,
  String $signing_key_content,
) {
  include rbac

# set up the webserver

  include tails::profile::nginx

  # save disk space
  augeas { 'logrotate_nginx':
    context => '/files/etc/logrotate.d/nginx',
    changes => [
      'set rule/rotate 0',
      'rm rule/compress',
      'rm rule/delaycompress',
    ],
    require => Class['tails::profile::nginx'],
  }

# ensure we have all the plugins for proper monitoring

  include monitoring::plugins::number_in_file

# ensure we have the signing key available

  $gnupg_homedir = '/srv/reprepro/.gnupg'
  file { "${gnupg_homedir}/keys.asc":
    mode    => '0640',
    owner   => root,
    group   => reprepro,
    content => $signing_key_content,
    require => File[$gnupg_homedir],
  }

  file { '/srv/apt-snapshots/time-based/private-keys.asc':
    owner   => root,
    group   => 'reprepro-time-based-snapshots',
    mode    => '0440',
    content => $signing_key_content,
  }

# ensure we have the release manager's pgp keys imported

  include yapgp
  $rbac::roles['foundations-team-members'].each | String $username | {
    $fp = $rbac::users[$username]['pgpkey']

    pgp_key { "reprepro-${fp}":
      ensure  => present,
      fp      => $fp,
      user    => 'reprepro',
      require => Class['reprepro'],
    }
  }

# install and configure reprepro
# TODO: refactor the tails::profile::reprepro manifests

  include reprepro

  $uploaders = $rbac::roles['foundations-team-members'].map |$commiter| { "${rbac::users[$commiter]['pgpkey'][-16,16]}+" }

  class { 'tails::profile::reprepro::custom':
    uploaders => $uploaders,
    require   => [Class['tails::profile::nginx'], Class['reprepro']],
  }

  class { 'tails::profile::reprepro::snapshots::tagged': }

  class { 'tails::profile::reprepro::snapshots::time_based':
    architectures => $snapshots_architectures,
    signwith      => $signing_key,
  }

# ensure access for ft members with commit rights

  rbac::user { 'foundations-team-members': }

  rbac::ssh { 'foundations-team-members-to-reprepro-time-based-snapshots':
    user    => 'reprepro-time-based-snapshots',
    role    => 'foundations-team-members',
    require => Class['tails::profile::reprepro::snapshots::time_based'],
  }

  User <| title == 'reprepro' |> {
    purge_ssh_keys => true,
  }

  rbac::ssh { 'foundations-team-members-to-reprepro':
    user    => 'reprepro',
    role    => 'foundations-team-members',
    require => Class['reprepro'],
  }
}
