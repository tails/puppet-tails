# @summary
#   Manage a Hedgedoc service
#
# @param db_pass
#   database password
#
# @param domain
#   domainname
#
# @param gitlab_baseurl
#   baseurl of gitlab server to authenticate to
#
# @param gitlab_clientid
#   oauth2 client id
#
# @param gitlab_clientsecret
#   oauth2 client secret
#
# @param session_secret
#   session secret
#
class tails::profile::hedgedoc (
  String $db_pass,
  String $domain,
  String $gitlab_baseurl,
  String $gitlab_clientid,
  String $gitlab_clientsecret,
  String $session_secret,
) {
  #
  # Database
  #

  $db_dialect = 'mariadb'
  $db_host = '10.0.2.2'
  $db_name = 'hedgedoc'
  $db_user = 'hedgedoc'
  $db_port = 3306
  $db_database = 'hedgedoc'

  class { 'mysql::server':
    override_options => {
      'mysqld' => {
        'ssl_ca'   => undef,
        'ssl_cert' => undef,
        'ssl_key'  => undef,
      },
    },
  }

  mysql::db { $db_name:
    charset  => 'utf8mb4',
    collate  => 'utf8mb4_unicode_ci',
    user     => $db_user,
    password => $db_pass,
    grant    => ['ALL'],
  }

  include backupninja

  backupninja::mysql { 'backup-hedgedoc-db':
    ensure    => present,
    databases => 'hedgedoc',
  }

  #
  # Container
  #

  include tails::profile::podman

  $user = 'hedgedoc'
  $group = 'hedgedoc'
  $home = '/srv/hedgedoc'
  $uid = 3000000
  $gid = 3000000

  file { "${home}/podman.env":
    ensure  => file,
    content => epp('tails/profile/hedgedoc/podman.env.epp', {
        db_host             => $db_host,
        db_name             => $db_name,
        db_pass             => $db_pass,
        db_port             => $db_port,
        db_dialect          => $db_dialect,
        db_user             => $db_user,
        domain              => $domain,
        gitlab_baseurl      => $gitlab_baseurl,
        gitlab_clientid     => $gitlab_clientid,
        gitlab_clientsecret => $gitlab_clientsecret,
        session_secret      => $session_secret,
    }),
    owner   => $uid,
    group   => $gid,
    mode    => '0640',  # contains passwords
    notify  => Podman::Container['hedgedoc'],
    require => File[$home],
  }

  tails::profile::podman::container { 'hedgedoc':
    uid     => $uid,
    gid     => $gid,
    image   => 'quay.io/hedgedoc/hedgedoc:latest',
    flags   => {
      'publish'  => '3000:3000',
      'volume'   => ["${home}/uploads:/hedgedoc/public/uploads:z"],
      'env-file' => "${home}/podman.env",
      # this container will be able to see the hosts localhost at 10.0.2.2
      'network'  => 'slirp4netns:allow_host_loopback=true',
      'label'    => [
        'io.containers.autoupdate=registry',
        'PODMAN_SYSTEMD_UNIT=podman-hedgedoc.service',
      ],
    },
    subdirs => [{
        path => 'uploads',
        uid  => $uid + 10000,
        gid  => $gid + 10000,
    }],
    require => Mysql::Db[$db_name],
  }

  #
  # HTTP reverse-proxy
  #

  include tails::profile::nginx

  tails::profile::nginx::reverse_proxy { 'hedgedoc':
    public_hostname   => $domain,
    upstream_hostname => 'localhost',
    upstream_port     => 3000,
    extra_locations   => {
      '/socket.io/' => {
        ssl_only   => true,
        proxy      => 'http://localhost:3000',
        raw_append => [
          'proxy_set_header Upgrade $http_upgrade;',
          'proxy_set_header Connection upgrade;',
        ],
      },
    },
    require           => Tirewall::Public_service['tails::profile::hedgedoc HTTP'],
  }

  tirewall::public_service { 'tails::profile::hedgedoc HTTP':
    dport => 80,
  }

  tirewall::public_service { 'tails::profile::hedgedoc HTTPS':
    dport => 443,
  }
}
