# @summary
#   profile to manage APT on Tails systems
#
# @param cron_mode
#   periodic download or unattended upgrades
#
# @param codename
#   the debian codename we're running
#
# @param repos
#   the debian repositories
#
# @param debian_url
#   the url to download from
#
# @param include_src
#   whether we want src
#
# @param proxy
#   whether to use a proxy
#
# @param proxy_host
#   the proxy hostname
#
# @param proxy_port
#   the proxy port
#
# @param proxy_https
#   whether the proxy does https
#
# @param proxy_https_acng
#   whether the proxy is acng
#
# @param proxy_direct
#   whether it's a direct proxy
#
# @param listchanges_which
#   listchanges for news, changelog, or both
#
# @param custom_keys
#   custom apt keys to configure
#
class tails::profile::apt (
  Enum['update', 'unattended_upgrades'] $cron_mode,
  String $codename                                     = 'bookworm',
  String $repos                                        = 'main',
  String $debian_url                                   = 'http://ftp.us.debian.org/debian',
  Boolean $include_src                                 = false,
  Boolean $proxy                                       = true,
  String $proxy_host                                   = "apt-proxy.${facts['networking']['domain']}",
  Stdlib::Port $proxy_port                             = 3142,
  Boolean $proxy_https                                 = false,
  Boolean $proxy_https_acng                            = false,
  Boolean $proxy_direct                                = false,
  Enum['news', 'changelog', 'both'] $listchanges_which = 'news',
  Hash $custom_keys                                    = {},
) {
  $proxy_hash = $proxy ? {
    true => {
      host       => $proxy_host,
      port       => $proxy_port,
      https      => $proxy_https,
      https_acng => $proxy_https_acng,
      direct     => $proxy_direct,
    },
    false => {},
  }

  class { 'apt':
    proxy            => $proxy_hash,
    include_defaults => {
      'deb' => true,
      'src' => $include_src,
    },
    purge            => {
      'sources.list'   => true,
      'sources.list.d' => true,
      'preferences'    => true,
      'preferences.d'  => true,
    },
  }

  apt::source { $codename:
    location => $debian_url,
    release  => $codename,
    repos    => $repos,
    pin      => {
      originator => 'Debian',
      codename   => $codename,
      priority   => 990,
    },
  }

  case $codename {
    /(bullseye|bookworm)/: {
      apt::source { "${codename}-updates":
        location => $debian_url,
        release  => "${codename}-updates",
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => "${codename}-updates",
          priority   => 990,
        },
      }
      apt::source { "${codename}-security":
        location => 'http://security.debian.org/debian-security',
        release  => "${codename}-security",
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => "${codename}-security",
          priority   => 990,
        },
      }
      class { 'apt::backports':
        pin      => 100,
        location => $debian_url,
      }
      if $codename == 'bullseye' {
        apt::source { 'bookworm':
          location => $debian_url,
          release  => 'bookworm',
          repos    => $repos,
          pin      => {
            originator => 'Debian',
            codename   => 'bookworm',
            priority   => 2,
          },
        }
      }
      apt::source { 'sid':
        location => $debian_url,
        release  => 'sid',
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => 'sid',
          priority   => 2,
        },
      }
    }
    'sid': {
      apt::source { 'experimental':
        location => $debian_url,
        repos    => $repos,
        release  => 'experimental',
        pin      => {
          originator => 'Debian',
          release    => 'experimental',
          priority   => 2,
        },
      }
    }
    default: {
      fail("codename = ${codename} is not supported by tails::profile::apt yet.")
    }
  }

  apt::pin { 'zzz_Debian_fallback':
    originator => 'Debian',
    priority   => -10,
  }

  case $cron_mode {
    'update':           {
      ::apt::conf { 'periodic_enable':
        content  => 'APT::Periodic::Enable "1";',
        priority => 11,
      }
      ::apt::conf { 'periodic_download_upgradeable_packages':
        content  => 'APT::Periodic::Download-Upgradeable-Packages "1";',
        priority => 11,
      }
      cron { 'apt_cron_every_N_hours': ensure => absent }
    }
    'unattended_upgrades': {
      include tails::profile::sysadmins
      class { 'unattended_upgrades':
        mail    => { 'to' => $tails::profile::sysadmins::email, },
        origins => ['origin=*'],
      }
    }
    default: {}
  }

  # Run apt-daily-upgrade.service 6 times a day: apt-daily.timer
  # updates APT lists up to twice a day which apt-daily-upgrade.timer
  # triggers upgrades only once a day. Between the APT lists update
  # and the upgrade, our monitoring gets unhappy. We used to apply
  # upgrades 6 times a day with cron-apt so let's do the same.
  # Note that this will only apply upgrades if the relevant
  # APT::Periodic::* options are set, which we do above depending on
  # $cron_mode, so what follows is safe even on systems where we
  # disable unattended upgrades.
  service { 'apt-daily-upgrade.timer': ensure => running }
  systemd::dropin_file { 'apt-daily-upgrade.timer/frequency.conf':
    unit     => 'apt-daily-upgrade.timer',
    filename => 'frequency.conf',
    content  => '[Timer]
# Reset the default value
OnCalendar=
# Run apt-daily-upgrade.service every 4 hours
OnCalendar=*-*-* 0,4,8,12,16,20:00
',
  }
  systemd::dropin_file { 'apt-daily-upgrade.service.d/frequency.conf':
    unit     => 'apt-daily-upgrade.service',
    filename => 'frequency.conf',
    content  => '[Service]
# Force upgrading even if we already did so less than 24h ago.
# /usr/lib/apt/apt.systemd.daily uses this timestamp file
# to tell when it applied upgrades last, and would otherwise
# refuse applying them if it already did so in the last 24h.
ExecStartPre=/bin/rm -f /var/lib/apt/periodic/upgrade-stamp
',
  }

  class { 'apt_listchanges': which => $listchanges_which }

  # Ensure we are told by our monitoring system when reboot-notifier
  # creates its flag file, but disable reboot-notifier's own email
  # notification system instead.
  package { 'reboot-notifier': ensure => installed }
  file_line { 'disable_reboot-notifier_email':
    path    => '/etc/default/reboot-notifier',
    match   => 'NOTIFICATION_EMAIL=.*',
    line    => 'NOTIFICATION_EMAIL=',
    require => Package['reboot-notifier'],
  }

  ::apt::conf { 'periodic_autoclean_interval':
    content  => 'APT::Periodic::AutocleanInterval "1";',
    priority => '08',
  }
  ::apt::conf { 'periodic_clean_interval':
    content  => 'APT::Periodic::CleanInterval "1";',
    priority => '09',
  }
  ::apt::conf { 'dont_keep_downloaded_packages':
    content  => 'APT::Keep-Downloaded-Packages "false";',
    priority => 10,
  }
  ::apt::conf { 'proxy_bugs_debian_org':
    content  => 'Acquire::HTTP::Proxy::bugs.debian.org "DIRECT";',
  }

  $custom_keys.each | String $keyname, Hash $keyparams | {
    apt::key { $keyname:
      * => $keyparams,
    }
  }

  file { '/etc/apt/keyrings':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }
}
