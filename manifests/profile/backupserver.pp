# @summary
#   profile to manage a borg backup server
#
# @param backupagents
#   hash with agents who can write backups
#
class tails::profile::backupserver (
  Hash $backupagents,
) {
  class { 'borgbackup::server':
    clients => $backupagents,
  }

  $backupagents.each | String $agent, Hash $sshkey | {
    rbac::ssh { "sysadmins-to-${agent}":
      user => $agent,
      role => 'sysadmins',
    }
  }
}
