# @summary
#   Receive RSS feeds by e-mail
#
#   Can be configured through Hiera by setting the following hash:
#
#     tails::profile::rss2email::feeds:
#       feedname:
#         url: https://groups.google.com/forum/feed/groupname/msgs/rss.xml?num=50
#         from: feeds@lizard.tails.net
#         to: sysadmins@tails.net
#
# @param default_from
#   the default sender
#
# @param default_to
#   the default recipient
#
# @param feeds
#   the hash of feeds to mail (see summary)
#
class tails::profile::rss2email (
  String $default_from,
  String $default_to,
  Hash   $feeds        = {},
) {
  class { 'rss2email':
    default_from => $default_from,
    default_to   => $default_to,
  }

  $feeds.each | String $feedname, Hash $config | {
    $from    = $config['from']    ? { undef => $default_from, default => $config['from'] }
    $to      = $config['to']      ? { undef => $default_to,   default => $config['to'] }
    $filters = $config['filters'] ? { undef => {},            default => $config['filters'] }

    rss2email::feed { $feedname:
      url     => $config['url'],
      from    => $from,
      to      => $to,
      filters => $filters,
    }
  }
}
