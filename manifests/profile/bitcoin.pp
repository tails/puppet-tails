# @summary
#   profile for bitcoin server
#
class tails::profile::bitcoin () {
  # install bitcoind

  apt::pin { 'bitcoind':
    packages   => 'bitcoind',
    originator => 'Debian',
    codename   => 'sid',
    priority   => 990,
  }

  include bitcoind

  # grant accountants access

  rbac::ssh { 'accounting-team-members-to-bitcoin':
    user    => 'bitcoin',
    role    => 'accounting-team-members',
    require => Class['bitcoind'],
  }

  # arrange backups

  file { '/var/backups/bitcoin':
    ensure => directory,
    owner  => bitcoin,
    group  => bitcoin,
    mode   => '0700',
  }

  file { '/var/lib/bitcoin/backups':
    ensure => directory,
    owner  => bitcoin,
    group  => bitcoin,
    mode   => '0700',
  }

  $wallet_tmp_backup = '/var/lib/bitcoin/backups/wallet.dat'
  $wallet_backup = '/var/backups/bitcoin/wallet.dat'
  cron { 'backup-bitcoin-wallet':
    command => "bitcoin-cli backupwallet \"${wallet_tmp_backup}\" && mv \"${wallet_tmp_backup}\" \"${wallet_backup}\"",
    user    => bitcoin,
    hour    => 1,
    minute  => 59,
  }

  # ensure we receive mail to bitcoin

  include tails::profile::sysadmins

  mailalias { 'bitcoin':
    recipient => $tails::profile::sysadmins::email,
  }

  # useful for processing the JSON output of bitcoin-cli

  ensure_packages(['jq'])
}
