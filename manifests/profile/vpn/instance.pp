# @summary
#   This class manages a Tails VPN installation using tinc in `switch`
#   mode. By default it only needs the vpn_address and vpn_subnet parameters.
#
#   Note that this manifest does not check that you pass consistent
#   values for $vpn_address CIDR and $vpn_subnet netmask. Obviously you
#   should. Take care to do so!
#
#   It's also possible to define the ipaddress tinc will listen on with
#   the ipaddress parameter. Usefull if you have several interfaces.
#
# @param connect_to
#   the node to connect to
#
# @param ip_address
#   this node's IP (defaults to known fact)
#
# @param routes
#   routes to add when tinc comes up
#
# @param vpn_address
#   the VPN internal ip address, used to configure the VPN
#   interface. Must be in the CIDR format, e.g 192.168.0.2/24.
#
# @param vpn_subnet
#   the VPN internal subnet, which is used to
#   configure the routing. Must be in the form of subnet/netmask, e.g
#   192.168.0.0/255.255.255.0.
#
# @param ensure
#   whether to have tinc configured or not
#
# @param proxy
#   possible proxy
#
# @param collect_routes
#   whether to collect exported routes from other nodes
#
define tails::profile::vpn::instance (
  Stdlib::IP::Address::V4::CIDR $vpn_address,
  Pattern[/\A([0-9]{1,3}\.){3}[0-9]{1,3}\/([0-9]{1,3}\.){3}[0-9]{1,3}\z/] $vpn_subnet,
  Array[String] $connect_to                             = [],
  Stdlib::IP::Address::V4 $ip_address                   = $facts['networking']['ip'],
  Array[Array[Stdlib::IP::Address::V4, 2, 2]] $routes   = [],
  Enum['present', 'absent'] $ensure                     = present,
  Optional[String] $proxy                               = undef,
  Variant[Boolean, Array[String]] $collect_routes       = true,
) {
  $vpn_spaddress = split($vpn_address, '/')
  $vpn_ip = $vpn_spaddress[0]

  $vpn_spsubnet = split($vpn_subnet, '/')
  $vpn_net = $vpn_spsubnet[0]
  $vpn_netmask = $vpn_spsubnet[1]

  assert_type(Pattern[/.+@.+/], $name)
  $sp_name = split($name, '@')
  $hostname = $sp_name[0]
  $vpn_name = $sp_name[1]
  $interface = $vpn_name

  assert_type(String[1], $hostname)
  assert_type(String[1], $vpn_name)

  $directory_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'directory',
  }

  include tails::profile::vpn

  file {
    "/etc/tinc/${vpn_name}":
      ensure  => $directory_ensure,
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      require => Package['tinc'];
    "/etc/tinc/${vpn_name}/hosts":
      ensure  => $directory_ensure,
      purge   => true,
      recurse => true,
      owner   => 'root',
      group   => 'root',
      mode    => '0700';
    "/etc/tinc/${vpn_name}/tinc.conf":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      content => epp('tails/vpn/tinc.conf.epp', {
          hostname   => $hostname,
          connect_to => $connect_to,
          proxy      => $proxy,
      });
    "/etc/tinc/${vpn_name}/rsa_key.priv":
      ensure => $ensure,
      owner  => 'root',
      group  => 'root',
      mode   => '0600',
  }

  # Build the tinc-up/down files

  concat {
    "/etc/tinc/${vpn_name}/tinc-up":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      require => File["/etc/tinc/${vpn_name}"];
    "/etc/tinc/${vpn_name}/tinc-down":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      require => File["/etc/tinc/${vpn_name}"];
  }

  concat::fragment {
    "${vpn_name}_tinc-up":
      target  => "/etc/tinc/${vpn_name}/tinc-up",
      content => epp('tails/vpn/tinc-up.epp', {
          interface   => $interface,
          vpn_ip      => $vpn_ip,
          vpn_net     => $vpn_net,
          vpn_netmask => $vpn_netmask,
          routes      => $routes,
      }),
      order   => 0;
    "${vpn_name}_tinc-down":
      target  => "/etc/tinc/${vpn_name}/tinc-down",
      content => epp('tails/vpn/tinc-down.epp', {
          interface => $interface
      }),
      order   => 0;
  }

  case $collect_routes {
    true: {
      Concat::Fragment <<| target == "/etc/tinc/${vpn_name}/tinc-up" and tag != $trusted['certname'] |>>
      Concat::Fragment <<| target == "/etc/tinc/${vpn_name}/tinc-down" and tag != $trusted['certname'] |>>
      Tails::Profile::Vpn::Firewall_rule <<| tag != $trusted['certname'] |>>
    }
    Array: {
      $collect_routes.each | String $source | {
        Concat::Fragment <<| target == "/etc/tinc/${vpn_name}/tinc-up" and tag == $source |>>
        Concat::Fragment <<| target == "/etc/tinc/${vpn_name}/tinc-down" and tag == $source |>>
        Tails::Profile::Vpn::Firewall_rule <<| tag == $source |>>
      }
    }
    default: {}
  }

  # Start this instance on Tinc service start

  concat::fragment { "nets_boot_${vpn_name}":
    target  => '/etc/tinc/nets.boot',
    content => "${vpn_name}\n",
    notify  => Service['tinc'],
  }
}
