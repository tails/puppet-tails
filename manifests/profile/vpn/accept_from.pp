# @summary
#   Setup firewalll rules to accept connections to the Tinc port from an
#   address.
#
# @param address
#   The IP address to accept connections from.
#
# @example
#   tails::profile::vpn::accept_from{ 'My Node':
#     address  => '10.13.12.1',
#   }
define tails::profile::vpn::accept_from (
  Stdlib::IP::Address::V4 $address,
) {
  ['tcp', 'udp'].each | String $proto | {
    firewall { "300 accept Tinc from ${name} (${proto.upcase()})":
      table  => 'filter',
      chain  => 'INPUT',
      proto  => $proto,
      source => $address,
      dport  => 655,
      action => 'accept',
    }
  }
}
