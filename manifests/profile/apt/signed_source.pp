# @summary
#   Configure an APT source that is signed by a certain key.
#
#   Fetch the key identified by ${key_fp} from a keyserver in compile-time, store
#   it in /etc/apt/keyrings and add an APT source using the 'Signed-By' option.
#
# @param ensure
#   whether we want this source
#
# @param key_fp
#   fingerprint of the signing public key
#
# @param key_source
#   where to get the key from if it's not fetchable from listservers
#
# @param location
#   location of the apt source
#
# @param release
#   debian release we want
#
# @param repos
#   debian repositories we want
#
define tails::profile::apt::signed_source (
  String $key_fp,
  String $key_source,
  String $location,
  String $release,
  String $repos,
  Enum['present', 'absent'] $ensure = 'present',
) {
  $key_path = "/etc/apt/keyrings/${name}-${key_fp}.asc"

  file { $key_path:
    ensure  => $ensure,
    source  => $key_source,
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File['/etc/apt/keyrings'],
  }

  $_location = ($key_fp and $location) ? {
    true    => "[signed-by=${key_path}] ${location}",
    default => $location,
  }

  apt::source { $name:
    ensure   => $ensure,
    location => $_location,
    release  => $release,
    repos    => $repos,
    require  => File[$key_path],
  }
}
