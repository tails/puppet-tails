Facter.add('tails_live_certs') do
  setcode do
    basedir = '/etc/letsencrypt/live'
    livecerts = {}
    begin
      Dir.children(basedir).select { |e| File.directory? basedir + '/' + e }.each do |d|
        livecerts[d] = {}
        Dir.children(basedir + '/' + d).select { |g| File.file? basedir + '/' + d + '/' + g }.each do |f|
          livecerts[d][f] = File.read(basedir + '/' + d + '/' + f)
        end
      end
      livecerts
    rescue Errno::ENOENT, Errno::EACCES
      {}
    end
  end
end
